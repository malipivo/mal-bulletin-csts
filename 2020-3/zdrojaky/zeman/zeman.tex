% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode 
% !TEX root = ../../mal-core.tex

\gdef\mujnazevCS{Neúplné vzorky z Poissonova rozdělení}
\gdef\mujnazevEN{Incomplete Poisson samples}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Ondřej Zeman, Jiří Dvořák}

\bonushyper
%%%  DEFINICE DVORAK
\newtheorem{lemma}{Lemma}
\newenvironment{dukaz}{
  \par\medskip\noindent
  \textit{Důkaz}.
}{
\newline
\rightline{$\qed$}  % nebo \SquareCastShadowBottomRight z balíčku bbding
}



\nazev{\mujnazevCS}

\nazev{\mujnazevEN}

\author{Ondřej Zeman, Jiří Dvořák}

\Adresa{Matematicko-fyzikální fakulta Univerzity Karlovy, Sokolovská 83, 186\,75 Praha~8}

\Email{zeman.ond@seznam.cz, dvorak@karlin.mff.cuni.cz}


\Abstrakt{V tomto příspěvku se budeme zabývat odhadem parametrů z~neúplného vzorku z Poissonova rozdělení, konkrétně z té části náhodného výběru, ve které chybí nulová pozorování. Cílem je odhadnout rozsah původního náhodného výběru $N$ a parametr Poissonova rozdělení $\lambda$. Zaměříme se na odhady popsané ve dvou článcích. Novější článek uvádí, že v daném místě pouze opakuje postup staršího článku. Ve skutečnosti ale vychází z jiných věrohodnostních funkcí a dospěje k mírně odlišným odhadům. V obou článcích odvození tvaru věrohodnostních rovnic i výsledných odhadů chybí, v~našem příspěvku proto podrobná odvození doplníme a vyjasníme vztah mezi oběma články. Příspěvek vychází z bakalářské práce prvního autora.}
\smallskip
\KlicovaSlova{useknuté Poissonovo rozdělení, maximální věrohodnost, neúplný výběr}

\Abstract{This contribution addresses the problem of parameter estimation from an incomplete Poisson sample, i.e. from the part of the random sample where zero values are missing. The aim is to estimate the size of the original sample $N$ and the parameter $\lambda$ of the Poisson distribution. We focus on the estimators from two papers. The newer one claims that, in the relevant part, it only reviews the procedure from the older paper. However, it uses different likelihood functions and arrives at slightly different estimators. Derivations are missing in both of the papers and hence we present detailed derivations here and we clarify the connections between the papers. This contribution is based on the bachelor thesis of the first author.}
\smallskip
\KeyWords{truncated Poisson distribution, maximum likelihood, incomplete sample}



\section{Úvod}

V této práci se budeme zabývat bodovými odhady parametrů z náhodného výběru z Poissonova rozdělení, kde chybí všechna nulová pozorování. Vliv chybějících pozorování může být velmi výrazný v případě, že skutečná hodnota parametru Poissonova rozdělení je blízká nule. Jako modelovou situaci, kdy nelze pozorovat nulové hodnoty, můžeme použít příklad z \cite{DG73}, který se zabývá epidemií cholery v nejmenované indické vesnici.

Cholera má jako většina infekčních nemocí určitou inkubační dobu a lidé mohou být nakaženi, přestože se u nich zatím neprojevily příznaky. Označme $N$ počet domácností zasažených nákazou a předpokládejme, že počet lidí s~příznaky v domácnosti zasažené nákazou má Poissonovo rozdělení s~parametrem $\lambda$. Nahlášené jsou ale jen počty lidí s~příznaky z těch domácností, kde má příznaky alespoň jeden člověk (32 domácností reportuje jednoho nemocného s~příznaky, 16 domácností reportuje dva, 6 domácností reportuje tři, 1 domácnost reportuje čtyři). Zůstává však neznámý počet domácností zasažených cholerou, kde (zatím) nikdo nemá příznaky. Nulová pozorování tedy nejsou k dispozici. Cílem je nyní odhadnout parametr $\lambda$ a celkový rozsah výběru $N$ (tedy včetně neznámého počtu nulových pozorování).

Poznamenejme na okraj, že je možné k problému přistoupit z opačné strany a pozorování si doplnit nulami až do celkového počtu domácností ve vesnici. V tomto kontextu je pak možné využít tzv. \uv{zero-inflated Poisson} modelů.

V tomto příspěvku představíme odhady z článků \cite{BDG78} a \cite{DG73}. Protože v původních článcích chybí odvození použitých věrohodnostních funkcí i výsledných odhadů, tato odvození zde podrobně provedeme. Půjde vpravdě o detektivní pátrání, protože novější článek \cite{BDG78} v dané pasáži tvrdí, že pouze opakuje postup ze staršího článku \cite{DG73}, ačkoli uvidíme, že použité věrohodnostní funkce i výsledné odhady se od sebe liší. Tento rozpor je velmi překvapivý s~ohledem na to, že autorské kolektivy obou článků nejsou disjunktní. Naopak jeden je podmnožinou druhého\ldots

Příspěvek vychází z bakalářské práce prvního autora \cite{Zeman2018}. V ní jsou diskutovány i další způsoby odhadu parametrů, odvozeny některé jejich asymptotické vlastnosti a pomocí simulací porovnána přesnost odhadů pro výběry s~konečným rozsahem.




\section{Základní pojmy}

Nechť $Y_1, \ldots, Y_N$ je náhodný výběr z Poissonova rozdělení s~parametrem $\lambda > 0$. Tento výběr budeme nazývat \textit{úplným výběrem}. Jako $X_1,\ldots,X_n$ označíme ty náhodné veličiny z úplného výběru, které nemají nulovou hodnotu. Náhodné veličiny $X_1,\ldots,X_n$ budeme nazývat \textit{neúplným výběrem}, případně \textit{neúplným vzorkem}. V dalším výkladu budeme považovat $n \in \mathbb{N}$ za známé, naopak $N \in \mathbb{N}, N \geq n,$ považujeme za neznámé a budeme ho chtít odhadnout spolu s~parametrem $\lambda$. Situaci, kdy $n=0$, mlčky přehlížíme, což však není příliš omezující.

Dále budeme značit symbolem $n_x$ počet pozorování s~hodnotou $x \in \mathbb{N}$, symbolem $R$ nejvyšší pozorovanou hodnotu, $S$ bude součet všech pozorovaných hodnot, $S=\sum_{x=1}^R x n_x$, $\lfloor a \rfloor$ bude dolní celá část kladného čísla $a$.

Netřeba jistě připomínat, že pravděpodobnosti v Poissonově rozdělení mají tvar $\frac{\lambda^k}{k!}\ee^{-\lambda},\, k = 0, 1,\ldots$ Uvádíme je však pro porovnání s~pravděpodobnostmi \textit{useknutého Poissonova rozdělení}, které mají tvar $\frac{\ee^{-\lambda}}{1-\ee^{-\lambda}} \frac{\lambda^k}{k!},\, k = 1, 2,\ldots$ Jde vlastně o pravděpodobnosti Poissonova rozdělení za podmínky, že náhodná veličina má kladnou hodnotu.






\section{Starší článek}

V této části se zaměříme na odvození postupu ze staršího článku \cite{DG73}. Vychází z věrohodnostních funkcí 
\begin{align} \label{eq:01}
 L(\lambda,N) & = \ee^{-N\lambda} \prod_{x=1}^R \left[\frac{\lambda^x}{x!}\right]^{n_x}, \\
 L^*(\lambda,N) & = \binom{N}{n}p^n q^{N-n}, \label{eq:02}
\end{align}
kde $p =  1- \ee^{-\lambda}$, $q = 1 - p = \ee^{-\lambda}$. V rovnici \eqref{eq:01} jsme proti článku \cite{DG73} doplnili do exponentu hodnotu $n_x$, protože její absenci v~článku považujeme za tiskovou chybu. Bez tohoto exponentu by totiž věrohodnostní funkce neobsahovala informaci o počtu pozorování pro jednotlivé hodnoty $x$, což by nedávalo smysl.


\subsection*{Odvození věrohodnostních funkcí}
Funkce \eqref{eq:01} je klasickou věrohodnostní funkcí pro úplný náhodný výběr $Y_1, \ldots,$ $Y_N$ z Poissonova rozdělení:
\begin{align*}
L(\lambda,N) = \prod_{i=1}^N \ee^{-\lambda}\frac{\lambda^{Y_i}}{Y_i!} =  \ee^{-N\lambda} \prod_{x=1}^R \left[\frac{\lambda^x}{x!}\right]^{n_x}.
\end{align*}
Tato funkce je monotónní v proměnné $N$ a pokud bychom ji maximalizovali samu o sobě, vždy bychom jako odhad $N$ dostali nejmenší přípustnou hodnotu, tedy $n$. To není žádoucí a samotná funkce \eqref{eq:01} k odhadu nestačí.

Protože platí $p=\mathbb{P}(Y_i \neq 0)$ a $q = \mathbb{P}(Y_i = 0)$, popisuje funkce \eqref{eq:02} pravděpodobnost, že v úplném náhodném výběru o rozsahu $N$ je $n$ nenulových pozorování a~$N-n$ nulových pozorování. Přidání této funkce má umožnit lepší odhad parametru $N$.


\subsection*{Odhady parametrů}
Odhady $\widehat{\lambda}$ a $\widehat{N}$ mají podle článku maximalizovat rovnice \eqref{eq:01} a \eqref{eq:02} současně. Autoři pak bez odvození uvádějí, že výsledné odhady splňují vztahy
\begin{align}
    \frac{\widehat{\lambda}}{1- \ee^{-\widehat{\lambda}}} & = \frac{S}{n}, \\
    \widehat{N} & = \frac{n}{1- \ee^{-\widehat{\lambda}}}.
\end{align}


\subsection*{Pomocné lemma}
Při maximalizaci funkce \eqref{eq:02} využijeme následujícího pomocného lemmatu \cite[s.\,144]{San72}. V odkazovaném článku se objevuje bez důkazu jako \uv{známé}, pouze s~odkazem do práce \cite[s.\,142]{Chapman51}. Tam je však formulováno pro pravděpodobnosti v hypergeometrickém rozdělení. Upravenou verzi důkazu, odpovídající naší situaci, proto uvádíme níže.

\begin{lemma}  \label{L1}
Nechť $q(\lambda) = \ee^{-\lambda},$ $ p(\lambda) = 1-q(\lambda) =1-\ee^{-\lambda}$. Pro libovolné pevné $\lambda>0$, $\widehat{N}=\left\lfloor\frac{n}{p(\lambda)}\right\rfloor$ maximalizuje $L^*(\lambda,N) = \binom{N}{n}p(\lambda)^n q(\lambda)^{N-n}$. Pokud $p(\lambda)= \frac{n}{N'}$ pro nějaké $N' \in \mathbb{N}$, pak $\widehat{N}$ i $\widehat{N}-1$ maximalizují $L^*(\lambda,N)$. Jinak $\widehat{N}$ je jediné maximum. 
\end{lemma}
\begin{dukaz}
Zvolme nejprve pevnou hodnotu $\lambda > 0$. Pro přehlednost budeme dále psát pouze $p=p(\lambda),\, q=q(\lambda)$. Protože $N$ je celočíselná proměnná, budeme nejprve uvažovat podíl dvou následujících hodnot funkce $L^*$, tedy
\begin{align*}
C(N) &=\frac{L^*(\lambda,N+1)}{L^*(\lambda,N)} = \frac{\binom{N+1}{n}p^n q^{N+1-n}}{\binom{N}{n}p^n q^{N-n}} =\frac{q(N+1)}{(N+1-n)}, \quad N \geq n. 
\end{align*}
Dále si definujeme odpovídající funkci reálné proměnné $C(V) =\frac{q(V+1)}{(V+1-n)}$ pro $V \in \mathbb{R},\, V\geq n$. Její derivace je záporná a funkce $C(V)$ je proto klesající:
\begin{align*}
    C'(V) = \frac{-qn}{(V+1-n)^2} < 0, \quad V \geq n.
\end{align*}

Předpokládejme nyní, že $C(n)<1$. Z tohoto předpokladu plyne, že funkce $L^*$ nabývá maxima pro $N=n$. Platí
\begin{align*}
C(n) = q(n+1) & < 1, \\
	   - q(n+1) + (n+1) & > -1 + (n + 1), \\
	   n+1 & > \frac{n}{1-q}, \\
	   n+1 & > \frac{n}{p}.
\end{align*}
Současně platí, že $p<1$, a proto $\frac{n}{p} > n$. Platí tedy $\left\lfloor\frac{n}{p}\right\rfloor = n$. V tomto případě nemůže $L^*$ nabývat maxima ve dvou různých bodech. Tvrzení lemmatu tedy v tomto případě platí, protože funkce $L^*$ skutečně nabývá maxima v bodě $n=\left\lfloor\frac{n}{p}\right\rfloor$.

Dále předpokládejme $C(n) \geq 1$. Nyní hledáme $N$ takové, že $C(N) \geq 1$, $C(N+1) <1$. Takové $N$ existuje, protože $C$ je klesající funkce, $C(n) \geq 1$ a
\begin{align*}
  \lim_{N\to\infty} C(N) = q \ < \ 1.
\end{align*}
Hledaný bod maxima pak bude 
\begin{align}\label{eq:Nhat}
\widehat{N}=N+1,
\end{align}
jak je vidět z definice funkce $C(N)$. Řešíme tedy soustavu nerovnic
\begin{align*}
\frac{q(N+1)}{(N+1-n)} & \geq 1, \\
\frac{q(N+2)}{(N+2-n)} & < 1.
\end{align*}
Po úpravě dostáváme podmínky
\begin{align*} 
\frac{n}{1-q} \geq \ N+1 \ >\frac{n}{1-q} - 1.
\end{align*}

Pokud $1-q=\frac{n}{N'}$ pro nějaké $N' \in \mathbb{N}$, dostáváme
\begin{align} \label{uneq01}
N' \geq N +1  > N'-1.
\end{align}
Z toho plyne $N'= N+1$, neboť $N$ i $N'$ jsou přirozená čísla, a v nerovnici \eqref{uneq01} je neostrá nerovnost rovností. Vzhledem k \eqref{eq:Nhat} je tedy $\widehat{N} = N+1 = N'$. Z~podmínky $C(N)=1$ plyne, že $L^*(\lambda, \widehat{N})=L^*(\lambda, \widehat{N}-1)$. V obou těchto bodech je tedy maximum funkce $L^*(\lambda,N)$. V tomto případě platí, že $N'=n/(1-q)$, a proto $\widehat{N}=N'=\frac{n}{p}$.

Pokud $1-q \neq \frac{n}{N'}$ pro žádné $N' \in \mathbb{N}$, nemůže v \eqref{uneq01} nastat rovnost, a proto je
\[ \frac{n}{p} > \ N +1 \ >\frac{n}{p} - 1. \]
Vzhledem k \eqref{eq:Nhat} je pak $\widehat{N}=\left\lfloor\frac{n}{p}\right\rfloor$ jediné maximum. %\\
\end{dukaz}




\subsection*{Odvození tvaru odhadů ze staršího článku}

V článku \cite{DG73} chybí postup, jak přistoupit k maximalizaci funkcí $L$ a $L^*$, viz \eqref{eq:01} a \eqref{eq:02}. Pouze se zde objevuje zmínka, že odhady mají maximalizovat obě funkce současně. Nejprve se tedy pokusíme maximalizovat obě funkce jako funkce dvou proměnných současně, přičemž proměnnou $N$ budeme brát jako celočíselnou. Ukážeme ovšem, že tento postup nevede k cíli. Následně ukážeme, že odhady z článku je možné získat maximalizací obou funkcí vzhledem k $\lambda$ a volbě $\widehat{N}$ tak, aby obě funkce nabývaly maxima pro stejné $\widehat{\lambda}$.

\subsubsection*{První postup.}

Začneme maximalizací $L(\lambda,N)$ vzhledem k $N$ a uvažujeme podíl
\begin{align*} 
\frac{L(\lambda, N)}{L(\lambda,N-1)}= \frac{\ee^{-N\lambda}}{\ee^{-(N-1)\lambda}} = \ee^{-\lambda}<1, \quad \forall \lambda > 0.
\end{align*}
Tento podíl jako funkce $N$ je proto klesající a z toho plyne, že bodem maxima je $\widehat{N_1}=n$. Nyní zafixujme $N=\widehat{N_1}$ a hledejme odhad $\lambda$. Logaritmováním a~dalšími úpravami dostaneme
\begin{align*}
l(\lambda,\widehat{N_1}) &= \ln(L(\lambda,\widehat{N}_1)) = -\widehat{N_1}\lambda + \sum_{x=1}^R \big(x n_x\ln \lambda - n_x \ln(x!)\big), \\
 \frac{\partial{l}}{\partial{\lambda}}(\lambda,\widehat{N}_1) &= -\widehat{N_1} + \sum_{x=1}^R \frac{x n_x}{\lambda} = 0, \\
 \widehat{\lambda}_1 &= \sum_{x=1}^R \frac{x n_x}{\widehat{N_1}} = \sum_{x=1}^R \frac{x n_x}{n} = \frac{S}{n}.
\end{align*}
Tedy funkce $L(\lambda,N)$ je maximalizována pro $\lambda=\widehat{\lambda}_1=S/n$, $N = \widehat{N}_1 = n$.

Dále použijeme lemma \ref{L1} k maximalizaci $L^*$ podle N. Dostáváme, že pro pevně zvolené $\lambda>0$ je maximum této funkce v bodě $\widehat{N}=\left\lfloor{ \frac{n}{1-\ee^{-\lambda}} } \right\rfloor $ v~případě, že $\frac{n}{1-\ee^{-\lambda}}$ není celé číslo. Jinak jsou body maxima hodnoty $\frac{n}{1-\ee^{-\lambda}}$ a $\frac{n}{1-\ee^{-\lambda}}-1$. 

Dále hledejme hodnotu $\lambda$, pro kterou nabývá funkce $L^*(\lambda,\widehat{N})$ maxima. Existuje posloupnost $\lambda_i,\, i \in \mathbb{Z},$ taková, že pro každé $i \in \mathbb{Z}$ je $\lambda_i > 0$ a navíc $\frac{n}{1-\ee^{-\lambda_i}}$ je přirozené číslo (takovou posloupností je například $\lambda_i = \ln (1 + 1/i)$). Takových hodnot $\lambda_i$ je spočetně mnoho, protože funkce $\frac{n}{1-\ee^{-\lambda}}$ je monotónní funkcí $\lambda$.

Funkce $L^*(\lambda,\widehat{N})$ je na každém intervalu $(\lambda_i,\lambda_{i+1}) $ diferencovatelná a~klad\-ná, a proto lze na každém z těchto intervalů funkci $l^*(\lambda,\widehat{N}) $ derivovat. Logaritmus je rostoucí funkce, takže se zachová i monotonie na intervalech $(\lambda_i,\lambda_{i+1})$. Dostáváme
\begin{align*}
l^*(\lambda,\widehat{N}) &= \ln L^*(\lambda,\widehat{N}) = \ln \binom {\widehat{N}}{n} + n \ln(1-\ee^{-\lambda}) - (\widehat{N}-n)\lambda, \\
  \frac{\partial{l^*}}{\partial{\lambda}}(\lambda,\widehat{N}) &= \frac{n \ee^{-\lambda}}{1-\ee^{-\lambda}} - \widehat{N} + n = \\
& =  \frac{n}{1-\ee^{-\lambda}} - \frac{n}{1-\ee^{-\lambda_{i+1}}} > 0, \quad \forall \lambda \in (\lambda_i, \lambda_{i+1}).
\end{align*}
Z toho dostáváme, že funkce $L^*(\lambda, \widehat{N})$ je na každém takovém intervalu rostoucí, a~proto jedinými kandidáty na body maxima jsou hodnoty $\lambda_i$. 

Předpokládejme tedy, že nějaké konkrétní $\lambda_i$ je bodem maxima. Z lemmatu \ref{L1} a vlastnosti $ \left\lfloor{ \frac{n}{1-\ee^{-\lambda_i}} } \right\rfloor = \frac{n}{1-\ee^{-\lambda_i}},\, i \in \mathbb{Z},$ plyne, že máme dva odhady parametru $N$ maximalizující rovnici $L^*(\lambda_i,N)$ vzhledem k $N$: 
\begin{align*}
    \widehat{N_2} = \frac{n}{1-\ee^{-\lambda_i}}, \qquad \widehat{N_3} = \frac{n}{1-\ee^{-\lambda_i}} - 1.
\end{align*}
Jak ukážeme dále, oba tyto odhady jsou pro každé $\lambda_i$ v rozporu s odhady získanými maximalizací funkce $L$. To je zřejmé pro odhad $\widehat{N}_2$, který se nemůže rovnat odhadu $\widehat{N}_1 = n$, protože jmenovatel zlomku je vždy menší než 1.

Předpokládejme tedy, že $\widehat{N_3} = \widehat{N_1} = n$. Dostáváme tedy
\begin{align*}
   n &= \frac{n}{1-\ee^{-\lambda_i}} - 1, \\
   \frac{n}{n+1} &= 1-\ee^{-\lambda_i}, \\
   \ee^{-\lambda_i} &= \frac{1}{n+1}, \\
   \widehat{\lambda}_3 &= \ln(n+1).
\end{align*}
Jenže $\widehat{\lambda}_1 = S / n$ je racionální číslo, a tedy nemůže být rovno $\widehat{\lambda}_3$ pro žádné $n$.

Tedy funkce $L(\lambda,N)$ a $L^*(\lambda,N)$ nemohou mít maximum ve stejném bodě a nejde je proto maximalizovat současně. Poznámka v článku \cite{DG73} o současné maximalizace je tedy přinejmenším zavádějící.

\subsubsection*{Druhý postup.} Nyní ukážeme jiný postup, kterým se dostaneme k odhadům uvedeným v článku \cite{DG73}.
Nejprve maximalizujeme funkci $L(\lambda,N)$ vzhledem k $\lambda$. Logaritmováním a dalšími úpravami dostaneme
\begin{align}
\ln (L(\lambda,N)) &= l(\lambda,N) = -N\lambda + \sum_{x=1}^R \big(x n_x \ln \lambda - n_x\ln (x!)\big),  \nonumber \\
\frac{\partial{l}}{\partial{\lambda}}(\lambda,N) &= -N + \sum_{x=1}^R \frac{x n_x}{\lambda} = 0, \nonumber \\
N &= \sum_{x=1}^R \frac{x n_x}{\lambda} = \frac{S}{\lambda}. \label{eqqN}
\end{align}
Protože platí
\begin{align*}
    \frac{\partial^2{l}}{\partial{\lambda^2}}(\lambda,N) = - \frac{S}{\lambda^2} < 0, \quad \forall \lambda>0,
\end{align*}  
je pro každé pevné $N\in \mathbb{N}$ hodnota $\lambda=S/N$ bodem maxima funkce $L(\cdot,N)$.

Postupujme nyní analogicky pro funkci $L^*$:
\begin{align}
\ln(L^*(\lambda,N)) = l^*(\lambda,N) &= \ln \binom {N}{n} + n \ln(1-\ee^{-\lambda}) - (N-n)\lambda, \nonumber \\
\frac{\partial{l^*}}{\partial{\lambda}}(\lambda,N)& = \frac{n \ee^{-\lambda}}{1- \ee^{-\lambda}} - N + n =0, \nonumber \\
\frac{n}{1 - \ee^{-\lambda}} &= N. \label{eqql}
\end{align}
Platí
\begin{align*}
    \frac{\partial^2{l^*}}{\partial{\lambda^2}}(\lambda,N) = \frac{-n \ee^{-\lambda}(1- \ee^{-\lambda}) - n \ee^{-2\lambda}}{(1- \ee^{-\lambda})^2} = 
    \frac{-n \ee^{-\lambda}}{(1- \ee^{-\lambda})^2} < 0, \quad \forall \lambda>0,
\end{align*} 
a tedy pro pevné $N \in \mathbb{N}$ je hodnota $\lambda$ splňující \eqref{eqql} bodem maxima funkce $L^*(\cdot,N)$.

Dosazením vztahu~\eqref{eqqN} do \eqref{eqql} dostáváme
\begin{align*}
\frac{n}{1 - \ee^{-\lambda}} &= \frac{S}{\lambda} ,\\
\frac{\widehat{\lambda}}{1- \ee^{-\widehat{\lambda}}} &= \frac{S}{n}.
\end{align*}
Hodnotu $\widehat{\lambda}$ splňující předchozí rovnici pak dosadíme do \eqref{eqqN} a dostaneme hodnotu $\widehat{N}$. Dopracovali jsme se tedy k odhadům uvedeným v článku \cite{DG73}. Hodnota $\widehat{N}$ je tedy volena tak, aby se maxima funkcí $L(\cdot,\widehat{N})$ a $L^*(\cdot,\widehat{N})$ nabývala ve stejném bodě $\widehat{\lambda}$.



\section{Novější článek}
V této části podrobně odvodíme postup z novějšího článku \cite{BDG78}. Vychází z~věrohodnostních funkcí
\begin{align} \label{eq:03}
L_1(\lambda,N) = \binom{N}{n}p^n q^{N-n}, \\
L_2(\lambda) = \left(\frac{q}{p}\right)^n \prod_{x=1}^R \left[\frac{\lambda^x}{x!}\right]^{n_x}, \label{eq:04}
\end{align}
kde opět $p = 1- \ee^{-\lambda}$, $q = 1 - p = \ee^{-\lambda}$. Je vidět, že funkce \eqref{eq:03} odpovídá přesně funkci \eqref{eq:02} ze staršího článku. Odlišnost je ovšem mezi funkcemi \eqref{eq:04} a \eqref{eq:01}.


\subsection*{Odvození věrohodnostních funkcí}
Funkce \eqref{eq:03} se shoduje s~funkcí \eqref{eq:02} a opět udává pravděpodobnost, že v úplném náhodném výběru o rozsahu $N$ je $n$ nenulových pozorování a~$N-n$ nulových pozorování. Funkce \eqref{eq:04} odpovídá věrohodnostní funkci náhodného výběru $X_1, \ldots, X_n$ o rozsahu $n$ z useknutého Poissonova rozdělení: 
\begin{align*}
L_2(\lambda) = \prod_{i=1}^n \frac{\ee^{-\lambda}}{1-\ee^{-\lambda}}\frac{\lambda^{X_i}}{X_i!} = \frac{q^n}{p^n} \prod_{i=1}^n \frac{\lambda^{X_i}}{X_i!} = \left(\frac{q}{p}\right)^n \prod_{x=1}^R \left[\frac{\lambda^x}{x!}\right]^{n_x}.
\end{align*}

Všimněme si, že rovnice \eqref{eq:04} nezávisí na $N$. Proto z této jedné rovnice můžeme získat odhad $\widehat{\lambda}$, dosadit jej do \eqref{eq:03} a odtud spočítat odhad $\widehat{N}$. Odhad $\widehat{N}$ je tedy získán metodou podmíněné maximální věrohodnosti \cite[s.\,182]{BDG78}.


\subsection*{Odhady parametrů}
Odhady $\widehat{\lambda}$ a $\widehat{N}$ mají podle článku maximalizovat funkce \eqref{eq:03} a \eqref{eq:04} současně. Autoři pak bez odvození uvádějí, že výsledné odhady splňují
\begin{align}
    \frac{\widehat{\lambda}}{1- \ee^{-\widehat{\lambda}}} & = \frac{S}{n}, \\
    \widehat{N}_2 & = \left\lfloor \frac{n}{1- \ee^{-\widehat{\lambda}}} \right\rfloor.
\end{align}





\subsection*{Odvození tvaru odhadů z novějšího článku}

Článek \cite{BDG78} uvádí, že se odhad $\widehat{\lambda}$ získá maximalizací $L_2(\lambda)$ a odhad $\widehat{N}$ pak následnou maximalizací $L_1(\widehat{\lambda},N)$. Je tedy
\begin{align} 
l_2 (\lambda) &= \ln (L_2(\lambda)) = n \ln\left(\frac{q}{p}\right) + \sum_{x=1}^R x n_x \ln\lambda - \sum_{x=1}^R n_x \ln(x!), \nonumber \\
l_2 (\lambda) &= n \ln(\ee^{-\lambda}) - n \ln (1- \ee^{-\lambda}) + S \ln\lambda - \sum_{x=1}^R n_x \ln(x!), \nonumber \\
l_2 (\lambda) &= -n\lambda - n \ln(1-\ee^{-\lambda}) + S \ln\lambda - \sum_{x=1}^R n_x \ln(x!), \nonumber \\
\frac{\partial{l_2}}{\partial{\lambda}}(\lambda) &= -n +\frac{-n \ee^{-\lambda}}{1- \ee^{-\lambda}}  + \frac{S}{\lambda}, \nonumber  \\
\frac{\partial{l_2}}{\partial{\lambda}}(\lambda) &= \frac{-n}{1- \ee^{-\lambda}} + \frac{S}{\lambda} = 0. \label{eqqqq}
\end{align}
Hodnota $\lambda$ splňující předchozí vztah je hledaným odhadem a splňuje také, po jednoduché úpravě, vztah
\begin{equation} \label{cond01}
\frac{\widehat{\lambda}}{1- \ee^{-\widehat{\lambda}}} = \frac{S}{n}.
\end{equation}

Dále, funkce \eqref{eqqqq} je kladná, resp. záporná, pokud je funkce $\frac{S}{n} - \frac{\lambda}{1-\ee^{-\lambda}}$ kladná, resp. záporná. Protože funkce $ \frac{\lambda}{1- \ee^{-\lambda}} $ je rostoucí, je
\begin{align*}
    \frac{S}{n}- \frac{\lambda}{1- \ee^{-\lambda}}>0 \ \text{pro} \  \lambda<\widehat{\lambda}, \\
    \frac{S}{n} -\frac{\lambda}{1- \ee^{-\lambda}}<0 \  \text{pro} \  \lambda>\widehat{\lambda}.
\end{align*}
V bodě $\widehat{\lambda}$ tedy nabývá funkce $L_2(\lambda)$ maxima. Zbývá ještě ukázat, kdy vůbec existuje nějaké $\widehat{\lambda}>0$, splňující \eqref{cond01}. Platí
\begin{align*}
\lim_{\lambda \to 0+} \frac{\lambda}{1- \ee^{-\lambda}} = 1 , \\
\lim_{\lambda \to \infty} \frac{\lambda}{1- \ee^{-\lambda}} = +\infty,
\end{align*}
a proto pro $S > n$ hledané $\widehat{\lambda}$ vždy existuje.

Použitím lemmatu~\ref{L1} na $L_1(\lambda,N)$ nakonec dostáváme, že pro $\lambda=\widehat{\lambda}$ je maximum funkce $L_1(\widehat{\lambda},N)$ v bodě
\begin{align*} 
\widehat{N}= \left\lfloor{\frac{n}{\widehat{p}}}\right\rfloor= \left\lfloor{\frac{n}{1-\ee^{-\widehat{\lambda}_c }}}\right\rfloor=
\left\lfloor{ \frac{\sum_{x=1}^R x n_x}{\widehat{\lambda_c}} } \right\rfloor.
\end{align*}

Poznamenejme ještě, že v případě, kdy $\frac{n}{\widehat{p}}$ je přirozené číslo, odhad $\widehat{N}$ není dle lemmatu~\ref{L1} jednoznačný, protože $L_1(\lambda,N)$ nemá jednoznačné maximum. V takovém případě i $\widehat{N}-1$ je maximálně věrohodným odhadem $N$.


Odhad $\widehat{\lambda}$ lze získat z rovnice \eqref{cond01} numerickou metodou, případně podle článku \cite{Irwin} existuje analytické vyjádření odvozené pomocí Lagrangeových řad:
\[ \widehat{\lambda} = \overline{X}^* - \sum_{j=1}^{\infty} \frac{j^{j-1}}{j!}
\big(\overline{X}^* \ee^{-\overline{X}^*}\big)^j, \]
kde $\overline{X}^* = \frac{S}{n}$.



\section{Ilustrační příklad}
Uveďme pro zajímavost ještě příklad převzatý z \cite{Cohen1960}, který jej převzal z \cite{Bortkiewicz}.

V pruské armádě se v letech 1875--1894 sbírala data o počtu vojáků, kteří zemřeli na následky kopnutí koněm. Data se sbírala v 10 různých vojenských jednotkách po dobu 20 let. Celkem bylo nahlášeno $S=122$ smrtí v $N=200$ výročních zprávách. Tabulka~\ref{tab:01} obsahuje nahlášené četnosti úmrtí v jedné jednotce za rok.


Předpokládejme, že počet vojáků zemřelých na následky kopnutí koněm má Poissonovo rozdělení. My se budeme tvářit, že neznáme celkový počet výročních zpráv $N$ a~zkusíme tuto hodnotu odhadnout pomocí postupu z~novějšího článku \cite{BDG78}. Dostáváme pak hodnoty $\widehat{\lambda} = 0{,}618$ a $\widehat{N} = 197$. Odhad~$N$ je tedy velmi blízký skutečné hodnotě 200. Ještě doplníme, že maximálně věrohodný odhad $\lambda$ z úplného výběru je pak $S/N=0{,}610$, tedy i odhad $\widehat{\lambda}$ je uspokojivý.

Ještě můžeme doplnit odhady parametrů pro příklad zmíněný v úvodu (výskyt cholery v domácnostech v indické vesnici). Postupem podle novějšího článku získáme odhady $\widehat{\lambda} = 0{,}972$ a $\widehat{N} = 88$. Odhadujeme tedy, že cholerou bylo zasaženo dalších $\widehat{N} - n = 88 - 55 = 33$ domácností, ve kterých zatím nikdo neměl příznaky.

\begin{table}
\centering
\renewcommand{\arraystretch}{1.2}
\begin{tabular}[t]{r|r}
$x$ & $n_x$ \\ \hline
0 & 109 \\
1 & 65 \\
2 & 22 \\
3 & 3 \\
4 & 1 \\
\end{tabular}
\caption{\label{tab:01}Nahlášené četnosti úmrtí -- kopnutí koněm v pruské armádě.}
\end{table}




\section{Závěr}
Naše detektivní pátrání ukázalo, že mezi postupy v článcích \cite{BDG78} a \cite{DG73} není žádný vztah, přestože autoři v textu tvrdí, že jsou postupy shodné. Články vychází z mírně odlišných věrohodnostních rovnic a dospívají k mírně odlišným odhadům parametru $N$. Postup ze staršího článku \cite{DG73} není z našeho pohledu zcela korektní, protože nedochází k žádné maximalizaci v proměnné~$N$, odhad $\widehat{N}$ je volen tak, aby obě věrohodnostní funkce jako funkce $\lambda$ nabývaly maxima ve stejném bodě. Postup z novějšího článku \cite{BDG78} je přímočařejší a~přirozeně vede k tomu, že odhad $\widehat{N}$ je přirozené číslo. Z těchto dvou možností tedy doporučujeme využít postup podle novějšího článku \cite{BDG78}.



%\bigskip

%\Podekovani{Pokud chcete na závěr někomu poděkovat, například poskytovateli prostředků, které byly použity při realizaci projektu, jehož je článek výstupem, můžete tak učinit zde.}

%\bigskip
\selectlanguage{english}

\renewcommand{\refname}{Literatura}
\begin{thebibliography}{9}
%\setlength\itemsep{0mm}

\bibitem{BDG78}
  Blumenthal, S., Dahiya, R.\,C., Gross, A.\,J. (1978): Estimating the complete sample size from an incomplete {P}oisson sample. {\it J. Amer. Statist. Assoc.} {\bf 73}, 182--187.
  
\bibitem{Bortkiewicz}
  Bortkiewicz, L. (1898): {\it Das Gesetz der Kleinen Zahlen.} B.\,G. Teubner, Leipzig, 1898.
  
\bibitem{Chapman51}
  Chapman, D.\,G. (1951): Some properties of the hypergeometric distribution with applications to zo\"ological sample censuses. {\it Univ. California Publ. Statist.} {\bf 1}, 131--159.
  
\bibitem{Cohen1960}
  Cohen, A.\,C. (1960): Estimating the parameter in a conditional {P}oisson distribution. {\it Biometrics} {\bf 16}, 203--211.
  
\bibitem{DG73}
  Dahiya, R.\,C., Gross, A.\,J. (1973): Estimating the zero class from a truncated {P}oisson sample. {\it J. Amer. Statist. Assoc.} {\bf 68}, 731--733.
  
\bibitem{Irwin}
  Irwin, J.\,O. (1959): 138. Note: On the estimation of the mean of a {P}oisson distribution from a sample with the zero class missing. {\it Biometrics} {\bf 15}, 324--326.
  
\bibitem{San72}
  Sanathanan, L. (1972): Estimating the size of a multinomial population. {\it Ann. Math. Statist.} {\bf 43}, 142--152.
  
\bibitem{Zeman2018}
  Zeman, O. (2018): {\it Neúplné vzorky z Poissonova rozdělení.} Bakalářská práce, MFF UK, Praha, 2018.
  
\end{thebibliography}



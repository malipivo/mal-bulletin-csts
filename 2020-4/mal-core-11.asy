if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-11";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

import graph3;
texpreamble("\usepackage{amsmath}");
unitsize(1.25cm);
currentprojection=orthographic(2.25,1.75,1);

//function: call the radial coordinate r=t.x and the angle phi=t.y
triple f(pair t) { real xx=t.x;
return ((xx)*cos(t.y), (xx)*sin(t.y), (xx)^2*sin(t.y)*cos(t.y));}
surface s1=surface(f,(0,-pi/4),(1.5,pi/4),6,6,
usplinetype=new splinetype[] {notaknot,notaknot,monotonic},
vsplinetype=Spline);
draw(s1,blueII+opacity(.5),meshpen=PPblueblackI+thick(),nolight,render(merge=true));
surface s2=surface(f,(0,3*pi/4),(1.5,5*pi/4),6,6,
usplinetype=new splinetype[] {notaknot,notaknot,monotonic},
vsplinetype=Spline);
draw(s2,blueII+opacity(.5),meshpen=PPblueblackI+thick(),nolight,render(merge=true));

triple f(pair t) { real xx=t.x;
return ((xx)*cos(t.y), (xx)*sin(t.y), 0);}
surface n1=surface(f,(0,pi/4),(1.5,3*pi/4),6,6,
usplinetype=new splinetype[] {notaknot,notaknot,monotonic},
vsplinetype=Spline);
draw(n1,blueII+opacity(.5),meshpen=PPblueblackI+thick(),nolight,render(merge=true));
surface n2=surface(f,(0,5*pi/4),(1.5,7*pi/4),6,6,
usplinetype=new splinetype[] {notaknot,notaknot,monotonic},
vsplinetype=Spline);
draw(n2,blueII+opacity(.5),meshpen=PPblueblackI+thick(),nolight,render(merge=true));

draw(scale(.05,.05,.05)*unitsphere,PPblueblack+1);
label(scale(.6)*"$\boldsymbol{0}$",(0,0,0),.5X+.25Y-.5Z);
label(scale(.6)*"$f$",(-1.3,-.75,.975),-.5X+.25Z);

draw(Label(scale(.6)*"$-1$",position=EndPoint),(.05,0,-1)--(-.05,0,-1));
draw(Label(scale(.6)*"$1$",position=EndPoint),(.05,0,1)--(-.05,0,1));
draw(Label(scale(.6)*"$1$",position=EndPoint),(1,0,.05)--(1,0,-.05));
draw(Label(scale(.6)*"$-1$",position=EndPoint),(-1,0,.05)--(-1,0,-.05));
draw(Label(scale(.6)*"$1$",position=EndPoint),(0,1,.05)--(0,1,-.05));
draw(Label(scale(.6)*"$-1$",position=EndPoint),(0,-1,.05)--(0,-1,-.05));

draw(Label(scale(.65)*"$x$",position=EndPoint),(-1.75,0,0)--(1.75,0,0),Arrow3);
draw(Label(scale(.65)*"$y$",position=EndPoint),(0,-1.75,0)--(0,1.75,0),Arrow3);
draw(Label(scale(.65)*"$z$",position=EndPoint),(0,0,-1.25)--(0,0,1.75),Arrow3);

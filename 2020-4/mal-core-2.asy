if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-2";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

real cc=1.5,u=5,v=3,rv=u/v,rm=1,rt=2*u,rp=rv-rm;int n=90;

import graph;

usepackage("animate");settings.tex="lualatex";defaultpen(.25);import animation;

size(0cm,6.cm);
pair wheelpoint(real t)
{return (rp*cos(t*rm/rv)+cc*cos(rp*t/rv),rp*sin(t*rm/rv)-cc*sin(rp*t/rv));}

guide wheel(guide g=nullpath,real a,real b,int n)
{real width=(b-a)/n;for(int i=0;i<=n;++i)
{real t=a+width*i;g=g--wheelpoint(t);} return g;}
real tinterval=0*rt*pi,t1=0,t2=t1+tinterval;draw(circle((0,0),rv),olive+.75);
real t1=8.8*pi/3; //navyse

animation a;
pair z1=wheelpoint(t1);dot(z1,red);real dt=(t2-t1)/n;

for(int i=0;i<=n;++i) {save();
real t=t1+dt*i,kx=rp*cos(rm*t/rv),ky=rp*sin(rm*t/rv);
filldraw(circle((kx,ky),cc),.2paleblue+white,.2paleblue+white+.5);
draw((0,0)--(rv*cos(rm*t/rv),rv*sin(rm*t/rv)),lightblue);
if (t>0) {filldraw((kx,ky)--arc((kx,ky),rm,180*rm*t/rv/pi,-180*rp*t/rv/pi)--cycle,white+.75blue+opacity(.25),drawpen=lightblue);}

draw(circle((0,0),rv),olive+.75);label("$K$",(-.6*rv,-.75*rv),SW,olive);
draw(circle((0,0),rp),dotted+blue+white);
draw(circle((0,0),rp-cc),yellow+.35red);draw(circle((0,0),rp+cc),yellow+.35red);

label("$x$",(rv+.25,0),N);draw((-rv-.25,0)--(rv+.25,0));
label("$y$",(0,rv+.25),W);draw((0,-rv-.25)--(0,rv+.25));

draw(wheel(0,10*pi,8*n),dotted+red);draw(circle((kx,ky),rm),blue+.75);
label("$k$",(kx-.6,ky-.75),SW,blue);draw((kx,ky)--wheelpoint(t),black+.625);
dot((kx,ky));dot(wheelpoint(t),red+black);draw(wheel(t1,t,8*max(1,i)),red+.5);

dot(wheelpoint(0),red+black);draw(wheel(0,t1,8*n),red+.5); //navyse
label("\scriptsize$t="+string(t,7)+"$",(.3*rv,-rv),SE,blue);
a.add();restore();} erase();

//...............toto pouzit pri online verzii (pri tlaci sa nezobrazi menu s animate) a TiKz obrazok zapoznamkovat
label(a.pdf(delay=250,"buttonsize=10pt,controls,loop,palindrome",multipage=false));

//...............toto pouzit pri tlacenej verzii (dole je pomocou tikz nakreslene menu)
//label(a.pdf(delay=250));

if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-5";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

import graph3;
import palette;

size(175,175,IgnoreAspect);

currentprojection=orthographic(2,.55,.25);

real f(pair z) {return sin(z.x)*sin(z.y);}

draw(surface(f,(-pi,-pi),(pi,pi),nx=24,Spline),blueII+opacity(.5),meshpen=PPblueblackI+thick(),nolight,render(merge=true));

draw(Label("$x$",1),(-1.2pi,0,0)--(1.2pi,0,0),brown,Arrow3);
draw(Label("$y$",1),(0,-1.2pi,0)--(0,1.2pi,0),brown,Arrow3);
draw(Label("$z$",1),(0,0,-1.2)--(0,0,1.2),brown,Arrow3);
label("$z=\sin{x}\cdot\sin{y}$",(.5,.5,-1.35),SE,deepblue);

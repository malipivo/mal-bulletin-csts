if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-8";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

import graph3;
size(3.5cm,0);
currentprojection=orthographic(1.25,1,2.5);

draw(shift(.2X+.2Y)*surface(scale(.75,1.2)*unitcircle),blackII+opacity(.25),nolight);

//function: call the radial coordinate r=t.x and the angle phi=t.y
triple f(pair t) { real xx=t.x;
return ((xx)*cos(t.y), (xx)*sin(t.y), ((xx)*cos(t.y))^2-((xx)*sin(t.y))^2+3);}
surface s=surface(f,(0,0),(1,2*pi),8,8,
usplinetype=new splinetype[] {notaknot,notaknot,monotonic},
vsplinetype=Spline);
draw(shift(.2X+.2Y)*scale(.75,1.2,1)*s,blueII+opacity(.5),meshpen=PPblueblackI+thick(),nolight,render(merge=true));

real x(real t) {return cos(t);}
real y(real t) {return sin(t);}
real z(real t) {return 0;}
real w(real t) {return cos(t)*cos(t)-sin(t)*sin(t)+3;}
path3 p1=graph(x,y,z,0,2*pi,operator ..);
draw(shift(.2X+.2Y)*scale(.75,1.2,1)*p1,PPblueblackI);
path3 p2=graph(x,y,w,pi/2,pi,operator ..);
draw(shift(.2X+.2Y)*scale(.75,1.2,1)*p2,PPblueblack+1.5);

draw(Label(scale(.65)*"$0$",position=EndPoint),(0,0,.05)--(0,0,-.15));
draw(Label(scale(.65)*"$1$",position=EndPoint),(.05,0,1)--(-.05,0,1));
draw(Label(scale(.65)*"$2$",position=EndPoint),(.05,0,2)--(-.05,0,2));
draw(Label(scale(.65)*"$3$",position=EndPoint),(.05,0,3)--(-.05,0,3));
draw(Label(scale(.65)*"$4$",position=EndPoint),(.05,0,4)--(-.05,0,4));
draw(Label(scale(.65)*"$1$",position=EndPoint),(1,0,.05)--(1,0,-.05));
draw(Label(scale(.65)*"$-1$",position=EndPoint),(-1,0,.05)--(-1,0,-.05));
draw(Label(scale(.65)*"$1$",position=EndPoint),(0,1,.05)--(0,1,-.05));
draw(Label(scale(.65)*"$-1$",position=EndPoint),(0,-1,.05)--(0,-1,-.05));

draw(Label(scale(.75)*"$x$",position=EndPoint),(-1.25,0,0)--(1.75,0,0),Arrow3);
draw(Label(scale(.75)*"$y$",position=EndPoint),(0,-1.25,0)--(0,2,0),Arrow3);
draw(Label(scale(.75)*"$z$",position=EndPoint),(0,0,-.25)--(0,0,5.1),Arrow3);

label(scale(.65)*"$A$",.8X-.2Y,Z,PPblueblackI);
label(scale(.65)*"$f(A)$",.8X-.2Y+3.25Z,-Z,PPblueblackI);

draw(shift(-.55X+.2Y+4Z)*scale(.05,.05,.05)*unitsphere,PPblueblack+1);
label(scale(.65)*"$\max{f(A)}$",-.55X+.2Y+4.2Z,Z,PPblueblack);
draw(shift(.2X+1.4Y+2Z)*scale(.05,.05,.05)*unitsphere,PPblueblack+1);
label(scale(.65)*"$\min{f(A)}$",.2X+1.4Y+1.8Z,-Z,PPblueblack);

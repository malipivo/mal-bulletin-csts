#!/usr/bin/bash

echo Metapost...
mpost 010.mp
lualatex 010-metapost.tex
lualatex 010-metapost.tex

echo PSTricks...
latex 020.tex
dvips 020.dvi
ps2pdf 020.ps
lualatex 020-pstricks.tex
lualatex 020-pstricks.tex

echo Asymptote...
asy -vk 030.asy
for soubor in `find -iname _030\*.eps`; do
  core=${soubor%.eps}
  echo $soubor
  ps2pdf $soubor
  pdfcrop --hires $core
  mv $core-crop.pdf $core.pdf
done
lualatex 030-asymptote.tex
lualatex 030-asymptote.tex

echo TikZ...
lualatex 040.tex
lualatex 040.tex
pdfcrop --hires 040.pdf
mv 040-crop.pdf 040.pdf
lualatex 040-tikz.tex
lualatex 040-tikz.tex


echo Zatěžkávací test s písmy...
lualatex 100-pisma.tex
pdf2svg 100-pisma.pdf 100-pisma-pdf2svg.svg
dvilualatex 100-pisma.tex
#dvisvgm -n --font-format=woff2 --exact --zoom=-1 --page=- -o 100-pisma-bezpisem.svg 100-pisma.xdvi
dvisvgm --font-format=woff2 --exact --zoom=-1 --page=- -o 100-pisma-spismy.svg 100-pisma.dvi
dvisvgm -n --exact --zoom=-1 --page=- -o 100-pisma-prespdf1.svg --pdf 100-pisma.pdf
dvisvgm -n --exact --zoom=-1 --page=- -o 100-pisma-prespdf2.svg --pdf --transform="R20,w/3,2h/5 T1cm,1cm S2,3" 100-pisma.pdf

exit 1
# overall cleaning
rm -f *.{aux,log,dvi,mpx}
rm -f mpxerr.tex

rm -f *.mps
rm -f 020.ps
rm -f {,_}*.{eps,prc}
rm -f _*.pdf
rm -f 030.gif

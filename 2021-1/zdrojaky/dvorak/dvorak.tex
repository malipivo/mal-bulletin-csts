% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode 
% !TEX root = ../../mal-core.tex

\newdimen\malwidth \malwidth=0.9\textwidth

\gdef\mujnazevCS{Využití $\chi^2$ testů v~prostorové statistice}
\gdef\mujnazevEN{$\chi^2$ tests in spatial statistics}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Martina Petráková, Jiří Dvořák}

\bonushyper

\nazev{\mujnazevCS}

\nazev{\mujnazevEN}

\author{\mujauthor}

\Adresa{Matematicko-fyzikální fakulta Univerzity Karlovy, Sokolovská 83, 186\,75 Praha~8}

\Email{martinapetrakova@centrum.cz}, 
\href{mailto:dvorak@karlin.mff.cuni.cz}{\texttt{dvorak@karlin.mff.cuni.cz}}

\Abstrakt{V~tomto článku představíme několik možností uplatnění známého $\chi^2$~testu při řešení problémů z~prostorové statistiky. Nejprve zavedeme pojem bodového procesu a~jeho funkce intenzity a~představíme dva příklady bodových procesů -- binomický a~Poissonův bodový proces. Budeme se zabývat testováním, zda pozorovaná data pochází z~Poissonova procesu, zda je pro~\mbox{Poissonův} proces funkce intenzity konstantní, zda závisí na dané kovariátě, zda je v~předem určeném tvaru, závislém pouze na konečném počtu parametrů, či zda je v~součinovém tvaru. Použití vybraných testů předvedeme pro~vhodná reálná data.}
\smallskip
\KlicovaSlova{prostorová statistika, bodový proces, Poissonův proces, binomický proces, funkce intenzity, $\chi^2$ test}


\Abstract{We present several ways of using the well-known $\chi^2$~test to solve problems in spatial statistics. First we introduce the notion of a~point processes and its intensity function and we provide two examples of point processes -- binomial and Poisson point process. We consider the problem of~testing the hypotheses that the observed data comes from a~Poisson process, that for~a~Poisson process the intensity function is constant, that it depends on~a~given covariate, that it has a~specified form, depending only on finitely many parameters, or that it is in the product form. For some of the tests we illustrate their use for appropriate real datasets.}
\smallskip
\KeyWords{spatial statistics, point process, Poisson process, binomial process, intensity function, $\chi^2$ test}


\section{Úvod}\label{sec01}
Chceme-li zkoumat data, která zachycují výskyt nějakých jevů v~čase (např. příchod zákazníků), v~prostoru (např. pozice hnízd daného druhu ptáků) nebo časoprostoru (např. zemětřesení), můžeme využít teorii bodových procesů a~odpovídající postupy prostorové statistiky. Bodové procesy popisují náhodné množiny bodů a~své využití proto mají v~řadě oborů, jmenovitě například v~epidemiologii (výskyt nakažených jedinců), ekologii (pozice stromů daného druhu), medicíně (prostorové uspořádání neuronů), materiálových vědách (výskyt poruch v~krystalové mřížce) a~mnoha dalších. Pozorovaná data pak tvoří pozice všech bodů procesu, které se vyskytují v~zadaném kompaktním pozorovacím okně. Jak poloha pozorovaných bodů tak jejich počet jsou náhodné a~je obvyklé, že mezi body dochází k~interakcím -- body se mohou přitahovat nebo naopak odpuzovat. Tyto vlastnosti (náhodný počet pozorovaných bodů, závislosti mezi jednotlivými body) vedou k~tomu, že typicky není možné pro modelování takových dat použít náhodný výběr a~k~analýze využít postupy klasické statistiky. V~tomto článku se zaměříme na případ, kdy tyto postupy využít umíme. 

Nejprve představíme dva příklady bodových procesů -- {\it binomický proces} a~{\it Poissonův bodový proces} -- a~jednu z~jejich základních charakteristik, {\it funkci intenzity}, která popisuje očekávaný počet pozorovaných bodů v~dané oblasti. Budeme se zabývat otázkou, co na základě pozorovaných dat umíme říci o~funkci intenzity, například zda je možné předpokládat, že je konstantní a~bodový proces je tedy tzv. {\it homogenní}. Jako základní model přitom budeme předpokládat právě Poissonův bodový proces, který má tu důležitou vlastnost, že mezi jeho body nejsou žádné interakce. Přítomnost bodu v~daném místě tedy nijak neovlivňuje přítomnost dalších bodů v~jeho okolí. Tato vlastnost bude stěžejní k~tomu, abychom mohli použít výsledky odvozené pro~náhodné výběry a~využít známého postupu $\chi^2$~testu na vybrané problémy. Klíčovou roli zde hraje podmíněné rozdělení Poissonova procesu při~daném počtu pozorovaných bodů, které je popsáno ve~větě~\ref{vetapodminenerozděleni}.

Ve statistice bodových procesů je běžné, že máme k~dispozici pouze jednu pozorovanou realizaci, tj. jednu množinu bodů pozorovanou v~zadaném pozorovacím okně. Pro tuto situaci v~sekci 3 sestavíme formální testy hypotéz, že homogenní bodový proces (tedy proces s~konstantní funkcí intenzity) je Poissonův (sekce \ref{sekcepoissonovskost}), že je Poissonův bodový proces homogenní (sekce \ref{sekcehomogenita}), že rozdělení Poissonova procesu závisí na dané kovariátě (sekce \ref{sekcekovariata}), že je funkce intenzity Poissonova procesu v~daném tvaru závisejícím na neznámých parametrech (sekce \ref{sekcetestdobreshody}), nebo že je funkce intenzity Poissonova procesu v~součinovém tvaru (sekce \ref{sekceseparabilita}). Některé z~těchto testů si také ukážeme na~jednoduchých reálných příkladech.


\section{Bodové procesy}
\emph{Bodový proces} $\mathbb{X}$ je náhodná lokálně konečná množina bodů v~$\mathbb{R}^d,\ d \in \mathbb{N}$. Formálně jde o~zobrazení mezi pravděpodobnostním prostorem a~prostorem lokálně konečných množin vybaveným vhodnou $\sigma$-algebrou. Pro úplnost dodáváme, že pro lokálně konečnou množinu má její průnik s~libovolným kompaktem pouze konečný počet bodů a~můžeme si ji tedy představit jako množinu izolovaných bodů v~prostoru. Ukázky realizací různých bodových procesů v~rovině nabízí obrázky~\ref{obrfunkceint}, \ref{obrbinxpoisson} a~\ref{obrpoissxthomas}.

Zde se omezujeme na bodové procesy v~euklidovském prostoru. Náhodná je nejen poloha bodů procesu, ale také jejich počet v~podmnožinách $\mathbb{R}^d$. Pozorujeme-li tedy nějakou oblast $W \subset \mathbb{R}^d$ (pozorovací okno), pak počet bodů procesu~$\mathbb{X}$ v~množině $W$ je náhodná veličina, kterou označíme $\mathbb{X}(W)$. V~článku se budeme věnovat převážně bodovým procesům v~rovině ($d=2$) a~k~procesům ve vyšší dimenzi se dostaneme v~části \ref{sekceseparabilita} Všechny zde uvažované množiny budou borelovsky měřitelné.

Jednou ze základních popisných charakteristik bodového procesu je jeho \emph{míra intenzity} $\Lambda$, která popisuje očekávaný počet bodů procesu v~dané mno\-žině. Pro množinu $B \subset \mathbb{R}^d$ pak platí $\Lambda (B) = \mathbb{E} \mathbb{X}(B)$. Pokud je míra intenzity absolutně spojitá vzhledem k~$d$-rozměrné Lebesgueově míře, říkáme její hustotě $\lambda(u),\ u\in \mathbb{R}^d$, \emph{funkce intenzity} bodového procesu. Platí pak
\begin{align*}
  \Lambda (B) = \int_{B} \lambda(u) \, \mathrm{d}u.
\end{align*}

Je-li funkce $\lambda(u)$ konstantní, t.j. $\lambda(u) = \lambda$ pro všechna $u \in \mathbb{R}^d$, hovoříme o~\emph{homogenním} bodovém procesu a~konstantu $\lambda$ nazýváme \emph{intenzita} (pro míru intenzity pak platí $\Lambda(B) = \lambda |B|$, kde $|B|$ je Lebesgueova míra množiny $B$). V~opačném případě hovoříme o~\emph{nehomogenním} bodovém procesu.  Obrázek~\ref{obrfunkceint} ukazuje příklad realizace homogenního a~nehomogenního bodového procesu a~vliv funkce intenzity na rozmístění bodů.

\begin{figure}[!htb]
%\begin{center}
\centering
  \includegraphics[width=\malwidth]{Poissonint.pdf}
  \caption{Ukázka realizace homogenního (vlevo) a~nehomogenního bodového procesu v~$\mathbb{R}^2$ (vpravo) v~pozorovacím okně $W=[0,1]^2$. Vlevo vidíme realizaci Poissonova procesu (viz část \ref{defpoisson}) s~intenzitou $\lambda=100$, vpravo pak realizaci Poissonova procesu s~funkcí intenzity $\lambda(x,y)=500\cdot|x-y|$.}\label{obrfunkceint}
%\end{center}
\end{figure}



\subsection{Binomický bodový proces}
Mějme nyní pozorovací okno $W \subset \mathbb{R}^d$ a~$n$ nezávislých, stejně rozdělených náhodných vektorů $X_{1}, \dots, X_{n}$ s~hodnotami v~tomto okně. Tyto vektory budou reprezentovat jednotlivé body procesu. Náhodná množina bodů $\{X_{1}, \dots, X_{n}\}$ pak představuje (v~mnoha ohledech nejjednodušší) příklad bodového procesu, tzv. \emph{binomický proces}. Pro počet bodů $\mathbb{X}(B)$ binomického procesu $\mathbb{X}$ v~nějaké množině $B \subset W$ platí 
\begin{align*}
 \mathbb{X}(B) = \sum_{i = 1}^{n} \mathbbm{1} \{X_{i} \in B\},
\end{align*}
kde $\mathbbm{1}$ značí indikátorovou funkci. Protože $X_{i}$ jsou nezávislé, stejně rozdělené náhodné vektory, jsou $ \mathbbm{1} \{X_{i} \in B\}$ také nezávislé, stejně rozdělené náhodné veličiny s~alternativním rozdělením. Proto $\mathbb{X}(B)$ má binomické rozdělení s~parametry $n$ a~$\Lambda (B)/\Lambda (W)$.



\subsection{Poissonův bodový proces} \label{defpoisson}
Dalším, pro tento článek stěžejním příkladem bodového procesu je \emph{Poissonův proces}. Bodový proces $\mathbb{X}$ je Poissonův proces s~mírou intenzity $\Lambda$, pokud má následující vlastnosti:
\begin{itemize}
	\item[P1:] Počet bodů $\mathbb{X}(B)$ v~množině $B \subset \mathbb{R}^d$ je náhodná veličina s~Poissonovým rozdělením s~parametrem $\Lambda (B)$.
	\item[P2:] Pro množiny $B_1, \dots, B_m$, $m \in \mathbb{N}$, po dvou disjunktní, jsou náhodné veličiny $\mathbb{X}(B_1),\dots, \mathbb{X}(B_m)$ nezávislé.
\end{itemize}

V~Poissonově procesu nejsou žádné interakce mezi body, ani přitažlivé, ani odpudivé, což je důsledkem nezávislosti počtu bodů v~disjunktních množinách. Naopak v~binomickém procesu jsou jednotlivé body umisťovány nezávisle na sobě, vzhledem k~pevnému celkovému počtu bodů v~zadaném okně ale nejsou počty v~disjunktních podmnožinách tohoto okna nezávislé. Volně řečeno, pokud už je bod umístěn v~jedné podmnožině, nemůže být umístěn v~jiné, s~ní disjunktní.

Příklad realizace Poissonova a~binomického procesu se stejnou intenzitou ukazuje obrázek \ref{obrbinxpoisson}. Následující věta popisuje vztah mezi těmito dvěma bodovými procesy. Stručně řečeno věta říká, že pokud se u~Poissonova procesu omezíme na pozorovací okno $W$ a~podmíníme celkovým počtem bodů procesu v~tomto okně, dostáváme právě binomický proces.

\begin{figure}[t]
	%\begin{center}
  \centering
		\includegraphics[width = \malwidth]{PoissonBinom.pdf}
		\caption{Ukázka realizace binomického (vlevo) a~Poissonova bodového procesu (vpravo) v~okně $W=[0,1]^2$ s~intenzitou $\lambda=100$.} \label{obrbinxpoisson}
	%\end{center}
\end{figure}

\begin{veta} \label{vetapodminenerozděleni}
Nechť $n \in \mathbb{N}$, $W \subset \mathbb{R}^d$ je omezená množina a~$\mathbb{X}$ je Poissonův bodový proces s~funkcí intenzity $\lambda(u),\ u\in \mathbb{R}^d$. Pak podmíněně při $\mathbb{X}(W) = n$ má restrikce procesu $\mathbb{X}$ na okno $W$ stejné rozdělení jako binomický proces $\mathbb{Y} = \{Y_{1},  \cdots Y_{n}\}$, kde $Y_{i}$ jsou nezávislé náhodné vektory s~hustotou
\begin{align*}
f(u) = \frac{\lambda(u)}{\int_{W} \lambda(v) \, \mathrm{d}{v}} \cdot \mathbbm{1}\{u \in W\},\quad u\in \mathbb{R}^d.
\end{align*}
\end{veta}

Důkaz můžeme nalézt v~práci \cite{Petrakova2020} či jiných zdrojích. Poznamenejme, že v~tomto případě shodou rozdělení bodových procesů $\mathbb{X}$ a~$\mathbb{Y}$ myslíme shodu rozdělení náhodných vektorů $(\mathbb{X}(B_1), \ldots, \mathbb{X}(B_k))$ a~$(\mathbb{Y}(B_1), \ldots, \mathbb{Y}(B_k))$ pro $k \in \mathbb{N}$ a~borelovské množiny $B_1, \ldots, B_k \subset W$.
	

Jak bylo řečeno výše, v~Poissonově procesu nejsou žádné interakce mezi body. Důsledkem je, že realizace procesu nemají žádnou strukturu, jak může\-me vidět na obrázku \ref{obrpoissxthomas}, kde porovnáváme realizaci homogenního Poissonova procesu s~realizací homogenního bodového procesu s~přitažlivými interakcemi mezi body.

%Konkrétně se jedná o \emph{Thomasové proces}. Uvažujme nejprve homogenní Poissonův proces \emph{rodičovských bodů}. Každý rodičovský bod má pak kolem sebe náhodný počet dceřinných bodů -- jejich počet má Poissonovo rozdělení a jejich poloha, relativně vzhledem k poloze daného rodičovského bodu, je popsána náhodným vektorem s $d$-rozměrným normálním rozdělením. Thomasové proces je pak tvořen množinou dceřinných bodů. Nenese tedy informaci o poloze rodičovských bodů.

\begin{figure}
	%\begin{center}
  \centering
		\includegraphics[width=\malwidth]{PoissonThomas.pdf}
		\caption{Ukázka realizace homogenního Poissonova procesu v~okně $W=[0,1]^2$ s~intenzitou $\lambda=100$ (vlevo) a~realizace homogenního bodového procesu ve stejném okně se stejnou intenzitou, ale s~přitažlivými interakcemi mezi body (vpravo).} \label{obrpoissxthomas}
	%\end{center}
\end{figure}



\section{Testy $\chi^2$ pro bodové procesy}

Na následujících stranách představíme několik možností použití $\chi^2$ testu k~testování různých hypotéz o~bodovém procesu. Vždy budeme předpokládat, že pozorujeme jednu realizaci bodového procesu $\mathbb{X}$ v~omezeném pozorovacím okně $W \subset \mathbb{R}^d$. Naším pozorováním tedy není náhodný výběr, ale jedna (lokálně konečná) množina. To pak vyžaduje vhodnou adaptaci standardních postupů.


\subsection{Test Poissonova rozdělení homogenního procesu} \label{sekcepoissonovskost}
Předpokládejme nyní, že bodový proces $\mathbb{X}$ je homogenní s~intenzitou $\lambda > 0$. Zajímá nás, zda je tento proces Poissonův. Testovat tedy budeme hypotézu, že je pozorovaná realizace homogenního procesu v~okně $W$ realizací Poissonova procesu, proti alternativě, že je realizací nějakého jiného homogenního procesu. Jak naznačíme dále, můžeme k~tomu použít test dobré shody s~Poissonovým rozdělením, viz \cite[sekce 10.4.2.]{Spatstat2015}. Obdobný postup v~klasickém kontextu uvádí také \cite[sekce 12.5.]{Andel2007}.

Z~definice Poissonova procesu v~sekci \ref{defpoisson} víme, že počet bodů $\mathbb{X}(A)$ v~nějaké množině $A \subset W$ je náhodná veličina s~Poissonovým rozdělením, a~navíc pro disjunktní oblasti jsou tyto počty nezávislé. Rozdělíme-li tedy okno~$W$ na $m$ disjunktních podmnožin $A_1, \dots, A_m$, se stejnou Lebesgueovou mírou $|A_1| = \dots = |A_m|$, pak počty bodů v~těchto oblastech $\mathbb{X}(A_1), \dots, \mathbb{X}(A_m)$ tvoří náhodný výběr z~Poissonova rozdělení se střední hodnotou $\lambda |A_1|$. 


Označme $n$ pozorovaný počet bodů. Střední hodnoty náhodných veličin $\mathbb{X}(A_1), \dots, \mathbb{X}(A_m)$ (střední počet bodů pozorovaných v~jednotlivých množinách) můžeme odhadnout jako
\begin{align*}
  \frac{\sum_{i=1}^{m}\mathbb{X}(A_i)}{m} = \frac{n}{m}.
\end{align*}
Z~počtů bodů v~jednotlivých množinách sestavíme vektor $(U_0,\dots,U_k)$, kde náhodná veličina $U_j$ udává, v~kolika množinách jsme napozorovali přesně $j$ bodů (pro poslední hodnotu je to $k$ a~nebo více bodů -- hodnota $k$ je volena buď jako největší hodnota z~pozorovaných počtů bodů v~jednotlivých množinách nebo tak, aby byla hodnota $U_k$ dostatečně vysoká pro použití $\chi^2$ testu). Vektor $(U_0,\dots,U_k)$ má poté za hypotézy, že $\mathbb{X}$ je Poissonův proces, multinomické rozdělení s~pravděpodobnostmi 
\begin{align*}
  p_j = \ee^{-{n}/{m}} \frac{({n}/{m})^j}{j!},\quad j \in \{0,\dots, k-1\} \quad \text{a} \quad p_k = 1 - \sum_{j = 0}^{k-1} p_j.
\end{align*}
Testová statistika je pak tvaru 
\begin{align*}
\sum_{j=0}^{k} \frac{(U_j - np_j)^2}{np_j}
\end{align*}
a~má za hypotézy asymptoticky $\chi^2$-rozdělení s~$k$ stupni volnosti. Hypotézu zamítáme pro vysoké hodnoty testové statistiky. Ilustraci použití testu na konkrétních datech zde neuvádíme s~ohledem na nutnost použít vysoký počet množin $m$ a~z~toho plynoucí nepřehlednost vizualizace problému.



\subsection{Homogenita Poissonova procesu} \label{sekcehomogenita}
V~předchozí části jsme předpokládali homogenitu bodového procesu a~hypotézou byla skutečnost, že se jedná o~Poissonův proces. Nyní budeme naopak předpokládat, že pozorovaná realizace pochází z~Poissonova bodového procesu, a~bude nás zajímat, zda je tento proces homogenní. Hypotézou je tedy konstantnost funkce intenzity na pozorovacím okně $W$, alternativou je její nekonstantnost \cite[sekce 6.4.2.]{Spatstat2015}.

Díky větě \ref{vetapodminenerozděleni} víme, že podmíněně při daném počtu pozorovaných bodů v~okně $W$ se na realizaci Poissonova procesu $\mathbb{X}$ můžeme dívat jako na realizaci binomického procesu $\mathbb{Y} = \{Y_1, \dots, Y_n\}$. Pokud je navíc Poissonův proces homogenní s~intenzitou $\lambda > 0$, pak náhodné vektory $Y_i$, udávající polohu jednotlivých bodů, mají rovnoměrné rozdělení v~pozorovacím okně $W$. 
  
Rozdělíme-li toto okno na disjunktní podmnožiny $A_1, \dots, A_m$, jsou očekávané počty bodů v~jednotlivých množinách rovny $\lambda \cdot |A_i|$. Pak podmíněně při~$\mathbb{X}(W) = n$ má náhodný vektor $(\mathbb{X}(A_1),\dots,\mathbb{X}(A_m))$ multinomické rozdělení $\mathrm{M}(n;p_1,\dots,p_m)$, kde $p_j = {|A_j|}/{|W|}$.

Testová statistika je v~tomto případě tvaru
\begin{align*}
  \sum_{j=1}^{m} \frac{(\mathbb{X}(A_j) - np_j)^2}{np_j}.
\end{align*}
Speciálně, pokud volíme množiny $A_1, \dots, A_m$ se stejnou Lebesgueovou mírou, je testová statistika tvaru
\begin{align*}
  \sum_{j=1}^{m} \frac{(\mathbb{X}(A_j) - {n}/{m})^2}{{n}/{m}}.
\end{align*}

Testová statistika má za hypotézy, že funkce intenzity je konstantní, opět asymptoticky $\chi^2$-rozdělení, tentokrát s~$m-1$ stupni volnosti. Hypotézu zamítáme pro vysoké hodnoty testové statistiky.

Pro ukázku použití testu se podíváme na data ze švédského jehličnatého lesa (data jsou volně dostupná v~balíčku \texttt{spatstat} pro \texttt{R}). Jde o~polohu semenáčků borovic (obrázek~\ref{fig:swedishpines}, vlevo). Jejich výskyt v~pozorovacím okně se zdá být homogenní. Pozorovací okno proto rozdělíme na $m=9$ disjunktních částí se stejným tvarem a~velikostí (obrázek \ref{fig:swedishpines}, vpravo). Počty pozorovaných bodů v~jednotlivých množinách také ukazuje obrázek~\ref{fig:swedishpines}. Hodnota testové statistiky je přibližně $4{,}68$, počet stupňů volnosti je 8 a~$p$-hodnota testu je přibližně $0{,}42$. Proto nezamítáme (řekněme na hladině 5\,\%) hypotézu o~homogenitě uvažovaného procesu. Upozorňujeme, že případné interakce mezi body jsme v~tomto ilustračním příkladu zanedbali.

\begin{figure}[t]
	%\begin{center}
  \centering
		\includegraphics[width=\malwidth]{Swedishpines.pdf}
		\caption{Švédský dataset. Levý panel: poloha 71 semenáčků borovic v~obdélníkovém okně o~rozměrech $9{,}6\,\times\,10$ metrů. Pravý panel: rozdělení pozorovacího okna na 9 disjunktních podmnožin se stejným tvarem a~velikostí, čísla udávají pozorovaný počet bodů v~jednotlivých množinách.}\label{fig:swedishpines}
	%\end{center}
\end{figure}


\subsection{Závislost Poissonova procesu na dané kovariátě} \label{sekcekovariata}
Homogenní Poissonův bodový proces není vhodným modelem v~situacích, kdy je intenzita výskytu bodů různá v~různých částech pozorovacího okna. V~takovém případě je funkce intenzity nekonstantní. Analogii předchozího postupu můžeme použít při testování, zda funkce intenzity Poissonova procesu závisí na dané kovariátě $Z$. Kovariátou rozumíme funkci, jejíž hodnoty pozorujeme v~okně $W$ spolu s~body procesu $\mathbb{X}$. % Kovariáta může například udávat nadmořskou výšku v daném místě (pokud nás zajímá, zda ovlivňuje výskyt určitého typu stromů) nebo vzdálenost od vodního zdroje (pokud zkoumáme polohu zvířecích nor) či zdroje znečištění (v případě analýzy výskytu vzácného onemocnění).
Kovariáta může například udávat nadmořskou výšku či pH půdy v~daném místě nebo vzdálenost od~vodních zdrojů či zdroje znečištění.

Test závislosti bodového procesu na kovariátě $Z$ formulujeme následovně. Předpokládáme, že pozorovaný bodový proces je Poissonův, a~testujeme hypotézu, že jeho funkce intenzity není ovlivněna hodnotami $Z$, proti alternativě, že ovlivněna je. Pro přehlednost test popíšeme ve speciálním případě, kdy za nulové hypotézy je funkce intenzity konstantní a~proces je tedy homogenní. Test je možné přímočaře upravit pro situaci, kdy je za nulové hypotézy funkce intenzity (známou) nekonstantní funkcí.

Testová statistika i~její asymptotické rozdělení budou stejné jako v~%předchozí 
čás\-ti~\ref{sekcehomogenita} Využijeme ale toho, že pozorovací okno $W$ můžeme na disjunktní množiny $A_1,\dots,A_m$ rozdělit zcela libovolně. Tyto množiny tentokrát zvolíme tak, aby reflektovaly hodnoty dané kovariáty $Z$. Ve snaze dosáhnout vysoké síly testu proti uvažované alternativě tedy například v~oboru hodnot kovariáty zvolíme hodnoty $a_1, \dots, a_{m-1}$ a~položíme $A_1 = \{ u\in W: Z(u) \leq a_1 \}$, $A_2 = \{ u\in W: a_1 < Z(u) \leq a_2 \},\ \dots,\ A_m = \{ u\in W: Z(u) > a_{m-1} \}$. Testová statistika je pak ve tvaru
\begin{align*}
  \sum_{j=1}^{m} \frac{(\mathbb{X}(A_j) - np_j)^2}{np_j},\quad \text{kde } p_j = \frac{|A_j|}{|W|}.
\end{align*}
Tato testová statistika má za hypotézy opět asymptoticky $\chi^2$-rozdělení s~$m-1$ stupni volnosti, hypotézu zamítáme pro vysoké hodnoty testové statistiky. Zde popsaný test (s~konkrétnější alternativní hypotézou) a~test z~předchozí sekce (s~velmi obecnou alternativou) spolu úzce souvisí, ale nejsou totožné. V~případě s~konkrétnější alternativou (závislost na kovariátě) jsme chytrou volbou dělení okna schopni dosáhnout vyšší síly testu, naopak v~případě velmi obecné alternativy musíme akceptovat nižší sílu testu.

%\enlargethispage{\baselineskip}
%\begingroup
%\abovecaptionskip=0pt
%\aboveskip=0pt
\begin{figure}[!b]
%\begin{center}
%\vskip-\baselineskip
\centering
  \includegraphics[angle=90, width=\textwidth]{Kovariata-crop.pdf}
  \caption{Barro Colorado Island dataset. Levý panel: poloha všech $3\,604$ stromů druhu \emph{Beilschmiedia pendula} v~obdélníkovém okně o~rozměrech $500\,\times\,1000$ metrů. Prostřední panel: nadmořská výška, světlé odstíny značí vysoké hodnoty, tmavé odstíny značí nízké hodnoty. Pravý panel: rozdělení pozorovacího okna na čtyři podmnožiny podle hodnot kovariáty (nadmořské výšky).}\label{fig:kovariata}
%\end{center}
\end{figure}%
%\endgroup

Pro ukázku použití testu se podíváme na data z~tropického deštného pralesa, konkrétně z~panamského ostrova Barro Colorado (data jsou volně dostupná v~balíčku \texttt{spatstat} pro \texttt{R}). Jde o~polohu stromů druhu \emph{Beilschmiedia pendula} (obrázek~\ref{fig:kovariata}, vlevo). Výskyt těchto stromů v~pozorovacím okně je zjevně nehomogenní a~zdá se být ovlivněn nadmořskou výškou (obrázek~\ref{fig:kovariata}, uprostřed). Pozorovací okno proto rozdělíme na $m=4$ disjunktní části podle hodnot této kovariáty tak, aby výsledné množiny měly stejnou plochu (obrázek \ref{fig:kovariata}, vpravo). Z~celkových $n=3\,604$ stromů jich pak 714 leží v~množině s~nejmenší nadmořskou výškou, v~druhé množině jich leží 883, ve~třetí množině $1\,344$, a~v~množině s~nejvyšší nadmořskou výškou jich leží 663. Hodnota testové statistiky je v~tomto případě přibližně 320, počet stupňů volnosti je 3 a~$p$-hodnota testu je tedy velmi blízká 0. Nulovou hypotézu zamítáme (na libovolné rozumně zdůvodnitelné hladině testu) a~činíme závěr, že funkce intenzity uvažovaného procesu závisí na dané kovariátě. 

Stojí za povšimnutí, že jsme tuto závislost odhalili, přestože funkce intenzity není monotónní funkcí kovariáty -- hodnoty nadmořské výšky nejvíce preferované tímto druhem stromů ležely mezi mediánem a~třetím kvartilem hodnot naměřených v~daném pozorovacím okně. Upozorňujeme, že případné interakce mezi body jsme v~tomto ilustračním příkladu zanedbali, vzhledem k~velikosti pozorovacího okna (stovky metrů) a~realistickému odhadu dosahu interakcí mezi dvěma stromy (vyšší jednotky metrů) je však vliv tohoto zanedbání na výsledky testu jen minimální.


\subsection{Test dobré shody Poissonova procesu} \label{sekcetestdobreshody}
V~předchozí části jsme se zabývali závislostí funkce intenzity bodového procesu na kovariátě $Z$, blíže jsme ale nespecifikovali tvar funkce intenzity, tedy jak konkrétně tato kovariáta ovlivňuje rozdělení bodového procesu. V~následující části budeme chtít sestavit formální test hypotézy, že bodový proces je Poissonův bodový proces s~funkcí intenzity pevného tvaru závislého pouze na konečně mnoha neznámých parametrech. Tato část vychází z~bakalářské práce \cite{Petrakova2020}, kde byl tento test podrobně odvozen pro funkci intenzity tvaru $\lambda(x,y) = \exp(\alpha+\beta_1 Z_1(x) + \beta_2 Z_2(y)),\ x,y \in \mathbb{R}$, kde $\alpha, \beta_1, \beta_2$ jsou neznámé reálné parametry a~$Z_1(x), Z_2(y)$ jsou známé spojité kovariáty. 

Chceme tedy testovat hypotézu, že naše pozorování je realizací Poissonova bodového procesu $\mathbb{X}$ v~rovině ($d=2$), jehož funkce intenzity je tvaru  $\lambda(x,y)=\exp(\alpha+\beta_1 Z_1(x) + \beta_2 Z_2(y))$, $x,y \in \mathbb{R}$. Pozorovací okno $W \subset \mathbb{R}^2$ budeme tentokrát předpokládat ve tvaru kartézského součinu $W = W_1 \times W_2$. Opět budeme vycházet z~věty \ref{vetapodminenerozděleni}, která říká, že podmíněně při $n$ pozorovaných bodech v~okně $W$ se na pozorovanou množinu bodů můžeme dívat jako na~realizaci binomického bodového procesu, tedy jako na realizaci náhodného výběru $\{X_1,\dots, X_n\}$ z~rozdělení, jehož hustota je tvaru 
\begin{align*}
  f(x,y) & = \frac{\lambda(x,y)}{\int_{W} \lambda(z,w) \, \mathrm{d}(z,w)} \cdot \mathbbm{1}_{W} (x,y) \\ 
	       & = \frac{\ee^{\beta_1 Z_1(x)} \cdot  \ee^{\beta_2 Z_2(y)}}{\int_{W_{1}} \ee^{\beta_1 Z_{1}(z)} \, \mathrm{d}z \cdot \int_{W_{2}} \ee^{\beta_2 Z_{2}(w)} \, \mathrm{d}w}  \cdot  \mathbbm{1}_{W} (x,y),\quad x,y \in \mathbb{R}.
\end{align*}

Uvažujme nyní rozklad množiny $W_1$ na $k$ disjunktních množin $B_1, \dots, B_k$ a~rozklad množiny $W_2$ na $l$ disjunktních množin $C_1,\dots, C_l$. Pak přirozeně dostáváme rozklad množiny $W$ na disjunktní množiny $A_{1,1}, \dots, A_{k,l}$, kde $A_{i,j}=B_i \times C_j$. Podmíněné rozdělení (při daném celkovém počtu $n$ pozorovaných bodů ve $W$) počtu bodů procesu $\mathbb{X}$ v~množině $A_{i,j}$ je binomické s~parametry $n$ a~$p_{i,j}(\beta_1,\beta_2)$, neboť můžeme psát
\begin{align*}
  \mathbb{X}(A_{i,j}) = \sum_{\alpha = 1}^{n} \mathbbm{1} \{X_{\alpha} \in A_{i,j}\}.
\end{align*}
Pravděpodobnosti $p_{i,j}(\beta_1,\beta_2)$ jsou přitom tvaru
$$
p_{i,j}(\beta_1,\beta_2) = \int_{B_{i} \times C_{j}} \frac{\ee^{\beta_1 Z_1(x)} \cdot  \ee^{\beta_2 Z_2(y)}}{\int_{W_{1}} \ee^{\beta_1 Z_{1}(u)}\, \mathrm{d}u \cdot \int_{W_{2}} \ee^{\beta_2 Z_{2}(v)} \,\mathrm{d}v} \,\mathrm{d}(x,y).
%= \frac{1}{\int_{W_{1}} e^{\beta_1 Z_{1}(u)} \mathrm{d}u} \int_{B_{i}} e^{\beta_1 Z_1(x)} \mathrm{d}x \cdot \frac{1}{\int_{W_{2}} e^{\beta_2 Z_{2}(v)} \mathrm{d}v} \int_{C_{j}} e^{\beta_2 Z_2(y)} \mathrm{d}y. 
$$
Vezmeme-li pak náhodný vektor $(\mathbb{X}(A_{1,1}), \dots, \mathbb{X}(A_{k,l}))$, bude mít za hypotézy, podmíněně při daném $n$, multinomické rozdělení závisející na konečně mnoha parametrech $\mathrm{M}(n;p_{1,1}(\beta_1,\beta_2), \dots, p_{k,l}(\beta_1,\beta_2))$. 

Nyní využijeme věty z~knihy \cite[str. 273]{Andel2007}, která říká, že za určitých předpokladů (pro tento model ověřených v~práci \cite{Petrakova2020}) a~za podmínky, že existuje právě jeden odhad neznámých parametrů $\beta_1$ a~$\beta_2$ modifikovanou metodou minimálního $\chi^2$ \cite[str. 272]{Andel2007}, platí, že testová statistika
\begin{align*}
\sum_{i = 1}^{k} \sum_{j = 1}^{l} \frac{(\mathbb{X}(A_{i,j})-np_{i,j}(\widehat{\beta}_1,\widehat{\beta}_2))^2}{np_{i,j}(\widehat{\beta}_1,\widehat{\beta}_2)}
\end{align*}
má asymptoticky $\chi^2$-rozdělení s~$kl - 3$ stupni volnosti $( \widehat{\beta}_1,\widehat{\beta}_2$ jsou dané odhady neznámých parametrů). Také v~tomto případě zamítáme nulovou hypotézu pro vysoké hodnoty testové statistiky. 

Analogický test lze samozřejmě odvodit i~pro modely Poissonova bodového procesu s~jinou funkcí intenzity závisející na neznámých parametrech, pokud tato splňuje potřebné předpoklady. Příbuzný postup lze nalézt v~literatuře \cite[sekce 10.4.1]{Spatstat2015}.



\subsection {Separabilita funkce intenzity Poissonova procesu} \label{sekceseparabilita}
Nyní se zaměřme na časoprostorové bodové procesy, tedy procesy v~$\mathbb{R}^d \times \mathbb{R}$, kde $d$ složek vyjadřuje polohu bodu v~prostoru a~poslední složka vyjadřuje časovou souřadnici. Časoprostorové procesy se liší od procesů v~$\mathbb{R}^{d+1}$ například v~tom, že pro určování vzdálenosti mezi body (či spíše událostmi) není přirozené používat eukleidovskou vzdálenost. Pro přehlednost se zde opět zaměříme na nejběžnější případ $d=2$. Přirozenou a~důležitou otázkou je, zda funkce intenzity $\lambda(x,y,t),\ x,y,t \in \mathbb{R}$, je \emph{separabilní}, tedy zda existují funkce $\lambda_1(x,y)$ a~$\lambda_2(t)$ tak, že 
\begin{align} \label{fceint}
\lambda(x,y,t) = \lambda_1 (x,y) \cdot \lambda_2(t). 
\end{align}
Na následujících řádcích odvodíme formální test hypotézy, že časoprostorový Poissonův bodový proces má separabilní funkci intenzity. Tento test je založen na vlastnostech Poissonova procesu a~jeho zobecnění na procesy s~interakcemi mezi body není možné provést při současném požadavku na dodržení nominální hladiny testu \cite{Separability2020}.


Předpokládejme tedy, že pozorujeme realizaci Poissonova bodového procesu $\mathbb{X} = \{X_1, \dots, X_n\}$ v~okně $W = W_1 \times W_2$, kde $W_1 \subset \mathbb{R}^2$ je pozorovací okno v~prostoru a~$W_2 \subset \mathbb{R}$ je pozorovací okno, či lépe pozorovací interval, v~čase. Uvažujme nulovou hypotézu, která říká, že funkce intenzity tohoto procesu je tvaru \eqref{fceint} (tedy je separabilní), proti alternativě, že separabilní není. Využijeme opět věty \ref{vetapodminenerozděleni}, která říká, že se na pozorované body můžeme dívat jako na náhodný výběr z~rozdělení s~hustotou
\begin{align*} 
  f(x,y,t) = \frac{\lambda(x,y,t)}{\int_{W} \lambda(x,y,t) \, \mathrm{d}(x,y,t)} \cdot \mathbbm{1}_{W} (x,y,t), \quad x,y,t \in \mathbb{R},
\end{align*} 
což můžeme díky výrazu \eqref{fceint} zapsat v~součinovém tvaru jako
\begin{align} \label{sephustota}
f(x,y,t) = \frac{\lambda_1(x,y) \cdot \mathbbm{1}_{W_1}(x,y)}{\int_{W_{1}}\lambda_1(x,y) \, \mathrm{d}(x,y)}  \cdot \frac{\lambda_2(t) \cdot \mathbbm{1}_{W_2}(t)}{\int_{W_{2}}\lambda_2(t) \, \mathrm{d}t}, \quad x,y,t \in \mathbb{R}.
\end{align}
Speciálně tedy vidíme, že prostorová a~časová složka náhodného vektoru jsou na sobě nezávislé (podmíněně při daném počtu bodů v~pozorovacím okně). Označme dále činitele ve výrazu \eqref{sephustota} postupně jako $f_1(x,y)$ a~$f_2(t)$.

Rozdělme nyní, stejně jako v~části \ref{sekcetestdobreshody}, pozorovací okno $W$ na disjunktní podmnožiny $A_{i,j} = B_i \times C_j$, kde $B_i \subset W_1,\ i\in \{1,\dots,k\},$ a~$C_j \subset W_2$, $j\in\{1,\dots, l\}$. Opět dostáváme, že náhodný vektor $(\mathbb{X}(A_{1,1}),\dots, \mathbb{X}(A_{k,l}))$ má multinomické rozdělení s~parametry $n$ a~$(p_{1,1}, \dots, p_{k,l}) $, kde $p_{i,j}$ je (podmíněná) pravděpodobnost, že bod procesu padne do oblasti $A_{i,j}$, tedy
\begin{align*}
  p_{i,j} = \int_{B_i \times C_j} f(x,y,t) \, \mathrm{d}(x,y,t) = \int_{B_i} f_1(x,y) \, \mathrm{d}(x,y) \cdot \int_{C_j} f_2 (t) \, \mathrm{d}t.
\end{align*}

Tedy pravděpodobnosti $p_{i,j}$ jsou za hypotézy v~součinovém tvaru a~může\-me použít test nezávislosti v~kontingenční tabulce \cite[str. 279]{Andel2007}. Pokud označíme $\mathbb{X}(B_i)$, resp. $\mathbb{X}(C_j)$, počet bodů procesu $\mathbb{X}$ s~prostorovými, resp. časovými, souřadnicemi v~množině $B_i$, resp. $C_j$, pak testová statistika je tvaru
\begin{align*}
  \sum_{i = 1}^{k} \sum_{j = 1}^{l} \frac{(\mathbb{X}(A_{i,j}) - {\mathbb{X}(B_i) \mathbb{X}(C_j)}/{n})^2}{{\mathbb{X}(B_i) \mathbb{X}(C_j)}/{n}}
\end{align*}
a~platí, že má asymptoticky $\chi^2$-rozdělení s~$(k-1)(l-1)$ stupni volnosti, přičemž hypotézu separability funkce intenzity opět zamítáme pro vysoké hodnoty testové statistiky. S~ohledem na obtížnou vizualizaci časoprostorových dat zde neuvádíme konkrétní příklad a~zájemce odkazujeme na článek%do článku
~\cite{Separability2020}.

\section{Závěr}
V~tomto článku jsme ukázali několik možností využití $\chi^2$~testu k~testování různých hypotéz o~bodových procesech. Předpoklad, že data pocházejí z~Poissonova procesu, byl při odvozování těchto testů stěžejní. Díky tvaru podmíněného rozdělení Poissonova procesu při daném počtu pozorovaných bodů bylo možné využít metody testování pro náhodné výběry. Pro jiné druhy bodových procesů by stejný postup využít nešlo.
 

%\bigskip

\Podekovani{Tento příspěvek vznikl s~podporou projektu GAČR 19-04412S.}

%\bigskip

\renewcommand{\refname}{Literatura}
\begin{thebibliography}{9}
\setlength\itemsep{-1pt}

\bibitem{Andel2007}  
Anděl, J. (2007) : {\it Základy matematické statistiky.} Matfyzpress, Praha.
  
\bibitem{Spatstat2015}  
Baddeley, A., Rubak, E., Turner, R. (2015) :  {\it Spatial Point Patterns: Methodology and Applications with R.} CRC Press.

\bibitem{Separability2020}
Ghorbani, M., Vafaei, N., Dvořák, J., Myllymäki, M. (2021+): Testing the first-order separability hypothesis for spatio-temporal point patterns. Podáno. Preprint dostupný online: \url{https://arxiv.org/abs/2009.04747}.

\bibitem{Petrakova2020}
Petráková, M. (2020): {\it Separabilita funkce intenzity Poissonova bodového procesu.} Bakalářská práce, MFF UK, Praha.
 
\end{thebibliography}



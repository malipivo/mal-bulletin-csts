% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode 
% !TEX root = ../../mal-core.tex

\def\zvyrazni#1{\textit{#1}}

\gdef\mujnazevCS{Informační bulletin České
statistické společnosti -- 30. výročí vydávání časopisu}
\gdef\mujnazevEN{Information Bulletin of the Czech Statistical Socienty -- 30th anniversary of journal publishing}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Hana Řezanková}

\bonushyper

\nazev{\mujnazevCS}

\nazev{\mujnazevEN}

\author{\mujauthor}

%\Adresa{}

\Email{rezanka@vse.cz}
\medskip

%\Abstrakt{}
%\smallskip
%\KlicovaSlova{}

%\Abstract{}
%\smallskip
%\KeyWords{}

I~když rok 2020 byl ve znamení
koronaviru, on-line výuky, práce v~některých profesích ve
stylu home office a ve znamení řady jiných omezení, byl spojen
také s~30. výročím založení České statistické
společnosti, stejně jako s~30. výročím vydávání časopisu
\zvyrazni{Informační bulletin
České statistické společnosti}


První číslo Informačního
bulletinu vyšlo 1. května 1990 s~úvodním článkem „Česká
statistická společnost založena!“ Toto číslo bylo věnováno
nově založené společnosti, jejímu ustavujícímu zasedání,
výsledkům voleb předsedy společnosti, hlavního výboru a
revizorů a první schůzce hlavního výboru. Dále byla do časopisu
zařazena řada sdělení, např. oznámení o~přednáškách,
seminářích a konferencích. Uveřejněn byl adresář členů ke
dni 1.~5.~1990 a na poslední straně bylo „trochu
statistiky“, konkrétně počty členů podle měst, organizací a
titulů.

Tím byl odstartován vývoj
časopisu, který se v~průběhu let vylepšoval po různých
stránkách. Časopis vycházel zpočátku pouze v~tištěné
podobě. S~vytvořením webových stránek České statistické
společnosti se naskytla možnost uveřejňovat jednotlivá čísla
též on-line, což bylo postupně realizováno, včetně doplnění
všech starších čísel časopisu. Dnešní doba je uspěchaná,
ale možná si někteří čtenáři najdou chvíli času a kromě
aktuálního čísla si prolistují i čísla z~dnešního
pohledu již historická. Předpokládám, že snad každého zujme
nějaký článek či informace, byť staršího data. Sama jsem
listovala a vzpomínala. 


\section{Listování aneb o~čem se psalo v~Informačním bulletinu}

Ve druhém čísle časopisu
převládaly články zaměřené na~odborný život statistiků,
např. o~konferenci COMPSTAT 1990, o~pracovišti „biometrie
lékařské fakulty Univerzity Palackého v~Olomouci“, o~novém
obsazení řídících funkcí ve Federálním statistickém úřadě.
Byly zavedeny rubriky \zvyrazni{Recenze}, \zvyrazni{Software}, \zvyrazni{Nenechte si ujít}
(oznámení o~konferencích). A~především byl publikován první
odborný článek s~názvem \zvyrazni{Co
je nového ve statistické analýze časových řad}
od pana profesora Jiřího Anděla. Třetí číslo již obsahovalo
dva odborné články a opět různé informace týkající se např.
statistického softwaru. V~roce 1990 vyšlo také číslo
mimořádné, věnované konferenci COMPSTAT 1990 a statistickému
softwaru.

Postupně byly zařazovány
rubriky \zvyrazni{Ze světa}
(např. o~zkušenostech z~Indie či o~statistice v~Nizozemí),
\zvyrazni{Ze společnosti},
\zvyrazni{Jubilea}
a jiné (oznámení o~konferencích lze nalézt také v~rubrice
\zvyrazni{Hurá do Evropy!}).
Zájemci naleznou řadu zajímavých článků, úvah nad některými
problematikami, dozví se mnohé z~historie statistiky a
pravděpodobnosti, také o~ekonomické statistice, či o~statistice
ve výzkumech veřejného mínění. V~brzké době nás čeká
Sčítání lidu, domů a bytů 2021. Kdo by se chtěl poohlédnout
do historie, v~č. 4/1991 vyšel článek \zvyrazni{Sčítání
lidu, domů a bytů 1991 – problémy a jejich řešení}
(autoři L. Balík, P. Čtrnáct, Š. Morávková a H. Vorlová). Kdo
by chtěl porovnat význam vybraných statistických časopisů podle
IF (impact factor) s~rokem 1988, doporučuji článek J. Anděla
\zvyrazni{Publikace a citace}
v~č. 1/1993. Zajímavé jsou samozřejmě všechny články,
některé zaujmou svým zvláštním tématem, jako např. článek
S. Komendy \zvyrazni{Korelace,
která nás šatí a obouvá}
v~č. 2/1993.

Jednotlivé rubriky se postupně
z~časopisu vytratily, i když různé informace byly
publikovány stále. Od roku 2013 byla rozlišena rubrika \zvyrazni{Vědecké
a odborné statě} od
ostatních rubrik, označovaných např. \zvyrazni{Jiné
statě}, \zvyrazni{Zprávy
a informace} nebo
\zvyrazni{Pozvánky na akce}.
V~č. 4/2015 byla základní rubrika přejmenována na \zvyrazni{Vědecké,
odborné a přehledové články},
od~č. 1/2016 je její název \zvyrazni{Vědecké
a odborné články}.
Vše ostatní je obvykle v~rubrice \zvyrazni{Zprávy
a informace}. 

Z~textů publikovaných
v~časopise si lze připomenout některá důležitá fakta,
jako např. dlouholetou spolupráci s~Českým statistickým
úřadem. V~č. 1/2001 se píše: „11. ledna 2001 byli členové
výboru České statistické společnosti doc. RNDr. J. Antoch, CSc.,
ing. Z. Roth, CSc. a doc. RNDr. J. Á. Víšek přijati předsedkyní
Českého statistického úřadu doc. Ing. Marií Bohatou, CSc.,
která se vyslovila pro další pokračování spolupráce ČSÚ
s~naší společností.“ Konkrétní spolupráci lze
zaznamenat s~časopisem Statistika, vydávaným ČSÚ. Ve
stejném čísle se píše: „Dva články z~loňského
bulletinu byly díky péči prof. Ing. J. Jílka, CSc. a doc. RNDr.
G. Dohnala, CSc. publikovány již tradičně v~příloze
časopisu Statistika (č. 2, únor 2001), čímž pokračuje
spolupráce, jíž si Česká statistická společnost velmi váží.“


\section{\TeX nická stránka věci}

Od č. 4/1991 je časopis sázen
v~\TeX u.
Od č. 1/1995 je u~časopisu uváděno ISSN 120-8022. Od č. 1/1999
byl symbol počítače s~histogramem a normální křivkou na
monitoru (původně byl pouze monitor bez počítače) nahrazen
oficiálním logem České statistické společnosti. V~roce
2009 byl „novinový“ formát (s~názvem časopisu, číslem,
ročníkem a datem na první straně nahoře) nahrazen formátem
s~obálkou, obvyklým pro časopisy. V~roce 2011 přibylo
DOI: 10.5300/IB. Protože časopis byl (a je dosud) zveřejňován i
na webové stránce ČStS, přibylo v~č. 2/2011 také ISSN pro
online verzi časopisu (1804-8617). Od č. 1/2013 je uváděno též
evidenční číslo registrace vedené Ministerstvem kultury ČR,
které je E 21214. 

Od roku 1993 do roku 2008 byla
poslední strana vyhrazena pro Obsah a pro detailnější informace
o~časopise, které zahrnují vždy též jméno předsedy České
statistické společnosti. S~přechodem na nový formát
s~obálkou byly tyto informace přesunuty na zadní část
obálky. Od roku 2010 bylo na webových stránkách zveřejňováno
každé číslo tak, jako by ho čtenář četl v~papírové
podobě, to jest v~pořadí přední část obálky, články
časopisu a zadní část obálky s~obsahem a dalšími
informacemi. Od č. 3/2020 bylo změněno pořadí tak, že je
nejprve přední strana obálky, pak zadní strana obálky
následovaná články. Pro čtenáře je tento způsob výhodnější,
protože je obsah k~dispozici před vlastními články a není
třeba se přesouvat na konec dokumentu.


\section{Lidé kolem Informačního bulletinu}

Přípravě časopisu se od
počátku věnuje Gejza Dohnal, který se ujal funkce redaktora.
Jména dalších kolegů, kteří se do přípravy časopisu
zapojovali, zůstávala zpočátku skryta a objevovala se spíše
náhodně. Například č. 1/2001 začíná G. Dohnal větami: „Je
úterý, 10. dubna 2001. Sedíme v~pracovně docenta Antocha a
připravujeme Informační Bulletin.“ V~č. 1/2005 jsou za
redakci už uvedena dvě jména, druhé jméno je Mgr. Pavel Stříž.
V~době, kdy se předsedou ČStS stal G. Dohnal (začátkem roku
2007), převzal funkci redaktora časopisu P. Stříž. V~souvislosti
s~přechodem na nový formát časopisu s~obálkou začíná
být publikována sedmičlenná redakční rada v~čele
s~předsedou redakční rady prof. Ing. Václavem Čermákem,
DrSc. Funkce redaktora byla zpřesněna na „technický redaktor“.
Později se v~redakční radě znovu objevuje jméno G. Dohnala,
který samozřejmě i ve funkci předsedy ČStS o~časopis pečoval,
jen nějakým nedopatřením nebyl uveden (na obálce byl uváděn
pouze jako předseda ČStS). V~č. 1/2011 se začíná také
objevovat poděkování Českému statistickému úřadu ve formulaci
„Toto číslo bylo vytištěno s~laskavou podporou Českého
statistického úřadu“.

V~roce 2013 byla redakční
rada rozšířena o~zahraničního člena doc. Jozefa Chajdiaka a
o~Mgr. Ondřeje Vencálka, který se stal odpovědným
redaktorem časopisu; výsledné technické sestavení jednotlivých
čísel i nadále až do současné doby zajišťuje P. Stříž.
Z~důvodu potřeby reprezentace časopisu při různých
aktivitách (např. žádosti o~zařazení do databáze Scopus), byl
G. Dohnal ustanoven šéfredaktorem časopisu (změněno v~č.
4/2014). V~č. 4/2015 se v~redakční radě poprvé
objevuje jméno ženy. Je to doc. Iveta Stankovičová, předsedkyně
SŠDS – dosud stále jediná žena v~redakční radě. 

Bohužel nejsme na světě věčně
a s~některými statistickými osobnostmi jsme se postupem let
museli rozloučit. Začal se zužovat i seznam jmen v~redakční
radě časopisu. Od č. 2/2018 chybí jméno prof. Čermáka, od č.
3/2019 jméno doc. Chajdiaka. 23. listopadu 2020 nás opustil doc.
Tvrdík. Všichni nám moc chybí.

Zřejmě každý alespoň tuší,
jak náročná je práce všech, kteří se podílejí na přípravě
časopisu. Od přijímání rukopisů, zajištění recenzních
posudků, přes komunikaci s~autory při úpravě článků,
technickou přípravu jednotlivých článků a sestavení celého
čísla, až po zajištění tisku a distribuci. Kromě členů
redakční rady je do celého procesu zapojena řada recenzentů.
Součástí přípravy časopisu je i zveřejnění na webových
stránkách, neboť on-line verze je vlastně samostatný časopis.
První webové stránky ČStS vytvořil doc. Jiří Žváček. Bylo
to v~době, kdy po úraze trávil svůj život ve zdravotnických
zařízeních a počítač ovládal tyčinkou, kterou držel
v~ústech. Na webové stránky umisťoval informace o~časopise,
včetně jednotlivých čísel. Bohužel, na 25. listopadu 2020
připadlo smutné páté výročí, kdy nás Jiří opustil. Život
však jde dál, vyvíjí se i technické možnosti, včetně nástrojů
pro správu webových stránek.

Od roku 2019 existuje již třetí
verze webových stránek České statistické společnosti.
Správcovství (tj. funkci webmastera) od druhé verze zajišťuje
Ing. Martina Litschmannová. Také díky ní můžeme listovat
jednotlivými čísly Informačního bulletinu ČStS on-line. Můžeme
však také listovat tištěnými čísly časopisu. V~poslední
době byla většina čísel vytištěna s~podporou Českého
statistického úřadu, samozřejmě díky jeho předchozím i
současnému vedení. Velmi si toho vážíme.

V~předchozích řádcích
se objevila řada jmen. Zejména prvního redaktora Gejzy Dohnala,
současného šéfredaktora časopisu, a současného odpovědného
redaktora Ondřeje Vencálka, který je rovněž předsedou České
statistické společnosti. S~časopisem jsou však spojena i
další jména. Jsou to jména autorů článků a informací, jména
recenzentů a vývojářů webových stránek. Na kontrole
jednotlivých čísel se podílí též RNDr. Marek Malý a RNDr. Jan
Klaschka, do distribuce tištěných čísel je zapojen Ing. Ondřej
Vozár. Cílem tohoto příspěvku není podat úplný výčet jmen.
Pouze naznačit, že časopis Informační bulletin ČStS již prošel
poměrně dlouhým vývojem díky obětavosti a píli řady členů
České statistické společnosti. Děkuji všem těmto kolegům za
péči o~časopis jménem svým i jménem všech ostatních, kteří
si práce spojené s~přípravou jednotlivých čísel časopisu
nesmírně cení.


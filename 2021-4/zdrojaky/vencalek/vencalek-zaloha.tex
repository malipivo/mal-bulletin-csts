% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode 
% !TEX root = ../../mal-core.tex

\gdef\mujnazevCS{Úloha o rozd\v elení s\' azky -- tradi\v cn\'i a bayesovsk\'y pohled}
\gdef\mujnazevEN{Problem of division of the stakes from traditional and bayesian point of view}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Ondřej Vencálek}

%\bonushyper

\nazev{Úloha o rozd\v elení s\' azky --\\tradi\v cn\'i a bayesovsk\'y pohled}

\nazev{\mujnazevEN}

\author{\mujauthor}


\Adresa{Katedra matematické analýzy a aplikací matematiky, Přírodovědecká fakulta Univerzity Palackého v Olomouci}

\Email{ondrej.vencalek@upol.cz}

\Abstrakt{Úloha o rozdělení sázky má důležité místo v historii rozvoje teorie pravděpodobnosti. Její klasickou podobu a řešení, které v 17. století navrhli Pascal a Fermat, zná současný český čtenář například z knihy J. Anděla Matematika náhody. Náš příspěvek se zabývá jak touto klasickou podobou, tak i situací, kdy je pravděpodobnost vítězství jednotlivých hráčů v dílčích partiích neznámá. V této situaci navrhujeme úlohu řešit bayesovsky, jak to v~18.~století navrhl Laplace.}

\KlicovaSlova{bayesovský, pravděpodobnost, rozdělení sázky}

\Abstract{The problem of division of the stakes has an important place in the history of the development of probability theory. Its classic form and solution, suggested by Pascal and Fermat in the 17th century, are probably known to the contemporary Czech reader from J. Anděl's book Mathematics of Chance. Our contribution deals with both this classic form and the situation when the probability of victory of individual players in sub-games is unknown. In this situation, we propose a Bayesian solution, as Laplace suggested in the 18th century.}

\KeyWords{bayesian, probability, division of the stakes}






\section{Úloha o rozdělení sázky a její role v rozvoji teorie pravděpodobnosti}\label{sec01}

Klasická úloha o rozdělení sázky zní takto: Dva hráči hrají hru, jejímž vítězem se stane ten, který dosáhne určitého předem pevně stanoveného počtu bodů v~sérii partií, kde vítězství v~jedné partii přinese jeden bod. Šance na zisk bodu je přitom pro oba hráče stejná. Výsledky jednotlivých partií považujeme za nezávislé. Hra je přerušena v situaci, kdy jeden z hráčů vede, a už v ní nelze pokračovat. Jak spravedlivě rozdělit bank? 

\subsection{Krátká historie a význam úlohy o rozdělení sázky}
Této celkem nenápadné úloze, známé v anglicky psané literatuře jako \uv{problem of points} či \uv{problem of division of the stakes}, ve skutečnosti náleží důležité místo v historii rozvoje teorie pravděpodobnosti. V polovině 17.~sto\-letí tuto úlohu úspěšně vyřešili Blaise Pascal a Pierre de Fermat. Jejich vzájemná korespondence ohledně úlohy o rozdělení sázky -- jedna ze slavných korespondencí v historii matematiky -- se odehrála v roce 1654. Více o této slavné historii viz např. \cite{Devlin}, \cite{Mlodinow} či \cite{Saxl}. Zájemcům doporučujeme práci Karla Mačáka \textit{Počátky počtu pravděpodobnosti} \cite{Macak}, obsahující mimo jiné komentovaný překlad \textit{Pojednání o aritmetickém trojúhelníku} (Traité du triangle arithmétique), které Pascal sepsal právě v roce 1654 (vydáno bylo v roce 1665). V roce 1994 připomněl slavnou historii na stránkách \textit{Informačního bulletinu ČStS} Jan Coufal \cite{Coufal}.  


Záludnost úlohy spočívá v tom, že není zřejmé, jak správně popsat základní množinu všech možností a specifikovat pravděpodobnosti těchto možností, neboli jak správně zvolit pravděpodobnostní prostor. Úloha se tedy týká samotných základů teorie pravděpodobnosti. Proto není překvapivé, že úlohu o rozdělení sázky zmiňuje např. Anděl ve své knize Matematika náhody~\cite{Andel} hned v první kapitole věnované základním pojmům teorie pravděpodobnosti. 

Méně často se zmiňuje, že přibližně sto let po Fermatovi s Pascalem úlohu úspěšně řešil Laplace, viz~\cite{Laplace}. Jeho přístup byl přitom bayesovský. Náš článek má ambici tento přístup k řešení slavné úlohy představit českému čtenáři. Nevycházíme přitom z Laplacova textu, ale z vlastních úvah a výpočtů, které ostatně nejsou příliš složité.     

\subsection{Klasické řešení úlohy o rozdělení sázky}
Řešení úlohy\footnote{Toto řešení je připisováno Fermatovi. Pascalovo řešení uvedeme ve čtvrté kapitole.} o rozdělení sázky vypadá následovně:
Označme písmenem $h$ počet bodů, který je nutný pro celkové vítězství.
O vítězi rozhodne nejvýše $2h-1$ partií (označme tento počet symbolem $n$).  
Uvažujeme všechny možné výsledky těchto $n$ partií, byť se to může zdát zbytečné, protože možná ani tolik partií k~roz\-hod\-nu\-tí potřeba nebude. 
Pro jednoduchost si představme případ, kdy $h=3$. Bude potřeba odehrát nejvýše $n = 2h-1 = 5$ partií.
Průběh všech pěti partií můžeme popsat pěticí písmen složenou ze znaků $A$ a $B$, kde~$A$ značí výhru prvního hráče a $B$ značí výhru druhého hráče. Např. pětice $AAAAA$ znamená, že všech pět partií vyhrál první hráč, pětice $ABBAB$ odpovídá situaci, kdy první hráč vyhrál v první a čtvrté partii a druhý hráč ve všech ostatních.
Snadno nahlédneme, že všech možných průběhů je $2^n$.   
Tento přístup, kdy uvažujeme výsledky všech $n$ partií, 
má výhodu v tom, že při rovnosti šancí na vítězství v jednotlivých partiích mají všechny možné průběhy stejnou pravděpodobnost. Výpočet pravděpodobnosti vítězství jednotlivých hráčů podmíněné určitým průběžným stavem je pak snadným cvičením. 

Anděl ve své knize Matematika náhody uvádí situaci, kdy k celkové výhře je třeba vyhrát šet partií, tedy $h=6$, a hra byla přerušena za stavu 5:3. Uvádí také seznam řešitelů, kteří si před Pascalem a Fermatem na úloze takříkajíc vylámali zuby -- dospěli k nesprávnému řešení. Uveďme nyní správné řešení.

Z $n = 2h-1 = 11$ partií je již $5+3=8$ odehráno a zbývá dohrát tři partie (pokud budeme ve hře pokračovat, i kdyby již bylo rozhodnuto o celkovém vítězství). Možných (stejně pravděpodobných) scénářů dalšího vývoje je celkem $2^3 = 8$. Jenom jediný z nich odpovídá porážce průběžně vedoucího hráče. Je to scénář, v němž všechny tři zbývající hry vedoucí hráč prohraje. Sázka by měla být rozdělena v poměru odpovídajícím šanci jednotlivých hráčů na výhru, tedy 7:1 (šance na výhru je 7:1 z pohledu vedoucího hráče, a 1:7 z~pohle\-du jeho soupeře). 

Uveďme nyní obecné řešení.
Označme $z_1$ počet partií, které do celkového vítězství chybí prvnímu hráči, a $z_2$ počet partií, které k~cel\-ko\-vé\-mu vítězství chybí jeho soupeři. Označme dále maximální počet partií, které zbývají k~roz\-hod\-nu\-tí o celkovém vítězi, symbolem $n_z$ (index $z$ připomíná, že jde o \textsl{zbývající} partie). Tedy $n_z = z_1+z_2-1$.  

Celkové vítězství prvního, resp. druhého hráče, budeme označovat symboly $W_1$, resp. $W_2$ ($W$ jako \textsl{Win}, tedy výhra).
První hráč celkově zvítězí, když jeho soupeř zaznamená nejvýše $z_2-1$ úspěchů (potřebuje jich totiž $z_2$). 
Pravděpodobnost, že druhý hráč vyhraje právě $k$ z $n_z$ partií je $ {n_z \choose k}/2^{n_z}$.
Pravděpodobnost celkového vítězství prvního hráče je tudíž 
$$ \textrm{P}(W_1) = \sum_{k=0}^{z_2-1} \frac{{n_z \choose k}}{2^{n_z}}. $$
Pravděpodobnost výhry druhého hráče se vypočte analogicky -- ve vzorci stačí nahradit symbol $z_1$ symbolem $z_2$. Tedy
$$ \textrm{P}(W_2) = \sum_{k=0}^{z_1-1} \frac{{n_z \choose k}}{2^{n_z}}. $$

Sázka by měla být rozdělena v poměru pravděpodobností na výhru $\textrm{P}(W_1) : \textrm{P}(W_2)$, který odpovídá šanci na výhru prvního hráče (pro druhého hráče je šance na výhru vyjádřena převrácenou hodnotou tohoto podílu, jde tedy o stejné rozdělení banku).  
	\begin{equation} \label{rov1}
     \sum_{k=0}^{z_2-1} {n_z \choose k}  :  \sum_{k=0}^{z_1-1}  {n_z \choose k} .
	\end{equation}

Doplňme, že v situaci popsané Andělem je $z_1=1$, $z_2=3$, a tedy $n_z=z_1+z_2-1 = 3$ a vztah~(\ref{rov1}) dává výše uvedený poměr 7:1. 



\section{Když šance nejsou vyrovnané a stav je 0:0}  \label{sec02}

Uvažujme nyní zobecnění úlohy o rozdělení sázky spočívající v tom, že šance na výhru v jednotlivých partiích nejsou pro oba hráče stejné. Budeme se na hru dívat optikou prvního hráče. Jeho pravděpodobnost výhry v jednotlivých partiích budeme označovat $p_1 \in (0,1)$.  

Vyšetřeme nyní závislost pravděpodobnosti celkové výhry prvního hráče $\textrm{P}(W_1)$ na pravděpodobnosti jeho výhry v jednotlivých partiích $p_1$.
Očividně $\textrm{P}(W_1)$ je rostoucí funkcí $p_1$ -- čím větší je pravděpodobnost výhry v jedné partii, tím větší je pravděpodobnost celkové výhry. Tuto funkci budeme označovat $w_1$, t.j. 
$$\textrm{P}(W_1) = w_1(p_1).$$
Představme si posloupnost $n$ nezávislých pokusů, v nichž pravděpodobnost výhry v jedné partii je $p_1$ (a je ve všech partiích stejná). Celkový počet vyhraných partií označme $X_1$. Tato náhodná veličina má binomické rozdělení s parametry $n$ a $p_1$. K celkové výhře přitom dojde, pokud počet výher $X_1$ bude alespoň $h$. Proto
$$ w_1(p_1) = \textrm{P}(X_1 \geq h). $$
Tato pravděpodobnost se dá vyjádřit pomocí distribuční funkce binomického rozdělení $F_{\textrm{Bin}(n, p_1)}$ takto:
	\begin{equation} \label{w1}
			w_1(p_1) = 1 - \textrm{P}(X_1 \leq h-1) = 1 - F_{\textrm{Bin}(n, p_1)}(h-1)					
	\end{equation}
Praktický výpočet vychází z tvaru pravděpodobnostní funkce binomického rozdělení
	\begin{equation} \label{w2}
			w_1(p_1) = 1 - \sum_{k=0}^{h-1} {n \choose k} p_1^k (1-p_1)^{n-k}.
	\end{equation}

Ze vztahu (\ref{w2}) je patrné, že $w_1(p_1)$ lze vyjádřit jako polynomickou funkci proměnné $p_1$. Například pro $h=3$ platí\footnote{S úpravami nám může pomoci např. software Mathematica:\\
\texttt{1-CDF[BinomialDistribution[5,p],2]}\\
\texttt{FunctionExpand[Expand[\%]]} }
\begin{eqnarray} \label{p5}
w_1(p_1) &=&  {5 \choose 3}p_1^3(1-p_1)^2 + {5 \choose 4}p_1^4(1-p_1)^1 + {5 \choose 5}p_1^5(1-p_1)^0  \nonumber \\
         &=& 6p_1^5 - 15p_1^4 + 10p_1^3. 
\end{eqnarray} 

\begin{figure}[!ht] 	\label{obr1}
	\centering
		\includegraphics{pw1-crop.pdf}
	\caption{Závislost pravděpodobnosti celkové výhry $w_1(p_1)$ na pravděpodobnosti výhry v jedné partii $p_1$ při hře na tři vítězství.}
\end{figure}


Obrázek \ref{obr1} znázorňuje závislost pravděpodobnosti celkového vítězství $w_1(p_1)$ na pravděpodobnosti výhry v jedné partii $p_1$ v situaci, kdy k celkovému vítězství stačí tři vyhrané partie ($h=3$). V obrázku je pro snazší orientaci dokreslena diagonála $w_1(p_1)=p_1$ odpovídající situaci, kdy se hraje jediná partie ($h=1$).
Je vidět, že (pro $h=3$) je-li např. $p_1=0,25$, je $w_1(p_1) < 0,25$, tedy pokud má hráč 25\% pravděpodobnost výhry v jedné partii, je pravděpodobnost, že celkově zvítězí, menší než 25 \%. Potřeba zvítězit opakovaně v~ně\-ko\-li\-ka partiích tak zvyšuje férovost hry -- pravděpodobnost celkového vítězství \uv{lepšího} hráče (hráče s $p_1>0,5$) je ještě větší než pravděpodobnost jeho vítězství v~jednotlivých partiích. 

Čím větší je počet vítězných partií požadovaný pro celkové vítězství, tím větší je (při libovolné pevně dané hodnotě $p_1$) pravděpodobnost výhry lepšího z hráčů, tedy platí \uv{čím více potřebných vítězství, tím je hra férovější}. Tato zákonitost je ilustrována na obrázku~\ref{obr2}.   

\begin{figure}[!ht] 	\label{obr2}
	\centering
		\includegraphics{pw2-crop.pdf}
	\caption{Srovnání závislosti pravděpodobnosti celkové výhry $w_1(p_1)$ na pravděpodobnosti výhry v jedné partii $p_1$ pro různé počty vítězných partií požadovaných pro celkové vítězství.}
\end{figure}



\section{Když šance nejsou vyrovnané a hra je rozehraná}  \label{sec03}

Úvahy a výpočty z předešlého odstavce lze snadno upravit pro situaci, kdy hra je již rozehrána. 
Stejně jako v kapitole~\ref{sec01} budeme počty zbývajících výher pro jednotlivé hráče označovat $z_1$, $z_2$, a maximální počet partií potřebných k rozhodnutí $n_z$.

První hráč zvítězí, bude-li počet partií, které vyhraje, alespoň $z_1$, tj.
\begin{eqnarray} \label{w1b}
			w_1(p_1; z_1, z_2) &=& 1 - \textrm{P}(X_1 \leq z_1-1) = 1 - F_{\textrm{Bin}(n_z, p_1)}(z_1-1)	\nonumber \\				
			         &=& 1 - \sum_{k=0}^{z_1-1} {n_z \choose k} p_1^k (1-p_1)^{n_z-k}.
	\end{eqnarray}
V tomto vzorci jsme zdůraznili, že pravděpodobnost vítězství prvního hráče není pouze funkcí pravděpodobnosti $p_1$, ale závisí také na průběžném stavu hry, resp. na počtu partií, které k celkovému vítězství potřebují jednotliví hráči, tedy na $z_1$ a $z_2$.
Vzorce (\ref{w1}) a (\ref{w2}) odvozené v kapitole \ref{sec02} jsou vlastně speciálním případem tohoto vzorce, kde $z_1 = z_2 = h$, a tedy $n_z = n$. 

Uvažujme speciální případ, kdy zatím všechny hry vyhrál první hráč a zbývá mu poslední vítězná partie, tedy stav je $(h-1):0$.
V tomto případě $z_1 = 1$, $z_2 = h$, $n_z = z_1 + z_2 - 1 = h$. Proto pravděpodobnost celkového vítězství prvního hráče je dle vzorce (\ref{w1b})
$$ w_1(p_1; 1, h) = 1 - (1-p_1)^h.$$
Pokud $p_1$ je blízké nule, může být tato pravděpodobnost stále dosti malá. Např. při hře na tři vítězství ($h=3$) a průběžném vedení \uv{outsidera}, jehož pravděpodobnost vítězství v jednotlivých partiích je $p_1 = 0,05$, je stále výrazně pravděpodobnější, že vyhraje druhý hráč, neboť pravděpodobnost celkové výhry prvního hráče je $1 - (1-0,05)^3 = 0,14$. Na druhou stranu pokud je počet partií dosti velký a vedoucí outsider není příliš horší než favorit, má již dosti dobrou šanci na vítězství. Např. při $p_1 = 0,2$ a $h=5$, je pravděpodobnost výhry prvního hráče $1 - (1-0,2)^5 = 0,67$.   


\section{Pascalovo řešení úlohy o rozdělení sázky}  \label{sec04a}
Pascalova úvaha vedoucí k řešení úlohy o rozdělení sázky je dnes známá jako \textit{věta o úplné pravděpodobnosti}. 
Pascal si uvědomil, že po situaci, kdy k celkovému vítězství chybí prvnímu z hráčů $z_1$ vítězných partií a druhému hráči $z_2$ partií, mohou následovat pouze dvě (vzájemně se vylučující) situace:
\begin{itemize}
	\item první zvítězí a bude mu zbývat $z_1-1$ vítězných partií, přičemž druhému stále bude chybět $z_2$ partií k vítězství,
	\item druhý hráč zvítězí a bude mu zbývat $z_2-1$ vítězných partií, zatímco prvnímu stále bude chybět $z_1$ partií. 
\end{itemize}
Jelikož první z uvedených možností nastane s pravděpodobností $p_1$ a druhá s pravděpodobností $1-p_1$, musí pro pravděpodobnost celkového vítězství prvního hráče $w_1(p_1; z_1, z_2)$ dle věty o úplné pravděpodobnosti platit:
\begin{equation}
  w_1(p_1; z_1, z_2) = w_1(p_1; z_1-1, z_2)\cdot p_1 + w_1(p_1; z_1, z_2-1)\cdot (1-p_1).
\end{equation}
Přitom zřejmě 
\begin{itemize}
	\item $w_1(p_1; 0, z_2) = 1$ pro libovolné $z_2>0$,
	\item $w_1(p_1; z_1, 0) = 0$ pro libovolné $z_1>0$.
\end{itemize}
Nyní již stačí tuto diferenční soustavu rovnic vyřešit -- vyjádřit $w_1(p_1; z_1, z_2)$.



\section{Když pravděpodobnost výhry neznáme -- Bayesovský přístup}  \label{sec04}

V předchozích částech jsme předpokládali, že známe pravděpodobnost výhry jednotlivých hráčů v jednotlivých partiích, tedy $p_1$ a $p_2=1-p_1$ jsou známé konstanty. V klasické podobě úlohy popsané v kapitole \ref{sec01} jde přitom o \uv{férovou} hru, kdy $p_1=p_2=0,5$. Pokládat hodnoty $p_1$ a $p_2$ za známé je realistické, například když jde o házení (dobře vyváženou) kostkou nebo jinou hazardní hru, kdy $p_1$ umíme vypočítat, jako je třeba ruleta ($p_1$ = 18/37 z pohledu sázejícího na černou/červenou; u americké rulety, která má zdvojenou nulu, je $p_1$ = 18/38). Jak ale rozdělit výhru mezi dva tenisty, když první z hráčů vede 2:0 na sety (při hře na tři vítězné sety), ale jeho dosavadní zápasová bilance je pasivní -- všechny čtyři předešlé vzájemné duely prohrál? Otázka také může znít, jak rozdělit svou sázku na vítězství těchto hráčů? Parametr $p_1$ je v tomto případě neznámý. Považujeme za přirozené o něm uvažovat bayesovsky, tedy jako o náhodné veličině. 

\subsection{Volba apriorního rozdělení hodnoty $p_1$}

Ještě předtím než hráči začnou hrát a než uvidíme, jak se kterému z nich daří, máme nějakou více či méně vágní představu o pravděpodobnosti výhry prvního hráče v jednotlivých partiích $p_1$. Tuto představu můžeme vyjádřit pomocí hustoty $f_{p_1}\colon (0,1) \rightarrow \mathbb R_0^+$.
  
Za apriorní rozdělení hodnot $p_1$ můžeme vzít beta rozdělení. 
Připomeňme, že toto rozdělení je dáno hustotou
	\begin{equation}
	f_{p_1}(p_1) = \frac{1}{\textrm{B}(\alpha, \beta)}\cdot p_1^{\alpha-1}(1-p_1)^{\beta-1}, \ \  p_1 \in (0,1), 
	\end{equation}
kde $\alpha$ a $\beta$ jsou parametry rozdělení (jde o kladná reálná čísla) a $\textrm{B}(\alpha, \beta)$ je hodnota tzv. beta funkce v bodech $\alpha, \beta$ (viz~kapitola 1.3.1 v~\cite{AndelMS}). Hodnota $\textrm{B}(\alpha,\beta) \in (0,\infty)$ je vlastně jen normalizační konstanta -- kladné reálné číslo, jímž násobíme výraz $p_1^{\alpha-1}(1-p_1)^{\beta-1}$, aby šlo o hustotu (integrál hustoty přes celé $\mathbb R$ musí být roven 1). 

Parametry apriorního rozdělení $\alpha$ a $\beta$ volíme podle typu hry, jakou spolu hráči hrají, a také podle toho, co o nich víme. 
Pokud nemáme důvod žádného z hráčů favorizovat, položíme $\alpha = \beta$ (dále budeme mluvit jen o parametru $\alpha$). Čím větší hodnotu $\alpha$ v takovém případě zvolíme, tím menší rozptyl bude rozdělení mít, neboť platí
$$ \textrm{var}(p_1) = \frac{\alpha\beta}{(\alpha+\beta)^2 (\alpha+\beta+1)} = \frac{\alpha^2}{(2\alpha)^2 (2\alpha+1)} = \frac{1}{4(2\alpha+1)}. $$
Rozptyl přitom můžeme chápat jako míru nejistoty -- čím větší $\alpha$, tím menší rozptyl a tím menší nejistota.
%Pokud výsledky jednotlivých partií hodně záleží na schopnostech hráčů a méně na štěstí, jako je tomu například u mnoha sportů, je vhodné volit prior vágní, tedy hodnoty $\alpha$ nízké. Jako intuitivní se jeví volba $\alpha=\beta=1$ odpovídající rovnoměrnému rozdělení na intervalu $(0,1)$. Naopak v situaci, kdy je výsledek určen náhodou (hod mincí), můžeme volit hodnoty $\alpha$ velké, neboť nejistota v parametru $p_1$ je \uv{pouze} nejistota ohledně férovosti mince (či jiného losovacího zařízení). 
Míra apriorní nejistoty ohledně hodnoty $p_1$ (vyjádřená, jak jsme uvedli, pomocí parametrů apriorního rozdělení) má podstatný vliv na odhad celkových šancí na výhru: 
\begin{itemize}
	\item Uvažujeme nejprve hru, kde výsledek téměř nezávisí na schopnostech hráčů, ale závisí hlavně na \uv{štěstí}.
Takovou hrou je například hra, v~níž o výsledku rozhoduje hod mincí. 
Nevíme-li nic o minci, se kterou se hází, bude rozumné uvažovat apriorní rozdělení s hustotou symetrickou kolem hodnoty 0,5 a s malým rozptylem, který odpovídá (malé) nejistotě ohledně \uv{férovosti} mince. 
Dojde-li ve hře např. ke stavu 3:0, naši představu o hodnotě parametru $p_1$ to sice ovlivní (začínáme se lehce klonit k možnosti, že mince je \uv{vychýlená}), ovšem zřejmě nijak výrazně, neboť i při hodnotě $p_1=0,5$ může k danému stavu celkem běžně dojít.
  \item V jiném typu her výsledek hodně závisí na schopnostech hráčů. 
	Jde např. o různá sportovní klání. 
Pokud na začátku o kvalitách (schopnostech) soupeřů nic nevíme, budeme opět volit apriorní rozdělení symetrické kolem hodnoty 0,5, neboť nemáme důvod žádného ze soupeřů favorizovat. Bude však rozumné volit apriorní rozdělení vágní, tj. s velkým rozptylem. 
Dojde-li pak ve hře ke stavu 3:0, bude to celkem jasný signál, že jeden ze soupeřů je lepší a naši představu o hodnotě parametru $p_1$ to značně ovlivní.
\end{itemize}
  
Zatím jsme probrali případy, kdy nemáme důvod dopředu některého z~hrá\-čů favorizovat. 
Někdy však fovorita známe.
Jde o situaci, kdy výsledek hry záleží dosti na schopnostech hráčů, a my známe dosavadní vzájemnou bilanci hráčů. 
První hráč v minulosti vyhrál $v_1$ partií a jen $v_2$ partií prohrál ($v_1 > v_2$). 
Pak rozumnou volbou apriorního rozdělení je beta($1+v_1, 1+v_2$). 
Toto rozdělení je vlastně aposteriorním rozdělením v situaci, kdy apriorní rozdělení je rovnoměrné, tj. beta(1,1), po odehrání $v_1+v_2$ partií s $v_1$ vítězstvími prvního hráče. 
 
\subsection{Rozdělení hodnoty $p_1$ a rozdělení hodnoty $w_1$}

Naše přesvědčení se týká hodnoty $p_1$ vyjadřující pravděpodobnost výhry v~jed\-né partii, nikoliv v celé sérii. 
S touto pravděpodobností se zřejmě intuice vypořádává lépe než s pravděpodobností celkového vítězství, která, jak jsme ukázali, dosti závisí na počtu výher nutných k celkovému vítězství. 
Ze vztahu (\ref{w1}) však plyne, že $w_1$ je rostoucí funkcí parametru $p_1$, a můžeme proto vypočítat hustotu veličiny $w_1$ z hustoty veličiny $p_1$.   
Využijeme přitom následující vztah, viz Věta 3.5 v~\cite{AndelMS}:
\begin{equation}
f_{w_1}(y) = \frac{f_{p_1}(w_1^{-1}(y))}{w_1'(w_1^{-1}(y))}, \ \  y \in (0,1),
\end{equation}
kde $f_{w_1}$ je hustota veličiny $w_1$ (pravděpodobnost celkového vítězství), $f_{p_1}$ je hustota veličiny $p_1$ (pravděpodobnost vítězství v jedné partii) a $w_1^{-1}$ je inverzní zobrazení k zobrazení, které hodnotě $p_1$ přiřazuje hodnotu $w_1$ dle vzorce (\ref{w1}). 

Drobnou vadou na kráse (či právě naopak krásou?) je skutečnost, že inverzní funkci často neumíme explicitně vyjádřit, neboť k jejímu vyjádření by bylo potřeba nalezení kořene příslušného polynomu (vyššího stupně). Například pro výše zmíněnou situaci hry na tři vítězství ($h=3$) je $w_1(p_1)$ polynomem 5. stupně, viz~(\ref{p5}). Derivace této funkce má tvar
$$w_1'(p_1) = 30p_1^2(1-p_1)^2,$$ 
a proto při hustotě proměnné $p_1$ odpovídající beta rozdělení s parametry $\alpha$ a $\beta$ můžeme hustotu veličiny $w_1$ v libovolném bodě $y \in (0,1)$ vyjádřit následovně:
$$ 
f_{w_1}(y) = \frac{(w_1^{-1}(y))^{\alpha-1}(1-w_1^{-1}(y))^{\beta-1}}{\textrm{B}(\alpha,\beta)\cdot 30 (w_1^{-1}(y))^{2}(1-w_1^{-1}(y))^{2}}. 
$$
Pro pevně dané $y\in (0,1)$ vypočteme hodnotu $w_1^{-1}(y)$ jako hodnotu $p_1 \in (0,1)$, pro niž platí $6p_1^5-15p_1^4+10p_1^3 = y$ v intervalu (0,1). 

Souvislost hustoty $f_{p_1}$ a $f_{w_1}$ je ukázána na obrázku \ref{obr-pw}. Ten se opět týká hry na tři vítězství (k výpočtům jsou použity výše uvedené vzorce), přičemž parametr $p_1$ má beta rozdělení. Jsou přitom uvažovány tři různé dvojice parametrů $\alpha$ a $\beta$, přičemž jejich poměr je vždy 3:2 -- jde o dvojice (3, 2), (30, 20) a (150, 100). Uvažujeme tak různou míru nejistoty ohledně rozdělení parametru $p_1$. Pro dvojici (3, 2) je nejistota největší, naopak pro dvojici (150, 100) je nejmenší.   

\begin{figure}[!ht] 	\label{obr-pw}
	\centering
		\includegraphics{example1-crop.pdf}
	\caption{Hustota náhodné veličiny $p_1$ (červeně) a jí odpovídající hustota náhodné veličiny $w_1$ (modře) ve třech různých situacích, kdy rozdělení veličiny $p_1$ je beta rozdělení s parametry $\alpha = 3, \beta=2$ (nahoře), $\alpha = 30, \beta=20$ (uprostřed) a $\alpha = 150, \beta=100$ (dole)}.
\end{figure}

Ve všech případech vidíme, že hustota $f_{w_1}$ je oproti hustotě $f_{p_1}$ více koncentrována u okraje intervalu (0,1). To odpovídá našemu pozorování učiněnému v kapitole~\ref{sec02}, že s rostoucím počtem partií potřebým k celkovému vítězství se hra stává férovější, neboť ten, kdo je mírně favorizován v jedné partii, je (při nutnosti vyhrát více partií k dosažení celkového vítězství) ještě více favorizován na celkové vítězství. 


\subsection{Jak v bayesovském přístupu rozdělit sázku}

V klasickém přístupu bylo celkem zřejmé, jak rozdělit sázku. Když v situaci uvažované v knize~\cite{Andel} může nastat 8 různých stejně pravděpodobných scénářů, z nichž jen jeden vede k vítězství druhého hráče, dělí se sázka v poměru 7:1. Jde tedy o poměr pravděpodobností vítězsví, tedy $\textrm{P}(W_1)$ ku $\textrm{P}(W_2)$. 
Stejný poměr využíváme i v situaci, kdy $p_1$ nemusí být roven 1/2, ale jde o libovolnou hodnotu z intervalu (0,1), viz druhá a třetí kapitola totoho textu. 
Jak ale postupovat v situaci, kdy ohledně hodnoty parametru $p_1$ panuje nejistota? V~úvahách nám pomůže následující jednoduchý příklad:

Představme si, že máme k dispozici tři hrací kostky -- dvě klasické a třetí, která má na třech stěnách vždy jednu tečku a na zbylých třech není nic. Náhodně vybereme jednu kostku a hodíme. Vyhráváme, padne-li \uv{jednička}. Pravděpodobnost výhry je tedy buď 1/6 (pokud jsme vybrali jednu z klasických kostek), nebo 1/2 (pokud jsme vybrali speciální kostku). Pravděpodobnost výhry snadno vyjádříme dle věty o úplné pravděpodobnosti takto:
$$  \textrm{P}(W_1) = \frac{1}{6}\cdot \frac{2}{3} + \frac{1}{2}\cdot \frac{1}{3}.$$
Výraz na pravé straně této rovnosti přitom můžeme také chápat jako střední hodnotu náhodné veličiny $w_1$, která může nabývat hodnoty $1/6$ s pravděpodobností 2/3, a hodnoty 1/2 s pravděpodobností 1/3, neboť pro tuto veličinu platí  
$$ \textrm{E}w_1 = \frac{1}{6}\cdot \frac{2}{3} + \frac{1}{2}\cdot \frac{1}{3}.$$
Proto v bayesovském přístupu k úloze o rozdělení sázky dělíme sázku v poměru 
$\textrm{E}w_1$ ku $(1-\textrm{E}w_1)$, kde
$$ \textrm{E}w_1 = \int_0^1 y f_{w_1}(y) \textrm{d}y = \int_0^1 w_1(p_1) f_{p_1}(p_1) \textrm{d}p_1 = \textrm{E}(w_1(p_1)).$$ 

Podotkněme, že spravedlivé dělení sázky odpovídá nejen poměru \textit{pravděpodobností vítězství} jednotlivých hráčů, ale také poměru \textit{očekávaných výher} jednotlivých hráčů, neboť očekávanou výhru vypočteme (v klasickém případě) jako součin pravděpodobnosti vítězství a částky, o kterou se hraje (v~případě prohry je velikost výhry nulová). V bayesovském případě očekávanou výhru vypočteme jako součin $\textrm{E}w_1$ a částky, o kterou se hraje. V obou případech pak \textit{poměr} očekávaných výher obou hráčů nezávisí na částce, o kterou se hraje.  

Postup si opět ukážeme pro hru na tři vítězství. S využitím vztahu (\ref{p5}) dostáváme
\begin{equation}\label{f1}
 \textrm{E}w_1 = 6\textrm{E}p_1^5 - 15\textrm{E}p_1^4 + 10\textrm{E}p_1^3.
\end{equation}
Nyní stačí využít vztahu pro obecné momenty beta rozdělení (viz \cite{Kotz}):
\begin{equation}\label{f2}
 \textrm{E} X^k = \frac{\alpha+k-1}{\alpha+\beta+k-1} \textrm{E}X^{k-1} = \prod_{i=0}^{k-1} \frac{\alpha+i}{\alpha+\beta+i}.
\end{equation}
Využitím vztahů (\ref{f1}) a (\ref{f2}) pak dostáváme výsledný poměr
$$\frac{\textrm{E}w_1}{1-\textrm{E}w_1} = 
\frac{(\alpha+2)(\alpha+1)\alpha}{(\beta+2)(\beta+1)\beta} \cdot \frac{\alpha^2+5\alpha\beta+7\alpha+10\beta^2+25\beta+12}{10\alpha^2+5\alpha\beta+25\alpha+\beta^2+7\beta+12}.$$

\subsection{Numerická ilustrace}

Výsledky uvedené v této kapitole použijeme k výpočtu poměru, v jakém má být rozdělena sázka, v několika konkrétních situacích. Budeme uvažovat zatím nedohranou hru na tři vítězsví pro všechny možné průběžné stavy od 0:0 až po 2:2 a různá apriorní rozdělení. Budeme přitom uvažovat situace, kdy žádného z hráčů dopředu nefavorizujeme (rozdělení beta(1,1) a beta(4,4)), kdy je jeden z hráčů mírným favoritem (rozdělení beta(3,2) a beta(2,3)), a kdy je jeden hráč favorizován celkem jasně (beta(5,2) a beta(2,5)). 

Je třeba ještě rozmyslet, jakým způsobem budeme nakládat s novou informací získanou v průběhu hry. Ta samozřejmě mění naši představu o rozdělení parametrů beta rozdělení: výhra prvního hráče v první partii posune naši představu o hodnotě $p_1$ směrem k větším hodnotám, zatímco jeho eventuální prohra tuto představu posune směrem k menším hodnotám. Stav 1:0 (resp. 0:1) však zároveň přibližuje (vzdaluje) prvního hráče k (od) celkovému vítězství. I kdybychom zůstali u apriorního rozdělení veličiny $p_1$, bude kaž\-dá partie měnit poměr spravedlivého rozdělení sázky. Pokud bychom např. u tenisových utkání znali dosavadní počet vítězství a proher hráčů ve vzájemných zápasech, nikoliv však detailní informaci o poměru setů v těchto utkáních, je kombinování apriorní informace (na úrovni zápasů) a aktuální informace (na úrovni setů) problematické. V této situaci může být opodstatněné zůstat u apriorního rozdělení a zohledňovat aktuální stav pouze jako rozdílnou \uv{vzdálenost} hráčů od celkového vítězství. 

\afterpage{%
%\rotatebox{90}{%
\noindent
\begin{sidewaystable}
		\begin{tabular}{|c|ccc|cccccc| }
			\hline
			stav & \multicolumn{3}{|c|}{parametry} & \multicolumn{6}{|c|}{rozdělení} \\ 
			     & $n_z$ & $z_1$ & $z_2$ & beta(1,1) & beta (4,4) &      beta(3,2) & beta(2,3)       & beta(5,2) & beta(2,5)\\
					\hline 
			0:0 &	5 & 3 & 3   & 1:1 (1:1) & 1:1 (1:1)  &    	9:5 (9:5) & 5:9 (5:9)     & 53:13 (53:13) & 13:53 (13:53)\\
			    \hline
			0:1 & 4 & 3 & 2   & 1:4 (2:3) & 3:8 (23:43)&     5:9 (1:1)  & 1:5 (17:53)   & 35:31 (2:1)   & 29:301 (5:37) \\
			1:0 & 4 & 2 & 3   & 4:1 (3:2) & 8:3 (3:2)  &     5:1 (53:17)& 9:5 (1:1)     & 301:29 (37:5) & 31:35 (1:2)\\ 
				\hline
			0:2 & 3 & 3 & 1   & 1:19 (1:3) & 1:10 (1:5) &     5:37 (2:5) & 1:20 (4:31)  & 7:26 (5:7) & 4:161 (1:20)\\ 
			1:1 & 3 & 2 & 2   & 1:1 (1:1)  & 1:1 (1:1)  &    25:17 (22:13)& 17:25 (13:22)&119:46 (65:19) & 46:119 (19:65)\\
			2:0 & 3 & 1 & 3   & 19:1 (3:1) & 10:1 (5:1)	&    20:1 (31:4) & 37:5 (5:2)   & 161:4 (20:1) & 26:7 (7:5)\\
			  \hline
			1:2 & 2 & 2 & 1   & 1:4 (1:2) & 5:17 (5:13) &    5:13 (2:3) & 1:5 (1:4)      & 21:34 (15:13) & 6:29 (3:25)\\
			2:1 & 2 & 1 & 2   & 4:1 (2:1) & 17:5 (13:5) &    5:1 (4:1)  & 13:5 (3:2)     & 29:6 (25:3)  &  34:21 (13:15)\\
			  \hline
			2:2	& 1 & 1 & 1   & 1:1 (1:1) & 1:1 (1:1) &      5:4 (3:2) & 4:5 (2:3)     &  7:4 (5:2) & 4:7 (2:5)\\
			\hline	
		\end{tabular}
	\caption{Poměr, v němž bude sázka rozdělena při hře na 3 vítězství, různých průběžných stavech a různých apriorních rozděleních. Poměry v závorce jsou počítány za předpokladu, že apriorní rozdělení průběžně neaktualizujeme.}
	\label{tab1}
\end{sidewaystable}%
%}%rotatebox
}


Výsledky výpočtů jsou uvedeny v tabulce 1. Pro každé apriorní rozdělení a každý průběžný stav je uveden poměr rozdělení sázky, když rozdělení $p_1$ průběžně aktualizujeme. V závorce je pak vždy uvedeno, jaký by tento poměr byl, kdybychom zůstali u apriorního rozdělení.

Několik pozorování z tabulky:
\begin{itemize}
	\item Pro symetrická apriorní rozdělení: Za nerozhodného stavu je rozdělení sázky vždy 1:1. Všimněme si symetrie: za stavu 0:2 rozdělujeme v poměru 1:19, za stavu 2:0 v poměru 19:1. Rozhodně stojí za povšimnutí, že pokud bychom svou velmi vágní představu neaktualizovali, byl by poměr za výše uvedených stavů \uv{jen} 1:3, resp. 3:1. Stojí taky za povšimnutí, že např. vedení 2:0 mění při vágním apriorním rozdělení beta(1,1) poměr rozdělení sázy (19:1) výrazněji než při méně vágním apriorním rozdělení beta(4,4), kdy je poměr 10:1. Pozorované dvě výhry totiž výrazněji mění naši představu o rozdělení parametru $p_1$.  
	\item Pro nesymetrická apriorní rozdělení: Za nerozhodného stavu je sázka rozdělena \uv{nerovně} ve prospěch toho, kdo je favorizován apriorním rozělením. Pokud svou představu neaktualizujeme, nerovnoměrnost poměru rozdělení při nerozhodných stavech 0:0, 1:1 a 2:2 postupně klesá až k poměru určeném parametry apriorního rozdělení, např. pro beta (3,2) jsou tyto poměry postupně 9:5 (tj. cca 1,8 : 1), 22:13 (tj. cca 1,7 : 1) a 3:2 (tj. 1,5 : 1). Pokud svou představu o rozdělení $p_1$ průběžně aktualizujeme, pak nerozhodný stav posouvá naši představu směrem k vyrovnanosti hráčů ($p_1$ posouvá směrem k hodnotě 1/2). Proto rozdělení za stavu 2:2 je blíže k poměru 1:1 naž kdybychom svou představu neaktualizovali -- poměr je 5:4 pro beta(3,2) rozdělení, což je blíže k 1:1 než poměr 3:2.   
\end{itemize}
Zajímavých pozorování je možno z tabulky jistě učinit více. Ponecháváme tato pozorování na čtenářích. 
 
\section{Závěr}
Článek připomíná slavnou úlohu o rozdělení sázky, která se dotýká samotných základů teorie pravděpodobnosti. Připomněli jsme její řešení, zobecnili ho na situaci, kdy pravděpodobnost výhry obou hráčů v jednotlivých partiích není stejná (hra není \uv{férová}). Ukázali jsme, jak rostoucí počet partií potřebných k celkovému vítězství zvyšuje šanci favorita na vítězství. Poté jsme ukázali, jak k úloze přistoupit v situaci, kdy pravděpodobnost vítězství hráčů v jednotlivých partiích je neznámá. Přirozený se nám jeví bayesovský přístup k tomuto problému. Ukázali jsme, jak rozdělení pravděpodobnosti výhry v jedné partii transformovat na rozdělení pravděpodobnosti celkové výhry. Nakonec jsme vypočítali, jak při tomto přístupu rozdělit sázku. 

Věříme, že zvídavý čtenář může mít mnoho dotazů a zlepšujících nápadů. Pokud se takový čtenář najde, může volně na tento text navázat a své nápady zveřejnit. 


%\Podekovani{}

%\bigskip

\renewcommand{\refname}{Literatura}
\begin{thebibliography}{9}
\setlength\itemsep{-1mm}

\bibitem{Andel} ANDĚL, Jiří. {\it Matematika náhody.} 
                Vydání čtvrté. Praha: MatfyzPress, nakladatelství Matematicko-fyzikální fakulty Univerzity Karlovy, 2020. 
								Odborná edice Matfyzpress. ISBN 978-80-7378-420-1.

\bibitem{AndelMS} ANDĚL, Jiří. {\it Základy matematické statistiky.} 
                Vydání třetí. Praha: MatfyzPress, nakladatelství Matematicko-fyzikální fakulty Univerzity Karlovy, 2011. 
								Odborná edice Matfyzpress. ISBN 978-80-7378-162-0.

\bibitem{Coufal} COUFAL, Jan. 
                 Alea iacta est aneb půl tisíciletí od vytištění úlohy rytíře de Méré
								 {\it Informační bulletin ČStS}.
								 5 (1 a 2).


\bibitem{Devlin} DEVLIN, Keith. 
                 {\it The unfinished game: Pascal, Fermat, and the seventeenth-century letter that made the world modern.} 
								 Basic Books, 2010.

\bibitem{Kotz} JOHNSON, Norman Lloyd, KOTZ Samuel, BALAKRISHNAN Narayanaswamy. 
                 {\it Continuous univariate distributions.} 
								2nd ed. New York: Wiley, c1994-c1995. ISBN 978-0-471-58494-0.

\bibitem{Laplace} LAPLACE, Pierre Simon (1774). 
                 {\it Memoire sur la probabilite des causes par les evenemens.}
                  English translation by Stephen M. Stigler, appearing in Statistical Science, {\bf 1}(3), 364-378.

\bibitem{Macak} MAČÁK, Karel. 
                 {\it Počátky počtu pravděpodobnosti.}
								Praha: Prometheus, 1997. Dějiny matematiky. ISBN 80-7196-089-6.

\bibitem{Mlodinow} MLODINOW, Leonard. 
                 {\it Život je jen náhoda: jak náhoda ovlivňuje naše životy.} 
								V Praze: Slovart, 2009. ISBN 9788073912598.

\bibitem{Saxl} SAXL, Ivan. 
                 {\it Vybrané kapitoly z historie teorie pravděpodobnosti a matematické statistiky.} 
								učební text 2003.

\end{thebibliography}



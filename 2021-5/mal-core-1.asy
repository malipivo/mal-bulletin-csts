if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-1";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

import graph;size(0,4.5cm);

xaxis("$x$",above=true,p=gray);yaxis("$y$",above=true,p=gray);

real f(real t) {return 1+cos(t);}
path g=polargraph(f,0,2pi)--cycle;
filldraw(g,pink+opacity(.5),drawpen=red);
label("\large$f$",(1.5,1.25),red);

dot("$0$",(0,0),N+E);dot("$a$",(1,0),N);dot("$2a$",(2,0),N+E);

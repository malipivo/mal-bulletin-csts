if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-10";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

import graph3;
import labelpath3;
texpreamble("\usepackage{amsmath}");
unitsize(.64cm);
currentprojection=orthographic(2.5,2,1);

draw(shift(2.42X)*scale(.05,.05,.05)*unitsphere);label(scale(.5)*"$u_1$",(2.2,0,0),-Z);
draw(shift(.88Y)*scale(.05,.05,.05)*unitsphere);label(scale(.5)*"$u_2$",(0,.8,0),-Z);
draw(shift(2.2Z)*scale(.05,.05,.05)*unitsphere);label(scale(.5)*"$u_3$",(0,0,2),-Y+.5Z);

draw(Label(scale(.65)*"$x_1$",position=EndPoint),(-.25,0,0)--(3,0,0),Arrow3(5));
draw(Label(scale(.65)*"$x_2$",position=EndPoint),(0,-.25,0)--(0,2.5,0),Arrow3(5));
draw(Label(scale(.65)*"$x_3$",position=EndPoint),(0,0,-.25)--(0,0,3.5),Arrow3(5));

draw(scale(2.42,.88,2.2)*unitbox,blackIV+linewidth(.25));
draw((2.42,0,0)--(2.42,.88,2.2) ^^ (0,.88,0)--(2.42,.88,2.2) ^^ (0,0,2.2)--(2.42,.88,2.2),blackIV+linewidth(.25));

draw(shift(1.1X+1.6Y+2.4Z)*scale(.075,.075,.075)*unitsphere,PPblueblack);
label(scale(.65)*"$\boldsymbol{a}$",1.1X+1.6Y+2.4Z,1.1Y-Z,PPblueblack);
draw((1.1,1.6,2.4)--(2.7,1.6,2.4) ^^ (1.1,1.6,2.4)--(1.1,3.2,2.4) ^^ (1.1,1.6,2.4)--(1.1,1.6,4),blueV+linewidth(.25));
draw(shift(1.1X+1.6Y+2.4Z)*rotate(-21.801409,X)*rotate(90,Y)*arc(O,1.5,0,90,-44.395569,0,CCW),blueV,Arrow3(4));
label(scale(.4)*"$\gamma_1$",2.000127X+1.736410Y+2.741024Z,blueV);
draw(shift(1.1X+1.6Y+2.4Z)*rotate(47.726311,Y)*rotate(-90,X)*arc(O,1.5,0,90,-74.940148,90,CCW),blueV,Arrow3(4));
label(scale(.4)*"$\gamma_2$",1.475127X+2.261410Y+2.741024Z,blueV);
draw(shift(1.1X+1.6Y+2.4Z)*rotate(111.801409,Z)*arc(O,1.5,0,-90,-49.490825,90,CCW),blueV,Arrow3(4));
label(scale(.4)*"$\gamma_3$",1.475127X+1.736410Y+3.266024Z,blueV);

draw(shift(2.42X)*rotate(-21.801409,X)*arc(O,.4,0,90,-90,0,CCW),blackIV+linewidth(.25));
draw(shift(2.2506X+.0616Y+.154Z)*scale(.03,.03,.03)*unitsphere,blackIV);
draw(shift(.88Y)*rotate(47.726311,Y)*arc(O,.4,0,90,-90,90,CCW),blackIV+linewidth(.25));
draw(shift(.15125X+.715Y+.1375Z)*scale(.03,.03,.03)*unitsphere,blackIV);
draw(shift(2.2Z)*rotate(111.801409,Z)*rotate(90,X)*arc(O,.4,0,-90,-90,90,CCW),blackIV+linewidth(.25));
draw(shift(.1694X+.0616Y+2.046Z)*scale(.03,.03,.03)*unitsphere,blackIV);

draw(Label(scale(.65)*"$\boldsymbol{u}$",position=EndPoint),(1.1,1.6,2.4)--(1.1+2.42,1.6+.88,2.4+2.2),PPblueblack+opacity(.7),Arrow3(5));
draw(Label(scale(.65)*"$\boldsymbol{u}$",position=EndPoint),(0,0,0)--(2.42,.88,2.2),PPblueblack+opacity(.7),Arrow3(5));
draw(rotate(-21.801409,X)*rotate(90,Y)*arc(O,1.5,0,90,-44.395569,0,CCW),PPblueblack+opacity(.7),Arrow3(4));
label(scale(.4)*"$\gamma_1$",0.900127X+0.136410Y+0.341024Z,PPblueblack);
draw(rotate(47.726311,Y)*rotate(-90,X)*arc(O,1.5,0,90,-74.940148,90,CCW),PPblueblack+opacity(.7),Arrow3(4));
label(scale(.4)*"$\gamma_2$",0.375127X+0.661410Y+0.341024Z,PPblueblack);
draw(rotate(111.801409,Z)*arc(O,1.5,0,-90,-49.490825,90,CCW),PPblueblack+opacity(.7),Arrow3(4));
label(scale(.4)*"$\gamma_3$",0.375127X+0.136410Y+0.866024Z,PPblueblack);

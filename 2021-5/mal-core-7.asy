if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-7";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

import graph3;
size(3.5cm,0);
currentprojection=orthographic(1.25,1,2.5);

real f(pair z) {real r=z.x^2+z.y^2;
return r>=0.02 ? min(8,sqrt(z.x^2+z.y^2)/(sqrt(z.x^2+z.y^2+1)-1)) : 0 ;}

bool allow(pair z) {return f(z)>0;}
surface s=surface(f,(-1.5,-1.5),(1.5,1.5),nx=16,allow);
draw(s,blueII+opacity(.75),meshpen=PPblueblackI+thick(),nolight,render(merge=true));

draw(Label(scale(.65)*"$1$",position=EndPoint),(.05,0,1)--(-.05,0,1));
draw(Label(scale(.65)*"$2$",position=EndPoint),(.05,0,2)--(-.05,0,2));
draw(Label(scale(.65)*"$3$",position=EndPoint),(.05,0,3)--(-.05,0,3));
draw(Label(scale(.65)*"$4$",position=EndPoint),(.05,0,4)--(-.05,0,4));
draw(Label(scale(.65)*"$5$",position=EndPoint),(.05,0,5)--(-.05,0,5));
draw(Label(scale(.65)*"$6$",position=EndPoint),(.05,0,6)--(-.05,0,6));
draw(Label(scale(.65)*"$7$",position=EndPoint),(.05,0,7)--(-.05,0,7));
draw(Label(scale(.65)*"$-1$",position=EndPoint),(-1,0,.05)--(-1,0,-.05));
draw(Label(scale(.65)*"$-1$",position=EndPoint),(0,-1,.05)--(0,-1,-.05));

draw(Label(scale(.75)*"$x$",position=EndPoint),(-2,0,0)--(1,0,0),Arrow3);
draw(Label(scale(.75)*"$y$",position=EndPoint),(0,-2,0)--(0,1,0),Arrow3);
draw(Label(scale(.75)*"$z$",position=EndPoint),(0,0,-1)--(0,0,9),Arrow3);

draw(scale(.05,.05,.05)*unitsphere,PPblueblack+1);
label(scale(.65)*"$0$",.25Z+.25Z,Y,PPblueblack);

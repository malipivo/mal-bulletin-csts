if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
settings.inlinetex=true;
deletepreamble();
defaultfilename="mal-core-9";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

pen blackIV=black*0.40+white*0.60;
pen blackII=black*0.20+white*0.80;
pen PPblueblack=blue*0.50+black*0.50;
pen PPoliveblack=heavygreen*0.50+black*0.50;
pen PPblueblackI =PPblueblack*0.60+white*0.40;
pen blueII=blue*0.20+white*0.80;
pen blueV=blue*0.50+white*0.50;

import graph3;
size(3cm,0);
//currentprojection=orthographic(2,2.5,1.5);
currentprojection=orthographic(2,2.5,2);

draw(box(O,(-.383616,1.180650,1.95)),blackII+linewidth(.25)+opacity(.5));
draw(shift(-.383616X)*scale(.05,.05,.05)*unitsphere,black);
draw(shift(1.180650Y)*scale(.05,.05,.05)*unitsphere,black);
draw(shift(1.95Z)*scale(.05,.05,.05)*unitsphere,black);
label(scale(.5)*"$x(t_0)$",(-.383616,0,0),Y,black);
label(scale(.5)*"$y(t_0)$",(0,1.180650,0),X,black);
label(scale(.5)*"$z(t_0)$",(0,0,1.95),X,black);

draw(Label(scale(.65)*"$x$",position=EndPoint),(-2,0,0)--(2,0,0),Arrow3);
draw(Label(scale(.65)*"$y$",position=EndPoint),(0,-2,0)--(0,2,0),Arrow3);
draw(Label(scale(.65)*"$z$",position=EndPoint),(0,0,-.25)--(0,0,4.2),Arrow3);

triple p(real t) { return (2*t*sin(2pi*t)/pi,2*t*cos(2pi*t)/pi,t); }
path3 pp=graph(p,0,2.95,operator ..);
draw(Label(scale(.65)*"$f$",position=EndPoint),pp,PPblueblack+1);
draw(scale(.05,.05,.05)*unitsphere,PPblueblack+1);
draw(shift(-.383616X+1.180650Y+1.95Z)*scale(.075,.075,.075)*unitsphere,PPblueblack+1);
label(scale(.6)*"$f(t_0)$",(-.383616,1.180650,1.95),Y,PPblueblack+1);

triple d(real t) { return (-.383616+7.221514*t,1.180650+3.015794*t,1.95+t); }
path3 dd=graph(d,-.24,.24,operator ..);
draw(Label(scale(.65)*"$d$",position=BeginPoint),dd,PPoliveblack+1);

triple d0(real t) { return (-.383616+7.221514*t,1.180650+3.015794*t,0); }
path3 dd0=graph(d0,.24,-.24,operator ..);draw(dd0--dd--cycle,blackII+linewidth(.25));draw(dd0,blackII+.5);

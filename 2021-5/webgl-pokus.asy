import graph3;
import palette;
size(175,175,IgnoreAspect);
currentprojection=perspective(2,.55,0.25);
real f(pair z) {return sin(z.x)*sin(z.y);}
draw(surface(f,(-pi,-pi),(pi,pi),nx=24,Spline),lightblue,render(merge=true));
surface f=surface(f,(-pi,-pi),(pi,pi),24,Spline);
draw(f,mean(palette(f.map(zpart),Rainbow(24))),brown);
draw(Label("$x$",1),(-1.2pi,0,0)--(1.2pi,0,0),brown,Arrow3);
draw(Label("$y$",1),(0,-1.2pi,0)--(0,1.2pi,0),brown,Arrow3);
draw(Label("$z$",1),(0,0,-1.2)--(0,0,1.2),brown,Arrow3);
label("$z=\sin{x}\cdot\sin{y}$",(.5,.5,-1.35),SE,deepblue);

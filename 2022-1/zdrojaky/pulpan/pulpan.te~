% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode 
% !TEX root = ../../mal-core.tex

\gdef\mujnazevCS{Informace a neurčitosti v~expertních odhadech}
\gdef\mujnazevEN{Information and uncertainty in expert estimations}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Zdeněk Půlpán}

\bonushyper
%\def\textgreek#1{{\color{red}oprav #1}}

\nazev{\mujnazevCS}

\nazev{\mujnazevEN}

\author{\mujauthor}

\Adresa{Prof. RNDr. Zdeněk Půlpán, CSc., Na Brně 1952, 500 09 Hradec Králové 9}

\Email{zdenek.pulpan@post.cz; zdenek.pulpan@upce.cz}

\Abstrakt{Každé rozhodování je možné s~hlediska psychologie redukovat na rozhodování mezi variantou hlavní, uvažovanou, a variantami alternativními. Sémantizovaná informace pro fuzzy množiny nebo intuicionistické fuzzy množiny nám umožňuje přejít od kvalitativní i ke kvantitativní stránce rozhodování, zaměřené na volbu pouze jediné ze dvou možných variant. Je popsána metoda, jak na základě odhadu elementárních nejistot kvantifikovat neurčitost a potřebnou informaci pro rozhodování v~tomto jednoduchém modelu. Z~popsaného je také možné odvodit zobecnění pro komplikovanější rozhodovací situaci.}
\smallskip
\KlicovaSlova{rozhodování, neurčitosti, odhad informace, fuzzy množiny, intuicionistické fuzzy množiny}

\Abstract{Every decision is possible with the terms of psychology reducible to deciding between alternative main consideration and alternative options. The semantic information of intuitionistic fuzzy sets allows us to focus on the quantitative aspect of this type of decision-making that is based on only one main alternative. The article describes the method of estimation based on the fundamental uncertainties and makes quantify uncertainty and information they is needed for decision-making in such a simple model. From the described methodis als opossible to derive a generalization of komplex decision-making situation.}
\smallskip
\KeyWords{decision making, uncertainties, estimation of information, fuzzy sets, intuicionistics fuzzy sets}


\section{Úvod}

Žijeme v~době intenzívního používání dotazníků (testů). Dotazníkům se přisuzuje dost vysoká vypovídací hodnota, na jejich výsledku závisí úspěšnost přijetí i absolvování střední školy a pak i vstup na školu vysokou. Kvalifikační zkoušky v~různých oborech se realizují pomocí dotazníků,probíhají různá marketingová šetření, výzkumy veřejného mínění, atd. Kdo je podroben dotazování, musí se \textit{rozhodnout} pro některou z~variant odpovědi (dokonce i tak, že nebude odpovídat).

Každé rozhodování je volbou mezi několika alternativami a předpokládá existenci určitého (velmi často jen konečného) souboru dobře identifikovatelných a od sebe dostatečně odlišitelných alternativ. Psychologie nás poučuje, že rozhodování pro právě jednu z~více uvažovaných alternativ si respondent redukuje na posloupnost rozhodování vždy jen mezi dvěma alternativami, z~nichž obyčejně jedna je z~určitých subjektivních důvodů respondentem preferována a považována za hlavní, nejdůležitější, správnou, atd. Rozhodovací proces lze pak modelovat orientovaným stromovým grafem, kde z~každého uzlu vycházejí jen dvě hrany, jejichž koncový uzel je obrazem určité alternativy. Každá z~alternativ má pak u~respondenta jistou míru volby, postup v~grafu je možný na základě rozhodnutí, které je u~respondenta podloženo jistými důvody. Rozhodování je poznamenáno nejistotou a důvody pro určitá rozhodnutí závisejí na mnoha i nepředvídatelných faktorech. Tušíme, že zde hraje roli náhoda, kterou je možné odhadnout pravděpodobností. Ta může mít svůj původ subjektivní (například z~osobní zkušenosti) nebo může být objektivizována odhadem z~relativních četností mimo nás realizovaných určitých rozhodnutí za přibližně stejných podmínek.

Nabízí se rozhodování modelovat konečným souborem alternativ  $\Omega =\{\alpha _1,\alpha _2,\dots,\alpha _n\}$ a pravděpodobnostmi jejich realizace  $p_i=p (\alpha _i ),\ i=1,2,\dots,n$,  $0\leq p_i \leq 1,$ kde  $\sum _1^np_i=1.$ Množina alternativ  $\Omega $ spolu s~pravděpodobností  $P (A)=\sum _{\alpha _i \in %\epsilon 
A}p_i$ pro  $A\subset\Omega $ může být základem pro formalizaci rozhodování v~prostoru alternativ  $\Omega .$ Dostáváme tak pravděpodobnostní prostor rozhodování  $ [\Omega ,2^{\Omega },P ]$.



\section{Různé míry neurčitosti a odpovídající informace}

Neurčitost  $H$ (nazýváme ji také entropie) rozhodnutí pro určitou alternativu v~ pravděpodobnostním prostoru  $[\Omega ,2^{\Omega },P ]$ může být určena ze známého Shannonova vztahu 

%$$
\begin{equation}
H=-\sum _1^n p_i\log _z p_i,\quad  0\leq H \leq\log _zn,
\tag{1}
\end{equation}
%$$
%, (1)
výraz  $0\cdot\log _z0$ definujeme limitou  $\lim _{x\leftarrow 0^{+}}x\log _zx=0$, kde  $z>1$ určuje jednotku neurčitosti. Je-li  $z=2,$ pak jednotka neurčitosti je 1 bit, je-li  $z=e$, základ přirozeného logaritmu, je jednotkou 1 nit, v~ostatních případech nazveme jednotku 1 lit. (Převod nitů na bity lze realizovat, obsahuje-li stejný výraz logaritmy, platí totiž  $\log _2x=\log _ex/\log _e2$;  $\log _ex$ označujeme také jako  $\ln x.$)

Představa neurčitosti rozhodování o~velikosti 1 bit je velice jednoduchá. Je to neurčitost rozhodování mezi dvěma stejně pravděpodobnými alternativami:
$$
\Omega =\{\alpha ,\beta \},\  p(\alpha )=p(\beta)=0{,}5;\ 
H=-0{,}5\log _20{,}5-0{,}5\log _20{,}5=1\ \mathit{bit}.
$$

Nejjednodušší model rozhodování předpokládá volbu právě jedné ze dvou relevantních (možných, uvažovaných) variant  $\alpha ,\beta $. Neurčitost je pak určitou mírou „síly volby“ některé z~variant. Neurčitost jako jedna z~charakteristik rozhodovacího procesu může být kvantifikována například na základě subjektivně nebo objektivně odhadnuté pravděpodobnosti  $p\epsilon <0,1>$ volby jedné ze dvou~variant, např. volbou $\alpha $ (když druhá varianta je  $\lnot \alpha =\beta $, Shannonovým vztahem (a viz Obr.1)

\begin{equation}
H (p)=-p\cdot \log _2p- (1-p)\log _2 (1-p)\ [bit] 
\tag{1b}
\end{equation}
kde je  $H(p)\in <0,1>$ (při tom klademe $0\cdot \log _2 0=0$).

Jedna ze základních vlastností neurčitosti (1b) je její symetrie vůči variantám $\alpha, \beta$:

\begin{equation}
H(p)=H(1-p);\quad 
H(0{,}5)=1=\max_p\{H(p)\};\quad
H(0)=H(1)=0.
%$(2)
\tag{2}
\end{equation}

Veličinu  $I(p)=1-H(p)$ je možné interpretovat jako míru informace, “spotřebované” pro volbu jedné z~variant. Chceme-li informaci sémantizovat, tj. polarizovat ji vzhledem k~určité variantě, např. $\alpha ,$ která například odpovídá správné variantě, volíme sémantizovanou informaci $I^s (p$) ve tvaru (\cite{2})

\begin{equation}
I^s (p) =\left\{
\begin{matrix}-I(p);\quad 0\leq p\leq 0{,}5\\
I(p);\quad 0{,}5<p\leq1
\end{matrix}
\right. \text{a platí }
\begin{matrix} 
I^s (0) = -1\leq I^s (p) \leq1=I^s(1);\\
I^s(0{,}5)=0.
\end{matrix}
\tag{3}
\end{equation}%(3)

%Obr. 1
\begin{figure}[!hbt]
\centering
%\includegraphics[width=3.2713in,height=4.1776in]{pulpanverze2-img/pulpanverze2-img001.png}
\begin{tikzpicture}%[scale=1.0]
\begin{axis}
    [
    /pgf/number format/.cd, use comma,
    xtick distance=0.2,
    minor tick num=3,
    tickwidth=8pt,
    %xticklabels={0,0.2,...,1}
    xlabel={$p$},
    ylabel={$H(p)$},
    ]
    \addplot[domain=0:1,thick] {-x*ln(x)/ln(2)-(1-x)*ln(1-x)/ln(2)};
    \addplot[domain=0:1,thick,dashed] {2*(1-max(x,1-x))};
\end{axis}
\end{tikzpicture}
\caption{Závislost neurčitosti $H(p)$ podle (1b) (graf má nelineární průběh, plná linka) a $H(p)$ podle (4) (graf neurčitosti je po částech lineární v~závislosti na pravděpodobnosti $p$, přerušovaná linka)}
\end{figure}

Neurčitost  $H(p)$ je také možné definovat vztahem (4), který je vlastně hrubou aproximací (1b)

\begin{equation}
H(p)=2\cdot(1-\max(p;1-p))\ [\text{lit}].
\tag{4}
\end{equation}

Sémantizovanou informaci je pak možné z~předchozího vztahu určit také jak je uvedeno v~(3). Všechny hodnoty veličin, které byly získané pomocí (4) jsou v~jednotkách lit.


\section{Příklad}

Mějme dotazník, skládající se z~$k$ položek (otázek). Ke každé položce jsou nabídnuty varianty odpovědí; za varianty odpovědí nemusíme jen uvažovat jejich reálné konstrukce, ale například varianty “správně - špatně”, z~nichž právě jedna je právná. Z~odpovědí respondentů na i-tou položku stanovíme pomocí relativních četností odhady pravděpodobností (například  $\widehat {p_i},1-\widehat {p_i}$ volby každé z~variant. Podle (3) pak získáme odhad  $I_i^s(\widehat {p_i})$. Součet  $\sum _1^kI_i^s(\widehat {p_i})$ je horním odhadem informace, kterou využil “průměrný” respondent ke správným odpovědím. Pro lepší interpretaci je třeba tuto hodnotu normovat vydělením maximální možnou hodnotou  $k$. Čím je hodnota  $\frac 1 k\sum _1^kI_i^s(\widehat {p_i})$ bližší 1, tím je rozhodování jedince ve prospěch správné odpovědi jednoznačnější, záporná hodnota by znamenala velkou “neznalost”, \ldots

Neurčitost rozdělení  $\overrightarrow p= \left(\begin{matrix}\alpha _1&\dots &\alpha _n\\p_1&\dots &p_n\end{matrix}\right)$ na prostoru  $\Omega n$ variant můžeme definovat vztahem

\begin{equation}
H(\overrightarrow p)=1-\frac n{n-1}\sum _1^n\left(p_i-\tfrac 1 n\right)^2 [\text{lit}],\quad \sum _1^np_i=1,\quad 0\leq p_i\leq1, %(5)
\tag{5}
\end{equation}
kde  $H(1;0;0;\dots;0)=0\leq H(\overrightarrow p)\leq 1=H(\tfrac 1 n,\tfrac 1 n,\dots,\tfrac 1 n).$

Pro  $n=2$ je  

\begin{equation}
H(p,1-p)=1-2\cdot\left[
\left(p-\tfrac12\right)^2+
\left(1-p-\tfrac12\right)^2
\right]=4\cdot p(1-p)\
[\text{lit}] %(5a)
\tag{5a}
\end{equation}

Jednotkou této neurčitosti je opět neurčitost volby mezi dvěma stejně pravděpodobnými variantami, tedy 1~bit. Stupnice pro (5a) vzhledem k~pravděpodobnosti  $p$ se však od Shannonovy entropie (1b) liší. (Nelze tedy jednotky z~(4), resp. z~(5a) převést na bity z~(1), resp. (1a).) Neurčitost  $H(p,1-p)$ je pro  $p\in \left<0;1\right>$ konkávní funkcí, pro  $p\in (0;0{,}5)$ je rostoucí, pro  $p\in (0{,}5;1)$ klesající, odpovídá aproximaci (1b).

Chceme-li posuzovat neurčitost s~hlediska rozhodování mezi variantou (jevem)  $\alpha _i$ a některou z~variant zbývajících (jevy  $\alpha _j\neq\alpha _i$), známe-li pravděpodobnost  $p_i$, volíme pravděpodobnostní prostor pro  $n=2$ s~elementárními jevy  $\alpha _i$,  $\lnot \alpha _i$ a s~pravděpodobnostmi  $p_i$,  $1-p_i$. Odpovídající neurčitost  $h(p_i)$ pak můžeme definovat i ve tvaru poměru (viz také Obr. 2)

\begin{equation}
h(p_i)=
\left\{
\begin{matrix}
\frac{p_i}{1-p_i},& \text{ když } 0\leq p_i<0{,}5\\
\frac{1-p_i}{p_i},& \text{ když } 0{,}5\leq p_i\leq1
\end{matrix}
\right.
\tag{6a}
\end{equation}

Pak je  $0\leq h(p_i)\leq 1,$ pro  $p_i\epsilon (0;0{,}5)$ rostoucí konvexní funkcí, pro  $p_i\in (0{,}5;1)$ klesající konvexní funkcí. Informaci, kterou přináší zpráva o~tom, že jev  $\alpha _i$ nastal, můžeme kvantifikovat hodnotou 

\begin{equation}
I(\alpha _i)= \left\{
\begin{matrix}
1-\frac{h(p_i)} 2,& \text{ pro } p_i\in \left<0;0{,}5\right) \\
 \frac{h(p_i)} 2, & \text{ pro } p_i\in \left<0{,}5;1\right>.
\end{matrix} 
\right.
\tag{7a} %(7a)
\end{equation}

Pak je  $0\leq I(\alpha _i)\leq1$. Při tom  $I(\alpha _i)=0$, když  $p_i=1$ a  $I(\alpha _i)=1,$ když  $p_i=0$. Menším hodnotám pravděpodobnosti  $p_i$ odpovídá větší hodnota informace. (Porovnejte vztah (7a) a (3).)

Neurčitost pravděpodobnostního rozdělení  $\overrightarrow p=  \left(
\begin{matrix}\alpha _1&\dots&\alpha _n\\p_1&\dots&p_n
\end{matrix}\right)$ 
můžeme pak definovat vztahem

\begin{equation}
h(\overrightarrow p)=\frac 1 2\sum _1^nh(p_i)\ [lit] 
%(6b)
\tag{6b}
\end{equation}
\noindent
kde $h(\overrightarrow p)$ je symetrickou funkcí pravděpodobností  $p_i$ a platí  $0=h(1;0;0;\dots;0)\leq h(\overrightarrow p)\leq1=h(0,5;0,5;0;\dots;0),$ ale  $h(\frac 1 n;\frac 1 n;\dots;\frac 1 n)=\frac n{n-1}$ pro všechna  $n\geq2$. Tato poslední vlastnost ukazuje na nutnost jiné interpretace této neurčitosti než předpokládají předchozí vztahy pro neurčitost (1), (4), (5). Také normování výrazu (6b) jednou polovinou má vztah k~základní situaci jen pro  $n=2.$ Vztah (6b) pro rozdělení  $\overrightarrow p$ se může proto doporučit pro odhad neurčitosti jen pro  $n=2.$

\begin{figure}[!hbt]
\centering
\begin{tikzpicture}%[scale=1.0]
\begin{axis}
    [
    /pgf/number format/.cd, use comma,
    xtick distance=0.5,
    %minor tick num=3,
    %tickwidth=8pt,
    %xticklabels={0,0.2,...,1}
    xlabel={$p_i$},
    ylabel={$h(p_i)$},
    %xmajorgrids,
    xminorgrids,
    ]
    %\addplot[domain=0:1,thick] {-x*ln(x)/ln(2)-(1-x)*ln(1-x)/ln(2)};
    %\addplot[domain=0:1,thick,dashed] {2*(1-max(x,1-x))};
    %\addplot[domain=0:1,thick] {4*x*(1-x)};
    \addplot[domain=0:0.5,thick] {x/(1-x)};
    \addplot[domain=0.5:1,thick] {(1-x)/x};
    \draw [dashed] (axis cs:0.5,0) --(axis cs:0.5,1);
    \draw [dashed] (axis cs:0,1) --(axis cs:1,1);
\end{axis}
\end{tikzpicture}
\caption{Neurčitost  $h(p_i)$, definovaná vztahem (5) pro  $n=2$}
\end{figure}

Pravděpodobnost správné odpovědi na určitou dotazníkovou položku závisí jak na úrovni respondenta (tu charakterizuje latentní proměnná \textit{schopnost}), tak i na \textit{obtížnosti} položky (respondenti stejné schopnosti reagují (odpovídají) na rozdílné položky různě, ale podobně na tutéž položku). Experimentálně se zjišťovala závislost počtu správných odpovědí v~reprezentativní skupině respondentů na schopnosti (předpokládalo se, že schopnost jeúměrná celkovému počtu správných odpovědí dotazníku s~větším počtem položek, schopnější je ten, kdo odpoví správně na vice položek). Výsledkem experimentu byla esovitá křivka závislosti relativních četností správných odpovědí na určitou položku na schopnosti jedince. Jedna z~možných aproximací této křivky byla gaussovskou distribuční funkcí (získaná stupnice schopností se pak nazvala \textit{probitová}), druhá z~aproximací je \textit{logitová} pomocí logistické funkce (8) buď s~jedním parametrem  $b$ (G. Rasch, \cite{5}) nebo logistické funkce (9) se dvěma parametry $a,b$ (A. Birnbaum, \cite{6}):

\begin{equation}
\mathit{Ra}(x)=p(x)=\frac 1{1+\ee^{-(x-b)}};\quad 
b,x\in (-\infty; +\infty) 
%(8)
\tag{8}
\end{equation}

\begin{equation}
\mathit{Bir}(x)=p(x)=\frac 1{1+\ee^{-a(x-b)}};\quad 
b,x\in (-\infty;+\infty);\quad 
a>0.
% (9)
\tag{9}
\end{equation}

Parametr  $b$ se interpretuje jako obtížnost dotazníkové položky, parameter  $a$ jako její diskriminační schopnost. Odhady uvedených parametrů se realizují z~výsledků odpovědí na dotazník na základě logistické regrese jak jsme již naznačili.

Dosadíme-li nyní pravé strany z~výrazů (8) nebo (9) do některého z~předchozích vztahů za pravděpodobnost správné odpovědi  $p_i$, dostaneme hlubší souvislosti mezi neurčitostí (případně informací), $i$-tou dotazníkovou položkou a schopností jedince. (Podrobněji v~\cite{2}.) Ještě připomeňme, že z~psychologického hlediska není ještě daný problem vyřešen. Schopnost není jednorozměrná veličina, závisí ještě na obsahu položek (ten jsme ale neuvažovali). Aby bylo možné vypočítané veličiny (entropii a informaci) správně interpretovat, musí být dotazníkové položky homogenizovány vzhledem ke svému obsahu (k~tomu může posloužit například faktorová analýza, která položky roztřídí s~hlediska obsahu). Další důležitou veličinou pro analýzu dotazníku je čas, který k~odpovědi respondent má. Víme, že existují jisté zlomové okamžiky, které mají na reakci respondenta podstatný vliv, pravděpodobnost správné odpovědi není vždy spojitou funkcí schopností či času (rozvinutější úvahy o~tom jsou například v~\cite{7}).

%Obr. 3
\begin{figure}[!hbt]
\centering
\begin{tikzpicture}
\begin{axis}
  [
  title={Pro $x=2$},
  %view={45}{-70},
  xlabel={Parametr $a$},
  ylabel={Parametr $b$},
  zlabel={Sémantická informace $I^s(p)$},
  zmin=-1,
  x dir=reverse,
  y dir=reverse,
     /pgf/number format/.cd, use comma,
    xtick distance=0.2,
    ytick distance=5,
    ztick distance=0.5,
  ]
\addplot3
  [
  %surf,
  mesh,
  draw=black,
  %scatter,
  fill=white,
  %only marks,
  %contour lua,
  %gray,
  domain=0:1,
  domain y=-10:10,
  %y dir=reverse,
  ] file {zdrojaky/pulpan/graf/vysledky.txt};
  %{1/(1+exp(-x*(2-y))};
\end{axis}
\end{tikzpicture}
\caption{Graf sémantické informace položky, odvozený z~(3) dosazením za $p$ z~pravé strany vztahu (9) pro $x = 2$ v~závislosti na parametrech $a, b$}
\end{figure}

Následující Obr. 4 ukazuje dvourozměrný graf sémantické informace dotazníkové položky při hodnotě parametru $b = 0$ a v~závislosti na úrovni respondenta $x$ při různých hodnotách parametru $a$. Závislost není lineární, ale s~růstem úrovně respondenta $x$ a při konstantním parametru $b$ roste sémantická informace.

%Obr. 4
\begin{figure}[!hbt]
\centering
\begin{tikzpicture}%[scale=1.0]
\begin{axis}
    [
  xlabel={Parametr $x$},
  ylabel={Sémantická informace $I^s(p)$},
  title={Pro parametr $b=0$},
    /pgf/number format/.cd, use comma,
    %xtick distance=0.5,
    %minor tick num=3,
    %tickwidth=8pt,
    %xticklabels={0,0.2,...,1}
    %xlabel={$p_i$},
    %ylabel={$h(p_i)$},
    %xmajorgrids,
    %xminorgrids,
    ]
    \addplot[thick] file {zdrojaky/pulpan/graf/vysl-100.txt};
    \addplot[thick,dashed] file {zdrojaky/pulpan/graf/vysl-75.txt};
    \addplot[thick,dotted] file {zdrojaky/pulpan/graf/vysl-50.txt};
\end{axis}
\end{tikzpicture}

\caption{Dvourozměrný graf sémantické informace dotazníkové položky ze vztahu (3) po dosazení za  $p$ z~pravé strany výrazu (9) při hodnotě parametru $b = 0$ a v~závislosti na úrovni respondenta $x$ při různých hodnotách parametru $a$}
\end{figure}
% \includegraphics[width=6.0626in,height=4.0311in]{pulpanverze2-img/pulpanverze2-img003.png} 

\textit{Obtížnost} $i-$té položky se v~klasické testové teorii určuje jako  $o_{1i}=1-p_i$. Podle Birnbauma ji ale musíme uvažovat jako funkci položkových parametrů  $a,b$ a úrovně schopnosti jedince  $x$. Sémantizovaná informace nám také umožňuje definovat obtížnost výrazem 

\begin{equation}
o_{2i}=
\left\{
\begin{matrix}
1-\frac{H(p)} 2& \text{ pro } 0\leq p\leq0{,}5,\\
 \frac{H(p)} 2& \text{ pro } 0{,}5\leq p\leq1.
\end{matrix}
\right.
\tag{10}%(10)
\end{equation}

Do vztahu (10) je možno za  $H(p)$ dosazovat buď výraz z~(1b), (4) nebo (5a). Pokaždé ovšem dostaneme jiné hodnoty, platí však ve všech případech, že s~rostoucím  $p$ klesá obtížnost, měřená na stupnici od 0 do 1.

Viděli jsme, že je více možností jak odhadovat prostřednictvím pravděpodobnosti množství informace, potřebné k~určitému rozhodnutí a jaká je případně jeho obtížnost. V~následujícím odstavci si ukážeme jiné metodické východisko k~podobným odhadům.


\section{Neurčitost modelovaná fuzzy množinami a intuicionistickými fuzzy množinami}

Ohodnocení volby určité varianty pravděpodobností je někdy velmi obtížné nebo není vůbec možné. Například rozhodnutí, zda je pacient vyléčený nebo nevyléčený záleží jen na úvaze odborníka, podobně rozhodování o~tom, že určitý žák je nadaný nebo nenadaný se nedá statisticky rozhodnout (i když se takové pokusy realizují). Místo objektivizovaných statistických šetření se musí uskutečňovat expertní odhady. I~v~situacích, kdy nelze od sebe přesně “oddělit” některé varianty se uplatňuje teorie fuzzy množin. Fuzzy množina reprezentuje sémantiku pojmu rozhodnutí. Odpovídá na to, jak “silně” jsou v~rozhodnutí (které je fuzzy množinou) zastoupeny určité prvky ze souboru  $\Omega $.

Uvažujme pouze konečnou množinu prvků (možností, vlastností)  $\Omega =\{\alpha _1,\alpha _2,\dots ,\alpha _n\}$. Jestliže každému prvku  $\alpha _i$ přiřadíme míru  $\mu _i=\mu (\alpha _i)$,  $\mu _i\epsilon \left<0;1\right>$, vyjadřující, že  $\alpha _i$ je součástí rozhodnutí  $\underline A$, je  $\underline A$ fuzzy množinou nad  $\Omega $. Čím bližší je hodnota  $\mu _i$ jedné, tím spíše je v~rozhodnutí  $\underline A$ prvek  $\alpha _i$, hodnota  $\mu _i=0$ znamená, že v~rozhodnutí  $\underline A$ prvek  $\alpha _i$ není. Fuzzy množinu můžeme zapisovat ve tvaru

\begin{equation}
\underline A=  \{\alpha _1/\mu _1,\alpha _2/\mu _2,\dots ,\alpha _n/\mu _n\}.
%(11)
\tag{11}
\end{equation}

Hodnoty  $\mu _i$ nevznikají obecně jako pravděpodobnosti, představují expertní rozhodnutí o~míře zastoupení  $\alpha _i$ v~rozhodnutí  $\underline A$. Proto nemusí obecně platit, že  $\sum \mu _i= 1$. (Pro další úvahy prvek  $\alpha _i,$ jehož  $\mu _i=0$ do seznamu  $\Omega $ zařazovat nebudeme.)

Neurčitost, o~které budeme zde uvažovat, má svůj původ v~nejistotě odhadu, které prvky  $\alpha _i$ do  $A$ patří. Soustřeďme se nyní na určitou vlastnost  $\alpha \in \Omega $. Jaká je neurčitost zastoupení  $\alpha \in \Omega $ ve fuzzy množině  $A$?

Je možné vyjít z~modelu rozhodování charakterizovaném fuzzy množinou  $B$, kde  $\beta =\Omega -\{\alpha \}$

\begin{equation}
\underline B = \{  \alpha / \mu (\alpha ); \beta / \mu (\beta ) \} ,\quad 
\mu (\alpha )+ \mu (\beta )  \leq1
%$(12)
\tag{12}
\end{equation}
kde míry věrohodnosti $\mu (\alpha ), \mu (\beta )  \in \left<0,1\right>$ jsou také subjektivním nebo objektivizovaným odhadem volby příslušné varianty. Neurčitost pro fuzzy množinu (12) můžeme při nezávislých volbách variant odhadnout ze vztahu (13) (viz \cite{2})

\begin{equation}
\begin{matrix}
H(\underline B) &= & 2 - [ \max\{ \mu (\alpha ), 1 - \mu (\alpha )\} + \max\{ \mu (\beta ), 1 — \mu (\beta )\} ] \\
  &= &  \min \{ \mu (\alpha ), 1-\mu (\alpha )\} + \min \{ \mu (\beta ),1-\mu (\beta )\}\ [\mathrm{lit}];\quad  0 \leq H( \underline B) \leq 1.
%(13)
\end{matrix}
\tag{13}
\end{equation}
 
Informaci $I ( \alpha )$ pro volbu varianty $\alpha $ i její sémantizovanou hodnotu $I^s (\alpha  )$ určíme podobně jako pro klasickou pravděpodobnostní neurčitost $H(p)$ z~následujících vztahů:

\begin{equation}
I~( \alpha) = 1- H ( \underline B);\quad
I^s (\alpha  ) = \left\{
\begin{matrix}
-I(\alpha ); & 0 \leq \mu (\alpha ) \leq 0{,}5\\
I(\alpha );&  0{,}5 \leq \mu (\alpha ) \leq 1.
\end{matrix}\right. 
%(14)
\tag{14}
\end{equation}

Pak ovšem je $-1  \leq I^s (\alpha  )  \leq  1$. Zápornou hodnotu informace můžeme interpretovat jako míru dezinformace nebo pomýlenosti apod. vzhledem k~volbě varianty $\alpha$. Jednotky informace odpovídají jednotkám neurčitosti (\cite{2}).

Pro fuzzy množinu $\underline B$ nemusí obecně platit následující vztah (15), který odpovídá základní vlastnosti pravděpodobnosti pro dvě vzájemně se vylučující varianty  $\alpha ,\beta$ (viz \cite{2,4})

\begin{equation}
\mu (\alpha )  + \mu (\beta ) = 1 
%(15) 
\tag{15}
\end{equation}

Protože jsou však v~našem modelu volby variant na sobě závislé (popisujeme situaci volby jen právě jedné z~variant), musíme předpokládat platnost vztahu (15). Pak neurčitost pro fuzzy množinu (12) za podmínky (15) můžeme odhadnout např. ze vztahu (16), který odpovídá (1a):

\begin{equation}
H(\underline B) =  -\mu (\alpha )\cdot \log _2\mu (\alpha )-(1-\mu (\alpha ))\log _2(1-\mu (\alpha ))\ [\text{bit}] 
%(16) 
\tag{16}
\end{equation}

Pak je také $0  \leq  H(\underline B)  \leq  1$ a informaci i její sémantizovanou podobu můžeme stanovit podobně jako v~předchozím případě.

Máme však více možností pro odhad neurčitosti $H(\underline B)$ tak, aby byly splněny intuitivní odpovídající podmínkám (2) a (15) (viz také \cite{2}). Jednoduchý vztah pro neurčitost $H(\underline B)$, předpokládáme-li platnost podmínky (15), můžeme vyjádřit i ve tvaru(porovnejte se vztahem (4)):

\begin{equation}
H(\underline B) = 2\cdot \min \{ \mu (\alpha ), 1-\mu (\alpha )\}\ [\text{lit}];\quad
0  \leq H(\underline B) \leq 1. 
%(17)
\tag{17}
\end{equation}

\textit{Poznámky:}

1. Analogicky s~neurčitostí, definovanou pro diskrétní rozdělení podle (5), je možné určit i neurčitost fuzzy množiny (11) s~odpovídající interpretací ve tvaru
$H(\underline A)=1-\sqrt{\frac 4 n\sum _1^n(\mu _i-0{,}5^2}$ [lit],  
$0 \leq H(\underline A) \leq 1$,
$H(\underline A)=0$ když pro každé $i$ je  $\mu _i$ buď rovno 0 nebo 1,  
$H(\underline A)=1$ když všechna  $\mu _i=0{,}5$.


2. V~práci \cite{2} jsou uvedeny další možnosti volby definic neurčitosti fuzzy množin. Jeden z~příkladů vychází z~definice funkce  $h(p_i)$ podle (6a). Pro fuzzy množinu (11) definujeme neurčitost  $H(\underline A)$ následujícím vztahem 

$$H(\underline A)=\frac 1 n\sum _1^nh(\mu _i)\ [\text{lit}].$$

Pak je zřejmě  $0 \leq H(\underline A) \leq 1$. Nejmenší hodnoty $H(\underline A)$ dosahuje, když všechna  $\mu _i=0$ nebo 1 a největší hodnoty, když všechna  $\mu _i=0{,}5$. Má i další vhodné vlastnosti. (\cite{2})

3. Shannonova metoda posuzování neurčitosti se může uplatnit i pro fuzzy množiny, jen je třeba miry věrohodnosti  $\mu (\alpha _i)$ pro fuzzy množinu  $\underline A$ normovat. Odpovídající míra neurčitosti je pak tvaru

$$H(\underline A)=-\frac{1} n\sum _1^n\frac{\mu _i}{\sum _1^n\mu _i}\log _2\frac{\mu _i}{\sum _1^n\mu _i}\ [\text{bit}],\quad
0 \leq H(\underline A) \leq 1.
$$

Interpretace předchozí neurčitosti je ale obtížnější než ta, která odpovídá vztahu založeném na pravděpodobnostním rozdělení. (Mezi pravděpodobnostním rozdělením a fuzzy množinou je často velký rozdíl i v~interpretaci zdánlivě odpovídajících vztahů. Předchozí vztah lze jako míru neurčitosti s~dobrou interpretací užít jen pro fuzzy množinu  $\underline B$.)

Chceme-li přizpůsobit model nejjednoduššího rozhodování i situaci, kdy nemusí být v~tomto procesu vybrána žádná z~nabídnutých možností (když např. rozhodovatel (respondent) má odpor vůči volbě některé z~variant nebo neví jak se má rozhodnout), pak je vhodné předpokládat, že 

\begin{equation}
\mu (\alpha ) + \mu (\beta )  \leq 1. 
%(18)
\tag{18}
\end{equation}

V~tom případě lze pro charakterizaci rozhodování mezi dvěma variantami $\alpha , \beta$ využít intuicionistickou fuzzy množinu (IFS) 
$\mathcal{F}$
v~následujícím tvaru (\cite{1})

\begin{equation}
\mathcal{F}= \{ \alpha / (\mu (\alpha ), \nu (\alpha ));\ 
\beta  / (\mu (\beta ), \nu (\beta ))\} 
%(19)
\end{equation}
kde $\mu (\alpha ), \nu (\alpha ) \in \left< 0; 1 \right> $, také 
$\mu (\beta ), \nu (\beta ) \in \left< 0; 1 \right>$ a 
$\mu (\alpha ) + \nu (\alpha )  \leq 1,\ 
\mu (\beta ) + \nu (\beta ) \leq 1$.

Rozhodování o~variantě $\alpha$ (a odpovídajícím způsobem i o~variantě $\beta$) je tak rozděleno do 3 hledisek:

a) posuzující míru přijetí varianty $\alpha$  (odhadováno $\mu (\alpha )$),

b) posuzující míru nepřijetí varianty $\alpha$ (odhadováno $\nu (\alpha )$),

c) posuzující míru nerozhodnosti pro volbu varianty $\alpha$ (určeno hodnotou $1- \mu (\alpha )- \nu (\alpha ) $).

Fuzzy množiny, odpovídající uvedeným typům rozhodování jsou

$$
\begin{matrix}
A1(\alpha ) &=& \{ \alpha / \mu (\alpha ); \neg\alpha / 1 - \mu (\alpha )\}; \\
A2(\alpha ) &=& \{ \alpha / \nu (\alpha ); \neg \alpha / 1 - \nu (\alpha )\}; \\
A3(\alpha ) &=& \{ \alpha / \pi (\alpha ); \neg \alpha / 1 - \pi (\alpha )\}, \quad
\text{kde } \pi(\alpha ) = 1- \mu (\alpha )- \nu (\alpha )
\end{matrix}
$$
a jejich neurčitosti $H(\underline A_1(\alpha )),\ H(\underline A_2(\alpha )),\ H(\underline A_3(\alpha ))$ určíme podle (16) nebo (17).

Pro IFS $\mathcal{F}_z$ (19) lze však také zavést neurčitost $H (\alpha )$, týkající se varianty $\alpha$  vztahem

\begin{equation}
H (\alpha ) = -(\mu (\alpha ) \log _2\mu (\alpha ) + 
  \nu(\alpha ) \log _2\nu (\alpha ) + \pi (\alpha ) \log _2\pi (\alpha ))\ [\text{bit}] 
  \tag{20} %(20) 
\end{equation}
a je pak 
\begin{equation}
0 \leq H (\alpha )  \leq   \log _23. 
\tag{21} %(21)
\end{equation}

Pro neurčitost $H (\alpha )$ a neurčitosti $H(\underline A_1(\alpha )),\ H(\underline A_2(\alpha )),\ H(\underline A_3(\alpha ))$ platí nerovnosti

\begin{equation}
H(\underline A_1(\alpha ))  \leq  H (\alpha );\quad 
H(\underline A_2(\alpha ))  \leq  H (\alpha );\quad 
H(\underline A_3(\alpha ))  \leq  H (\alpha ). 
\tag{22}%(22)
\end{equation}

(To vyplývá z~následující posloupnosti nerovností: 
$\log_2 (\nu + \pi) \geq \log_2 \nu;\ 
-\log_2 ( \nu + \pi) \leq -\log_2 \nu;\ 
-\nu \log_2 (\nu + \pi)  \leq -\nu \log_2 \nu$.)

Nerovnost (21) nám dává možnost určit vzhledem k~variantě $\alpha $ pro IFS $\mathcal{F}$ informaci $I(\mathcal{F})$ vztahem (23a)

\begin{equation}
I_\alpha (\mathcal{F}) = \log _23 - H (\alpha ) 
\tag{23a}%(23a)
\end{equation}
nebo její normovanou podobu ve tvaru
\begin{equation}
I_\alpha (\mathcal{F})_\text{norm} = 1 - H (\alpha )/ \log _23 
\tag{23b}%(23b)
\end{equation}
a pak i příslušnou její sémantizaci vztahem 

\begin{equation}
I_\alpha {(\mathcal{F})_\text{norm}}^\text{s} =  
\left\{
\begin{matrix}
-I\alpha (\mathcal{F})_\text{norm};\quad 0 \leq \mu (\alpha ) \leq 0{,}5\\
I\alpha (\mathcal{F})_\text{norm};\quad 0{,}5<\mu (\alpha ) \leq 1
\end{matrix}\right.
\tag{24}%(24)
\end{equation}
kde je $-1 \leq  {I_\alpha(\mathcal{F})_\text{norm}}^\text{s}  \leq  1.$

Při volbě právě jedné z~možných variant $\alpha , \beta$ v~našem modelu rozhodování popsaném IFS $\mathcal{F}$ musí, krom původních podmínek, také platit (18), proto 

\begin{equation}
\\nu(\alpha ) + \nu(\beta ) \leq 1.
\tag{25}%(25)
\end{equation}

Neurčitost pro IFS $\mathcal{F}$ pak můžeme za všech uvedených podmínek definovat vztahem

\begin{equation}
H(\mathcal{F})(\alpha ) = H(\underline A_1(\alpha ))+ H(\underline A_2(\alpha ))+ H(\underline A_3(\alpha )) 
\tag{26}%(26)
\end{equation}
a platí $0  \leq  H(\mathcal{F})(\alpha )  \leq 3$. Proto můžeme definovat pro IFS $\mathcal{F}$ i informaci $I (\mathcal{F})(\alpha )$ vztahem

$$
I (\mathcal{F})(\alpha ) = 3 - H(\mathcal{F})(\alpha )
$$
nebo jako normovanou informaci 
$$
I (\mathcal{F})_\text{norm}(\alpha ) = 1- H(\mathcal{F})(\alpha )/ 3.
$$

Uvedenou normovanou informaci můžeme také sémantizovat vzhledem k~variantě $\alpha$, podobně jako jsme to udělali v~předchozích případech. Volíme

\begin{equation}
I~(\mathcal{F})^\text{s} (\alpha ) =  \left\{
\begin{matrix}
-I(\mathcal{F})_\text{norm}(\alpha );\quad 0 \leq \mu (\alpha ) \leq 0{,}5\\
I(\mathcal{F})_\text{norm}(\alpha );\quad 0{,}5 \leq \mu (\alpha ) \leq 1
\end{matrix}\right.
\quad
-1 \leq I~(\mathcal{F})^\text{s} (\alpha ) \leq 1. 
\tag{27}%(27)
\end{equation}

Pro variantu $\beta$ můžeme odvodit odpovídající vztahy pro informaci obdobně.

Další otázkou je, jak variabilita odhadu hodnot příslušných věrohodností může ovlivnit hodnoty sémantizovaných informací. Ukážeme možný odhad chyby pro neurčitost $H (\mathcal{F}, \alpha )$, když $\mu (\alpha ) =  \overline{\mu }(\alpha )  \pm {\Delta}\mu (\alpha )$, kde  $\overline{\mu }(\alpha )$ je bodový odhad hodnoty $\mu (\alpha )$ z~měření (např. aritmetickým průměrem z~experimentálních dat) a  $\Delta\mu (\alpha )$ je odhad chyby měření (např. směrodatnou odchylkou z~měření):

$$
\Delta H(\mathcal{F},\alpha ) \sim
\frac{\text{d} H}{\text{d}\mu }\cdot
\Delta\mu (\alpha )= 
\left( \log _2\frac{1-\mu (\alpha )}{\mu (\alpha )}
\right) \cdot \Delta \mu (\alpha ).
$$

\section{Závěr}

Hodnoty sémantizovaných informací jsou stanovovány v~závislosti na možnosti získání vstupních informací. V~našem případě jde buď o~odhad pravděpodobnosti  $p$(subjektivní nebo objektivizovaný) nebo měr věrohodnosti  $\mu $. Interpretace získaných numerických hodnot je pak dána výchozími předpoklady metody i zkušeností experimentátora v~určité oblasti diagnóz. Metoda se hodí \textit{pro porovnávání} hodnot sémantizované informace různých diagóz. Nepracuje se zde s~apriorně danými a zcela exaktně definovanými jednotkami ani procedurami určení neurčitosti. Nemáme totiž v~humanitních vědách jiné možnosti. Záleží pak na zkušenosti experimentátora, jakou z~metod odhadu a i příslušného výpočtu bude volit a jak se naučí číselné hodnoty určené z~volených vzorců interpretovat. Že lze přitom dojít i k~dostatečně přesným výsledkům jsme naznačili v~práci \cite{9}.

Článek ukazuje, jak je možné z~kvalitativních informací zkonstruovat kvantitativně posuzovatelnou jistou vlastnost informace. 


\renewcommand\refname{Literatura}

\begin{thebibliography}{99}

\bibitem{1}%[1] 
Atanassov, K.T.,Intuitionistic Fuzzy Sets, Fuzzy Sets and Systems 20 (1986),87-96

\bibitem{2}%[2] 
Půlpán, Z., Odhad informace z~dat vágní povahy, Academia, Praha 2012, 200s.

\bibitem{3}%[3] 
Půlpán, Z., Mezurado bazita sur la nocio de entropio, grkg/Humankybernetik, Band 51, Heft 2, 2010, 70-79

\bibitem{4}%[4] 
Novák, V.: Fuzzy Sets and their Applications, Adam Hilger, Bristol and Philadelphia, SNTL Praha 1986, 248 s.

\bibitem{5}%[5] 
Rasch, G.: Probabilistic models for some intelligence and attainment tests, Copenhagen, Danish Institute for Educational Research, expanded edition, The University of Chicago Press, 1980 (1960 - 1980)

\bibitem{6}%[6] 
Birnbaum, A: Some latent trait models and their use in inferring anexaminee´s ability, in Lord F., N. \&Novick, M., R.(eds): Statistical theories of mental test scores, Reading, MA, Addison-Wesley (1968)

\bibitem{7}%[7] 
Půlpán, Z.: K~problematice hledání podstatného v~humanitních vědách, Academia Praha, Studie 1/2001, 135s.

\bibitem{8}%[8] 
Fisher, G.,H., Molenaar, I., W.: Rasch models: foundations, recent developments and applications, Springer-Verlag, New York 1995

\bibitem{9}%[9] 
Půlpán, Z., Jahodová Berková, A.: Hodnověrnost a intuitivně odhadovaná pravděpodobnost, Informační bulletin České statistické společnosti, roč. 30, č.1, březen 2019

\bibitem{10}%[10] 
Lord, F., M.: Applications of Item Response Theory to Practical Testing Problems. Hillsdale, NJ, Erlbaum 1980

\bibitem{11}%[11] 
Birnbaum, A.: Statistical theory for the logistic mental tests models with a prior distribution of ability, J. Math. Psychol. 6, 1969

\end{thebibliography}

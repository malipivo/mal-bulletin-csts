% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode 
% !TEX root = ../../mal-core.tex

\gdef\mujnazevCS{Několik poznámek o~náhodě}
\gdef\mujnazevEN{}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Jaromír Antoch}

\def\textapprox{{\fontencoding{T1}\selectfont\raisebox{0.5ex}{\texttildelow}}}

\bonushyper
\def\textquotedbl{>>}
\newfontfamily\pajova{CMU Serif}[Ligatures=TeX]
\def\textcyrillic#1{{\pajova#1}}

\nazev{\mujnazevCS}

%\nazev{\mujnazevEN}

\author{\mujauthor}

%\Adresa{}

\Email{antoch@karlin.mff.cuni.cz}


\section*{Shrnutí}
V~této úvaze autor popisuje jeden možný subjektivní pohled na pojem náhoda. Vzhledem k~šíři problematiky, její složitosti a~omezenému prostoru nejsou ani zdaleka pokryty všechny možné aspekty daného problému. Pozornost je soustředěna především na problematiku náhody v~oblasti vybraných hazardních her.

\section*{Začněme stručným výběrem z~klasiků}

Nejenom většina učebnic, ale i~populárních a~popularizujících knih o~pravděpodobnosti a~matematické statistice, se kterými jsem se v~životě setkal, začíná nějak takto:

\begin{enumerate}

\item Teorie pravděpodobnosti se zabývá jevy, které nastávají při \textit{hromadných dějích náhodné povahy}. Teorie pravděpodobnosti je odvětvím matematiky, a~tedy abstraktním vědním oborem… 
\par
Alfréd Rényi, Teorie pravděpodobnosti. \cite{10}

\item O~\textit{náhodném pokusu} hovoříme tehdy, když konáme pokus, jehož výsledek není jednoznačně určen podmínkami, za nichž je prováděn. Cílem první části skript je pro pojmy \textit{náhodný jev} a~\textit{pravděpodobnost} najít vhodný matematický model… 
\par
Karel Zvára a~Josef Štěpán, Pravděpodobnost a~matematická statistika. \cite{11}

\item Všem takovým činnostem, jejichž výsledek není jednoznačně předurčen podmínkami, za kterých probíhají, a~které jsou (alespoň v~zásadě, teoreticky) neomezeně mnohokrát opakovatelné za stejných podmínek, říkáme \textit{náhodné pokusy}… 
\par
Jiří Likeš a~Josef Machek, Počet pravděpodobnosti. \cite{12}

\item Teorie pravděpodobnosti studuje modely \textit{náhodných pokusů}, tj. takových pokusů, jejichž výsledek není zcela jednoznačně určen podmínkami pokusu… 
\par
Valerij Nikolajevič Tutubalin, Teorie pravděpodobnosti. \cite{13}

\item Začněme matematickým modelem pro popis \textit{náhodných jevů} a~jejich pravděpodobností… 
\par
Josef Štěpán, Pravděpodobnost a~statistika na střední škole, Základy pravděpodobnosti. \cite{14}

\item Užitečnost teorie pravděpodobnosti a~matematické statistiky je dnes uznávána nejenom ve světě, ale i~u~nás. Postupně i~zde lidé zjišťují, že rozhodování za přítomnosti \textit{prvku náhody} je třeba opřít o~statistická data… 
\par
Jiří Anděl, Matematika náhody. \cite{15}

\item Většina jevů, s~nimiž se setkáváme, má \textit{náhodný charakter}… Dlouho trvalo, než se podařilo vybudovat vhodný matematický model \textit{náhodného pokusu}. Dnes se používá převážně model, který vypracoval Andrej Nikolajevič Kolmogorov\footnote{Jak mi opakovaně zdůrazňoval profesor Klebanov, Andrej Nikolajevič Kolmogorov se zvláště na konci svého života snažil slovo náhoda, a~náhodu vůbec, úplně eliminovat z~pravděpodobnosti (\textcyrillic{нету понимания случайности}). Kolegy i~své studenty se snažil přesvědčit o~tom, že náhodnost nemá v~pravděpodobnosti co dělat.}… 
\par
Jiří Anděl, Statistické úlohy, historky a~paradoxy. \cite{16}

\item Pokusy, jejichž výsledek se od jednoho provedení pokusu k~druhému mění, nazýváme \textit{náhodné pokusy}. \textit{Výsledek náhodného pokusu závisí jednak na podmínkách }\textit{(pravidlech), které při jeho provádění dodržujeme, jednak na náhodě}. Přitom o~\textit{náhodném pokusu ve vlastním slova smyslu} mluvíme jedině tehdy, je-li za prvé měnlivost výsledků podstatná, a~za druhé, je-li ve výskytu výsledků obsažena určitá zákonitost…
\par
Pochopení plného významu termínu \textit{náhodný pokus} je poněkud ztěžováno tím, že v~něm je \textit{jednostranně zdůrazněna úloha náhody,} a~nijak se v~něm neodráží záměrnost a~plánovitost, s~jakou se tyto pokusy konají. Čtenář bude mít mnoho příležitostí vejít s~termínem \textit{náhodný pokus} ve známost a~přivyknout mu.
\par
Václav Dupač, Jaroslav Hájek, Pravděpodobnost ve vědě a~technice. \cite{17}
\end{enumerate}
\medskip

Autor se poté pustí do budování matematického modelu, pravděpodobnostního prostoru, náhodných jevů, a~jim odpovídajících pravděpodobností, atd. \textit{A~co to ta náhoda je?, }ptá se netrpělivý čtenář,\textit{ }protože ať knihou listuje jak listuje, zpravidla to v~ní nenajde. Přesněji\textit{, }skoro nikdy se nedozví, co autor pod pojmem náhoda rozumí, byť s~ním často \textit{vesele šermuje}. Zato se prakticky vždy dozví, jak vybudovat pravděpodobnostní prostor, (sigma) algebru (náhodných) jevů, náhodné veličiny atd. A~když se čtenář autora zeptá přímo, může-li, většinou se dozví, že to je příliš široké téma.

Řada čtenářů ale na výklad o~sigma algebrách není příliš zvědavá, protože matematiku, jak se dnes módně říká, \textit{až tak nemusí}. A~tak místo toho, aby studovali dál, začnou hledat náhradu v~tak zvaných statisticko\z pravděpodob\-nostních rodokapsech, jak říkával Josef Machek. Jejich hlavním zdrojem je nyní internet, kde na Wikipedii se pod heslem \textit{náhoda\footnotemark{}}\footnotetext{Jak mne upozornil kolega Dvořák, česká wiki stránka je v~tomto poněkud matoucí, neboť v~úvodu popisuje to, čemu angličtina říká \textit{Coincidence}. Anglická jazyková verze, na kterou se lze z~české verze na jedno kliknutí dostat, vede na stránku \textit{Randomness,} je pro naši debatu relevantnější. Jinými slovy, česká Wikipedie se v~případě hesla náhoda vyjadřuje k~něčemu trochu jinému.}
čtenář nejprve dozví, že \textit{Náhoda v běžné řeči označuje nečekaný souběh nesouvisejících událostí…} Autoři hesla dále rozebírají náhodu v~běžném životě, náhodu ve vědě, a~především náhodu jako filosofické téma, kam asi tento pojem nejspíše patří. O~pravděpodobnostním či statistickém pohledu na náhodu se čtenář nedozví prakticky nic – proč taky?! A~tak si otevře další webové stránky, na něž jej odkáže Google, a~zmatení mysli mnohdy nastává.

Autor by na tomto místě měl asi vysvětlit, proč se do této úvahy pustil. Hlavním důvodem je snaha si pro sebe sama alespoň částečně odpovědět na otázku, co pro něj pojem náhoda znamená. A~vyprovokovat další k~témuž. Významně k~tomu také přispěla letitá spolupráce s~Michalem Kulichem a~Jiřím Dvořákem při přípravě posudků pro MF ČR. Za podnětné diskuse musím oběma pánům velmi poděkovat. Především díky této spolupráci se autor v~uváděných příkladech soustředil, a~danou problematiku podstatně zúžil, na náhodu a~vybrané hazardní hry.



\section*{Jak se lze na náhodu dívat?}

Stručná odpověď je – různě. Typický čtenář našeho bulletinu dobře ví, že pohled teorie pravděpodobnosti a~matematické statistiky na náhodu se často liší, mnohdy podstatně, od běžného pohledu řady lidí. Pojem \textit{náhoda }je pro náš obor zpravidla spojen s~výsledky, jež nelze předem předpovědět s~jistotou. Ve většině případů, i~když ne ve všech, je náhoda také velmi úzce spojena s~nedostatkem informací. Na druhé straně, teorie pravděpodobnosti a~matematická statistika se nezajímají, a~ani se z~principu nemohou zajímat, o~všechny myslitelné náhodné pokusy a~experimenty, nýbrž zpravidla o~ty, které vykazují tak zvanou \textit{statistickou stabilitu}. Rozumí se tím ty náhodné pokusy, které lze opakovat, a~které, jsou-li opakovány za těch samých podmínek, poskytují výsledky, jež mají tendenci se „hromadit“. Myslí se tím většinou to, že opakujeme-li pokus $n$ krát za těch samých podmínek, $n_A$ značí počet výskytů sledovaného (náhodného) jevu $A$, potom $n_A/n$ má tendenci kolísat v~úzkých mezích. Tyto meze jsou zpravidla tím užší, čím více pokusů za týchž podmínek zrealizujeme. Přesné výsledky jednotlivých pokusů a/nebo jejich kombinací sice předpovědět nelze, nicméně lze stanovit šanci (pravděpodobnost, neurčitost, apod.), s~níž jednotlivé výsledky mohou nastat.

Jak již bylo řečeno, pojem náhoda (náhodný jev, náhodný princip, náhodný výsledek, náhodný úkaz, apod.) je většinou spojen s~výsledky, jež nelze předem s~jistotou předpovědět, resp. se situací, kdy nám chybí nějaká informace. Protože se jedná o~pojem velmi široký, pokusíme v~něm rozlišovat následujícím způsobem:

\begin{enumerate}
\itemsep=-1pt
\item \textit{Náhoda v~obecném slova smyslu}. Pod tímto typem náhody budeme chápat ty náhodné jevy (události, činnosti, pokusy, experimenty, apod.), jejichž výsledek nelze s~jistotou předpovědět ani tehdy, jsou-li předem známy veškeré podmínky, za nichž sledovaný jev nastane. Jinými slovy, jedná se o~ty jevy (události, činnosti, pokusy, apod.), jež nejsou jednoznačně určeny podmínkami, za nichž jsou realizovány.
\item \textit{Náhoda v~úzkém slova smyslu}. Pod tímto typem náhody budeme chápat ty náhodné jevy (události, činnosti, pokusy, apod.), vykazující statistickou stabilitu. Od náhodných činností v~obecném slova smyslu se náhodné činnosti v~úzkém slova smyslu liší tím, že lze předem vypočítat pravděpodobnosti výskytů všech možných výsledků těchto jevů (událostí, činností, pokusů, experimenty, apod.). Na tyto pravděpodobnosti se lze dívat jako na očekávaný podíl daných výsledků v~případě dlouhého opakování náhodného pokusu za týchž podmínek.
\end{enumerate}

Zatímco náhodu v~úzkém slova smyslu je teorie pravděpodobnosti a~matematická statistika vesměs schopna kvantifikovat, u~náhody v~obecném slova smyslu tomu tak nutně není. Na druhé straně, v~případě náhody v~obecném slova smyslu matematika a~statistika často nabízí modely umožňující více či méně přesně výsledky předpovídat, a~to nejenom výsledky sportovních utkání, dostihů, voleb všeho druhu, ale i~počasí nebo šanci na uzdravení těžce nemocného pacienta.

Někteří (před)čtenáři této úvahy proti výše uvedené klasifikaci leccos namítali, a~doporučovali spíše rozdělení na opakovatelné a~neopakovatelné jevy. Pro autora to však není to samé. Navíc se cítí do značné míry ovlivněn jak svými učiteli, tak diskusemi, jež na toto téma před lety vedl s~Josefem Machkem, jakož i~četbou knihy Tutubalinovy \cite{13}, kde autor na straně 14 píše: 

\textit{Všechny myslitelné pokusy lze rozdělit do tří skupin. V~první skupině jsou „dobré“ pokusy, u~nichž je zaručena úplná stabilita výsledků pokusu. V~druhé skupině jsou „horší“ pokusy, které nejsou sice úplně stabilní, jsou však statisticky stabilní. Do třetí skupiny patří zcela „špatné“ pokusy, které nejsou ani statisticky stabilní. V~první skupině je vše jasné i~bez teorie pravděpodobnosti. V~třetí skupině je tato teorie nepoužitelná. Druhá skupina je právě ona oblast, k~níž lze teorii pravděpodobnosti použít, avšak sotva kdy si můžeme být zcela jisti, že pokus patří k~druhé, a~ne k~třetí skupině...}

Jak mi mimo jiné napsal Michal Kulich\textit{: …Mluvit o~náhodě jen v~souvislosti s~náhodnými pokusy a~vyžadovat schopnost opakování pokusu za totožných podmínek, je v~praxi silně omezující. Žádný sportovní závod nemůže být nikdy za stejných podmínek zopakován. Maximálně v~nějakém mentálním cvičení, které s~realitou nemá nic společného. Přesto i~zde můžeme mluvit o~náhodě, pravděpodobnostech, modelech a~dělat rozumnou statistiku. Takže podmínka statistické stability ve většině aplikací a~situací nejde uplatnit, a~proto nemůže být vydávána za podstatu věci\footnotemark{}. A~experimenty dnešní společnost už prakticky přestala provádět. Je to pracné, stojí to peníze, člověk si s~tím musí namáhat mozek a~trvá to dlouho, tak se to nedělá. Pracujeme takřka výhradně s~daty získanými pasivním pozorováním procesů, které samovolně probíhají, a~my je zaznamenáváme (obvykle velmi neúplně a~nesystematicky). Toto také nikdy nejde zopakovat za totožných podmínek. Takže mám pocit, že to je celé ještě mnohem složitější. Atd. }\footnotetext{Zde autor úplně nesouhlasí, protože se domnívá, že tato podmínka je implicitně schována za řadou statistických modelů, které statistika pro řešení podobných úloh navrhla, a~jež na podobná data aplikuje.}
Závěr, s~ním autor stoprocentně souhlasí.

\section*{Náhoda a~hazardní hry}

Prakticky všechny knihy o~pravděpodobnosti někde na začátku zmiňují vybrané hazardní hry jako počátek počtu pravděpodobnosti\footnote{Profesor Klebanov mne nicméně upozornil, že podle některých ruských autorů jsou počátky pravděpodobnosti spojeny spíše s~pojistnými úlohami (pojištěním rizika), které byly pro snazší interpretaci převyprávěny v~jazyce hazardních her.}. Počátek, k~němuž posléze matematika vybudovala své modely. Na druhé straně, protože prakticky ve všech hazardních hrách podle okřídleného českého přísloví \textit{jsou peníze až na prvním místě}, snažil se na nich odjakživa kterýkoliv stát také něco „pro sebe trhnout“. A~proto u~nás, podobně jako jinde na světě, je hazardní hra „definována“ zákonem, a~zákon také výslovně říká, které typy hazardních her upravuje, a~jak. Nejenom pro pohodlí čtenáře, ale i~pro zajímavost, si proto připomeňme nejprve vybrané pasáže z~platného zákona.

\subsection*{Zákon o~hazardních hrách}

Na úvod připomeňme, že zákon č. 186/2016 Sb., \textit{Zákon o~hazardních hrách}, s~účinností od 1. ledna 2017, v~paragrafu tři, odstavec jedna \cite{2}, zavádí (mimo jiné) pojem \textit{hazardní hra}, a~v~odstavci dva jmenovitě vyjmenovává, které druhu hazardních her upravuje. Uveďme si pro zajímavost příslušné odstavce.

§\,3. Hazardní hra

\begin{enumerate}
\itemsep=-1pt
\item Hazardní hrou se rozumí hra, sázka nebo los, do nichž sázející vloží sázku, jejíž návratnost se nezaručuje, a~v~nichž o~výhře nebo prohře rozhoduje zcela nebo zčásti \textit{náhoda nebo neznámá okolnost}.
\item Tento zákon upravuje tyto druhy hazardních her:

\begin{enumerate}
\itemsep=-1pt

\item[a)]  loterii,

\item[b)]  kursovou sázku,

\item[c)]  totalizátorovou hru,

\item[d)]  bingo,

\item[e)]  technickou hru,

\item[f)]  živou hru,

\item[g)]  tombolu,

\item[h)]  turnaj malého rozsahu.
\end{enumerate}

\end{enumerate}


V~§\,50 je dále specifikována \textit{Hra technické hry}, zatímco v~§\,73 \textit{Internetová hra}. \cite{2}

Je dobré si všimnout, že všechny tyto klíčové paragrafy zásadně staví na~pojmu \textit{náhoda}. Přitom slovo \textit{náhoda} se v~zákonu jinak téměř nevyskytuje. Přesněji, zákon č.~186/2016 Sb. \cite{2} ji používá na pěti místech, zatímco předchozí zákon České národní rady č. 202/1990 Sb., \textit{O~loteriích a~jiných podobných hrách} \cite{1}, slovo náhoda používá dokonce pouze na třech místech. A~to přesto, že v~obou zákonech náhoda hraje klíčovou roli. Klíčový pojem \textit{náhoda} přitom není zákonodárcem nijak specifikován, alespoň ne z~pohledu matematika. Toto může vést, a~často vede, k~rozdílným interpretacím, které často končí soudními spory. Zájemce o~pochopení tohoto klíčového pojmu si přitom vůbec není jist, zda současná judikatura pro vymezení pojmu \textit{náhoda} vůbec poskytuje dostatečnou oporu. A~nejenom stručné, ale ještě více hluboké hledání na internetu tento zmatek, který je spojen i~s~tím, že různé profese a~obory, o~lidech nemluvě, si pod pojmem náhoda představují mnohdy diametrálně různé kategorie, jenom prohlubuje. 

Zkusme se proto na vybrané hazardní (podle zákona) hry podívat prizmatem náhody v~obecném a~úzkém slova smyslu, tak jak bylo tyto pojmy zavedeny výše. Zdůrazněme, že řazení odstavců nikterak neodráží důležitost té které kategorie. Podobně délka zpracování odráží spíše momentální zájem autora, nikoliv všeobecnou důležitost té které pasáže.

\section*{Loterie a~losování}

Loterie, ať již aktivní či pasivní, a~podobné typy losování výsledků, pokud jsou prováděny poctivě, a~to ať již taháním lístků či koulí z~osudí, nebo realizované pomocí specializovaných strojů (elektromechanických, elektronických, nebo obdobných), lze z~pohledu matematické statistiky a~teorie pravděpodobnosti považovat za zdroj náhody v~úzkém slova smyslu, neboť pravděpodobnosti očekávaných výsledků lze předem spočítat, resp. u~specializovaných strojů lze na základě opakovaných realizací jimi poskytovaných náhodných výběrů statisticky ověřit způsobilost příslušných strojů k~daným účelům\footnote{Publikované informace nicméně ukazují, že sestrojení specializovaných strojů vhodných pro dané účely je velmi obtížné, a~jejich empirické ověření ještě náročnější. }. 

Mezi aktivní loterie například patří Sportka, Loto, Keno, Euromiliony, starší a~pokročilí jistě pamatují Mates, a~jejich varianty. Mezi pasivní loterie\footnote{Tak zvané pasivní loterie zpravidla nabízejí losy, jež mají předtištěné sériové číslo, přičemž po ukončení prodeje se losuje. Los je tak dokladem o~účasti v~loterii, je obvykle číslovaný, a~je zároveň dokladem o~zaplacení určité částky jako vkladu do hry. Vyplacení případné výhry je podmíněno jeho předložením, takže lze los chápat jako poukaz na případnou výhru.} patřila například dříve velmi oblíbená Československá státní loterie či Loterie červeného kříže. V~některých evropských zemích, například v~zemích Iberijského poloostrova, jsou na rozdíl od střední Evropy pasivní loterie stále velmi oblíbené, například vánoční loterie \textit{El Gordo (Tlouštík)}. 

\section*{Tomboly}

Z~matematického hlediska lze často mezi loterie zařadit často také tomboly, pokud jsou prováděny poctivě, například taháním lístků z~osudí. Je však třeba uvažovat dva případy. 

\begin{itemize}
\itemsep=-1pt
\item Jestliže je výše výhry určena poctivým losováním, pak se zde uplatňuje, podobně jako u~loterií, princip náhody v~úzkém slova smyslu.
\item Existují také tomboly, které jsou realizovány způsobem \textit{každý los vyhrává}. Jestliže je výše či typ výhry určena poctivým losováním, a~je náhodná, potom se lze na danou situaci dívat jako na klasickou loterii, v~níž se uplatňuje princip náhody v~úzkém slova smyslu. Na druhé straně, pokud všichni vyhrávají totéž, jedná se o~cenu za účast v~soutěži, a~o~žádné náhodě zde mluvit nelze. 
\end{itemize}

Pro zajímavost poznamenejme, že podle současné legislativy je třeba hlásit Ministerstvu financí pouze tomboly, v~nichž hodnota všech výher převyšuje 100\,000 Kč\footnote{Zajímavý je také §\,62 zákona \cite{2}, kde se mimo jiné uvádí, že pravděpodobnost výhry nesmí být nižší než 1:200, a~úhrnná cena výher nesmí být nižší než 40\,\% a~vyšší než 80\,\% herní jistiny. Doufejme, že důvodem je snaha o~to, aby například na plesech politických stran nemohli sponzoři svou „podporu“ nemístně navyšovat.}.

\section*{Stírací losy}

Za podobnou kategorii jako loterie lze považovat stírací losy či stírací hry typu stírací poker, apod., neboť pravděpodobnosti jednotlivých výher lze předem spočítat na základě hracího plánu. Proto i~tento typ her lze považovat za zdroj náhody v~úzkém slova smyslu. Poznamenejme nicméně, že veškeré výpočty jsou zde založeny pouze na hracím plánu dané hry, a~na rozdíl od řady jiných loterií a~losování nemohou být ověřeny empiricky, neboť jednotlivé losy jsou setřením znehodnoceny. Toto tvrzení není stoprocentně pravda, neboť za cenu kontroly všech losů se může kontrolní orgán přesvědčit o~tom, zda by hra skutečně probíhala podle hracího plánu. Takové řešení je sice proveditelné, ale prakticky k~ničemu. Tatáž poznámka platí i~pro loterie pasivní, kde by bylo třeba zakoupit všechny losy a~zkontrolovat je.

\section*{Soutěže a~náhoda}

Vedle loterií, tombol, stíracích losů apod., diskutovaných výše, to jsou především soutěže, v~nichž náhoda může hrát, a~často hraje, důležitou roli. Jak uvidíme dále, z~pohledu matematické statistiky a~teorie pravděpodobnosti se vesměs jedná o~různé kategorie náhody, a~proto by loterie a~soutěže neměly být volně spojovány jenom proto, že v~nich vystupuje náhoda. Bohužel se tak často děje, což zavdává podklad pro četné spory a~nedorozumění, jakož i~pro soudní pře.

Velmi často diskutovanou otázkou také je, jak náhoda v~soutěžích vystupuje, a~především jak moc výsledky soutěží ovlivňuje. Jak uvidíme dále, pokud ve spojení se soutěží náhoda vystupuje, jedná se z~hlediska naší terminologie vesměs o~náhodu v~obecném slova smyslu. Rozdělme si proto nejprve soutěže na několik základním typů, a~rozeberme si každý typ odděleně. Poznamenejme, že zvolené dělení není, vzhledem k~proměnlivosti daného prostředí, nutně ani vyčerpávající ani jednoznačné. 

\begin{itemize}
\itemsep=-1pt
\item \textit{Rychlostní soutěže}
\item \textit{Soutěže obsahující otázku na budoucnost}
\item \textit{Soutěže, v~nichž vedle vlastního náhodného pokusu o~výsledku rozhoduje porota}
\item \textit{Soutěže, v~nichž vedle vlastního náhodného pokusu o~výsledku porota nerozhoduje}
\item \textit{Soutěže typu „každý vyhrává“}
\item \textit{Vědomostní (znalostní) soutěže}
\item \textit{Výkonnostní soutěže}
\item \textit{Spotřebitelské soutěže}
\end{itemize}

Než přejdeme k~podrobnější analýze jednotlivých kategorií soutěží, zastavme se u~jednoho důležitého bodu, jímž je porota, která v~řadě soutěží hraje podstatnou roli, a~jejíž rozhodování je často zatíženo jen těžko popsatelným stupněm náhody. \textit{Zde by autor chtěl zdůraznit své osobní přesvědčení, že určuje-li pořadí porota podle svých názorů, jedná se vždy o~soutěž.} Také by chtěl upozornit na často diskutovaný problém týkající se kompetence, nestrannosti a/nebo neutrality jejích členů. Například proto, že jsou organizátory často jmenováni podle ne vždy zcela jasných kritérií. Reálná praxe bohužel ukazuje, že i~v~případě kompetentních porot složených z~odborníků v~dané oblasti na slovo vzatých a~rozhodujících v~tak zvané\textit{ nejlepší víře,} se často rozhodování některých členů řídí příslovím: \uv{Někdo má rád holky, jiný zase vdolky.} Na závěr připomeňme, že pravděpodobnost možného korupčního jednání v~případě rozhodování porot může být nezanedbatelná.


\section*{Rychlostní soutěže}

V~hromadných sdělovacích prostředcích, především na rádiových stanicích, jsou populární soutěže typu: \textit{Vyhrává ten, kdo se dovolá jako první, kdo jako první zašle správnou odpověď pomocí SMS, kdo jako patnáctý zašle odpověď obsahující daný kód,} apod. Ve všech těchto případech je možno pravděpodobnost výhry přiřadit pouze na základě subjektivní úvahy, případně expertním odhadem. Náhoda se zde sice uplatňuje, ale pouze v~obecném slova smyslu. Poznamenejme, že na podobném principu jsou organizovány i~četné spotřebitelské soutěže. 

\section*{Soutěže obsahující otázku na budoucnost}

Cílem těchto soutěží bývá uhodnout něco, co nastane v~budoucnu, a~co v~současné době záleží na nepředvídatelných podmínkách. Vyhněme se mediálně oblíbeným sportovním nebo (před)volebním otázkám, a~zkusme si tipnout, jaká teplota bude na Nový rok roku 2023 v~pět ráno na meteorologické stanici v~Praze-Klementinu. V~tomto případě mají všichni soutěžící, resp. zájemci o~soutěž, k~dispozici historická data, dlouhodobé předpovědi pocházející z~různých modelů atd. To jim například umožňuje předpovědět pomocí statisticko-matematických postupů, a~tuto předpověď případně doplnit vlastními zkušenostmi. Je dobře známo, že veškeré předpovědi v~této oblasti jsou přitom zatíženy většími či menšími nejistotami. Úloha tedy spočívá v~nalezení co nejlepší (nejpřesnější) předpovědi, jež záleží na skutečnostech, jež mají povahu náhody v~obecném slova smyslu. Proto i~tento typ soutěží lze považovat za náhodný v~obecném slova smyslu. Zkušený meteorolog přitom bude asi mít větší šanci na úspěch než náhodně soutěžící občan. Je to podobné jako na psích dostizích, kde pravidelný návštěvník a~sázkař zná prostředí a~výkonnost soutěžících mnohem lépe než jednorázový návštěvník, který na dostihy zabloudil náhodou, a~vsadil si „pro legraci“. Na druhé straně, život nás mnohokrát poučil, že ne vždy vyhrává nejlepší a/nebo nejlépe připravený, neboť, jak říká přísloví: \uv{Náhoda je blbec!}

Poněkud složitější to je v~případě, je-li cílem uhodnout něco, co nastane v~budoucnu, co záleží na v~současné době nepředvídatelných podmínkách, a~co nebude možné jednoznačně určit ani v~okamžiku stanovení. Například, kolik lidí se zúčastní té které demonstrace, technoparty, či jiné podobné akce, apod. Je dobře známo, že v~takovýchto případech se zaručeně stoprocentní informace policie, tisku a/nebo organizátorů zpravidla diametrálně liší. A~to přesto, že panuje obecné přesvědčení, že díky současným technologiím je možno vše přesně určit. Počet účastníků pak musí stanovit některá autorita. Přestože lze tento typ soutěží zpravidla zařadit mezi náhodu v~obecném slova smyslu, závažným problémem bývá i~zde nestrannost a/nebo kompetence rozhodující autority. Navíc si lze snadno představit, že o~„rozhodující slovo“ se bude hlásit více paralelních autorit, jejichž závěry se budou lišit.

\section*{Soutěže, v~nichž vedle vlastního náhodného pokusu o~výsledku rozhoduje porota}

Specifickou kategorií jsou soutěže, v~nichž vedle kvality účastníků má důležité, mnohdy hlavní, slovo závěrečné rozhodnutí poroty. Do této kategorie patří nejenom řada sportů jako krasobruslení (skoky do vody, skoky na lyžích, apod.), ale i~volby královny krásy (taneční soutěže, apod.). I~tento typ soutěží lze zpravidla zařadit mezi náhodné v~obecném slova smyslu. 

Blízké předchozím úvahám jsou také tvůrčí soutěže, jež typicky spočívají v~tom, že soutěžící zašle podle předem vypsaných pravidel své dílo (obraz, foto, sochu, hudební opus, odborný vědecký projekt, technické řešení apod.). Ze zaslaných děl pak komise vybere jedno či více děl k~ocenění. Jakkoliv by měl být vybrán „nejlepší“ návrh, je možné, že jiná komise by mohla rozhodnout jinak. Hlavní potíž spočívá v~(ne)definici toho, co to je nejlepší, nejinovativnější apod. I~tento typ soutěží lze zpravidla zařadit mezi náhodné v~obecném slova smyslu.

Ať již existují problémy s~nestranností a/nebo kompetencí rozhodující poroty nebo nikoliv, praxe těchto dnů nám pravidelně ukazuje, že „poražení (nevybraní)“ výsledky často řeší, ať již oprávněně či nikoliv, stížnostmi nebo i~soudně.

Je-li rozhodnutí poroty navíc kombinováno s~názory diváků, kteří hlasují například pomocí SMS jako je tomu v~soutěži \textit{Star Dance … když hvězdy tančí}, je jakýkoliv matematický popis téměř nemožný, a~případné sázky/ kurzy jsou stanovovány na základě zkušenosti, expertních odhadů a~pocitů. Podobné soutěže lze vesměs zařadit mezi náhodné v~obecném slova smyslu, přičemž náhoda však často vystupuje velmi vzdáleně a~hraje pouze okrajovou roli, pokud vůbec.

\section*{Sportovní a~tvůrčí soutěže}

Události s~náhodným výstupem/koncem, v~nichž o~výsledku nerozhoduje porota, nýbrž především předem více či méně těžko předvídatelné okolnosti jakými jsou okamžitá výkonnost a~kvalita aktérů (hráčů, zvířat, apod.), počasí, stav hřiště či závodní dráhy, tlak publika apod., lze také zařadit mezi náhodné v~obecném slova smyslu. A~to i~přesto, že lidé navrhovali, navrhují a~budou navrhovat matematické a~statistické modely vycházející z~analýzy historických dat, informací o~okamžité formě aktérů, pravidelně prováděných výběrových šetřeních apod. Na druhé straně je třeba poznamenat, že řada těchto modelů poskytuje velmi dobré předpovědi, a~jejich kvalita se stále zlepšuje. Protože se na podobný typ soutěží odjakživa sázelo, je na zákonodárci, aby rozhodl o~jejich klasifikaci z~hlediska hazardních her.

\section*{Soutěže typu každý vyhrává}

Soutěže založené na principu, že každý účastník vyhrává, je vhodné rozdělit na dva případy. Pokud všichni vyhrávají totéž, tj. všechny výhry mají tutéž hodnotu, jedná se o~cenu za účast v~soutěži, a~o~žádné náhodě zde %lze 
nelze mluvit. Jestliže však je výše výhry určena poctivým losováním, pak se uplatňuje princip náhody v~úzkém slova smyslu, jak je diskutováno v~případě tombol výše. 

\section*{Vědomostní (znalostní) soutěže}

Především v~televizi a~na internetu se lze potkat s~řadou vědomostních (znalostních) soutěží. Kategorie znalostních soutěží je z~hlediska náhody značně ošemetná, a~každou soutěž je třeba pečlivě rozebrat odděleně. Na druhé straně je možno specifikovat některé typické zástupce a~podat charakterizaci jejich základních rysů. Zastavme se u~některých z~nich trochu podrobněji. 

\begin{itemize}
\itemsep=-1pt
\item V~řadě vědomostních soutěží si účastník vybírá postupně vždy jednu z~několika (zpravidla tří až pěti) možných odpovědí. Klasickým příkladem je populární televizní soutěž \textit{Chcete být milionář?}, v~roce 2008 nazývána \textit{Milionář}, hraná v~téže atmosféře a~za týchž podmínek po celém světě podle britského originálu \textit{Who wants to be a~millionaire?} Pořadí otázek je stanoveno předem a~soutěžící nemůže ovlivnit jejich pořadí. Princip náhody se zde objevuje omezeně v~některých fázích soutěže. 
\item Trochu jiná pravidla má (velmi populární) soutěž \textit{Kde domov můj}, hraná na ČT 1. Zde již soutěžící může částečně ovlivnit, na který typ otázky chce odpovídat, „přehodit“ úkol na jiného soutěžícího apod. Za náhodnou v~těchto soutěžích lze považovat volbu odpovědi v~případě, kdy soutěžící správnou odpověď nezná a~hádá. Princip náhody se zde objevuje omezeně nebo jen v~některých fázích soutěže, pokud vůbec. 
\end{itemize}
Proto je třeba velmi pečlivě rozebrat a~hodnotit každou podobnou soutěž zvlášť.


\section*{Náhoda a~internetový svět hazardních her}

Až do této chvíle jsme se mimo jiné vyhýbali internetovým hrám všeho druhu. Důvodem je, že se jedná o~problematiku na několik bulletinů, na něž, doufejme, časem také dojde. Chtěl bychom nicméně poznamenat, že nejenom četní hráči hazardních her, ale i~mnozí jejich provozovatelé, nemluvě o~řadě zákonodárců či novinářů, si neuvědomují, že náhoda v~internetovém světě hazardních her prakticky neexistuje. A~to bez ohledu na to, co si kdo pod pojmem představuje. To, co je vydáváno za náhodu, je zpravidla generováno některým deterministickým postupem, tj. algoritmem, který produkuje pevně danou posloupnost tak zvaných pseudonáhodných čísel, která jsou poté transformována na jednotlivé stavy dané hry podle zadaných pravidel. Termín pseudonáhodná čísla používáme ve shodě s~literaturou proto, že dané posloupnosti navenek působí, či mohou působit, jako náhodné a~úspěšně projít řadou statistických testů náhodnosti. \textit{To však nic nemění na tom, že každý, kdo zná tvar použitého generátoru, jeho implementaci a~počáteční startovací hodnoty, jakož i~algoritmus a~implementaci hry, si výstup může kdykoliv zreplikovat, čímž je jakákoliv náhodnost ztracena.} Stejné je to i~s~řadou (hazardních) her, které se hrají přes terminály všeho druhu v~kasinech, hernách či barech. 


\section*{Karetní hry a~náhoda}

Na závěr ještě pár slov k~problematice vlivu náhody v~karetních hrách. Jedná se o~nekonečné téma, kde není ani vítězů, ani poražených. V~každém případě, prakticky všechny karetní hry mají dvě základní fáze, tj. \textit{rozdávání} a~\textit{sehrávku}. Zatímco rozdávání a/nebo míchání, jsou-li prováděny poctivě, lze považovat za zdroj náhody v~úzkém slova smyslu, sehrávku nikoliv.  Pro tu spíše platí přísloví: \textit{Není umění hrát dobře s~dobrou kartou, ale umění je sehrát se špatnou kartou dobrou hru.}

Karty lidé hráli odjakživa jak pro zábavu, tak pro peníze\footnote{Karty jsou čertovy obrázky, říkávala babička.  Svádí k~hazardu, a~díky hře můžeš prohrát velké peníze.}. Jak svého času řekl Hospodářským novinám náměstek ministra financí Tomáš Zídek \cite{6,7}, veškeré karetní hry o~peníze by měly být zařazeny mezi hazard. \uv{Určitě chceme, aby poker a~další karetní hry patřily do kasin. Ve chvíli, kdy se hraje o~peníze, má mít nad tím stát kontrolu a~má mít i~peníze z~výher, \ldots}

Neuralgickým bodem většiny sporů je, zda se v~tom či onom případě jedná o~činnost, kterou je možno klasifikovat jako hazardní hru. Klasickým příkladem je například spor mezi Asociací českého pokeru a~Finanční správou o~to, jaká je role znalostního prvku a~jaká je role náhody. Tento spor posuzoval svého času jak Nejvyšší správní soud, tak dokonce Ústavní soud. Nedej bože, až dojde na řešení problému náhody a~psychologie, to budou teprve zajímavé spory všeho druhu. 

Poznamenejme nicméně, že jakkoliv byla, je, a~bude, hra o~peníze mnoha lidmi odsuzována, v~některých případech, například v~tak zvaném desetníkovém mariáši či tarokách, zkušenost ukazuje, že přítomnost i~jenom drobných peněz \textit{drží hráče při zemi,} a~zvyšuje jak kvalitu, tak zážitek ze hry. Hlavním důvodem je fakt, že se hráči mimo jiné vyhýbají beztrestnému hraní nesmyslů, a~více se snaží. Dobře zná každý, kdo hraje karty se svými dětmi nebo vnoučaty. \cite{8,9}

\section*{Pár otázek a~úkolů pro čtenáře na závěr}

\begin{enumerate}
\itemsep=-2pt
\item Náhoda také v~té či oné míře vystupuje v~řadě výkonnostních či spotřebitelských soutěží. Jak je to s~náhodou v~těchto případech?
\item Jaký je z~vašeho pohledu rozdíl mezi hazardní hrou a~soutěží? Lze je od sebe jasně oddělit? 
\item Je možné pomocí statistických postupů určit, jaký podíl v~té či které hře hraje náhoda a~jaký podíl má zkušenost a~znalosti hráčů? Magickým číslem bývá 50\,\% podíl ať již v~té či oné složky. 
\item Patří tak zvané \textit{kvízomaty }mezi vědomostní soutěže? \cite{3,4}
\item Jak pravděpodobnostně kvantifikovat usnesení Nejvyššího správního soudu ČR: \textit{…je postačující, že náhoda je ve hře přítomna, a~to v~míře nezanedbatelné?} (viz judikát Nejvyššího správního soudu 9 Afs 150/ 2013 – 79)
\cite{5}
\item Jak široce je třeba uplatňovat náhodný princip, aby bylo možné hovořit o~hazardní hře? (viz zákon č. 186/2016 Sb. § 3(1))  \cite{2}
\item Je třeba, aby se náhodný princip uplatnil až ve fázi určení výherce, nebo stačí, aby se uplatnil kdykoliv během dané hry? Atd.
\end{enumerate}
Čtenáře vyzýváme, aby se svými názory neváhali seznámit čtenáře našeho bulletinu.

%\section*{Vybraná literatura}
\def\refname{Vybraná literatura}

\begin{thebibliography}{19}
\itemsep=-1.5pt
\bibitem{1} Zákon České národní rady č. 202/1990 Sb. O~loteriích a~jiných podobných hrách, poslední stav textu k~1.\,1.\,2014.

\bibitem{2} Zákon č. 186/2016 Sb. Zákon o~hazardních hrách. Účinnost od 1.\,1.\,2017. 

\bibitem{3} MF ČR. Č.j.: MF-15845/2016/3403-5. Stanovisko -- nový herní software kvízomatů ze dne 31.\,8.\,2016.

\bibitem{4} Rozšířené stanovisko Ministerstva financí k~tzv. „Kvízomatům“ ze dne 24.\,7.\,2014.

\bibitem{5} Usnesení Ústavního soudu České republiky ÚS 3197/14 II.ÚS 3197/14 ze dne 19.\,11.\,2014.

\bibitem{6} Tomáš Zídek o~změnách, které přinese nový loterijní zákon. \href{https://www.mfcr.cz/cs/aktualne/v-mediich/2012/2012-02-29-vmediich-4006-4006}{\tt  https://\\ \tt www.mfcr.cz/cs/aktualne/v-mediich/2012/2012-02-29-vmediich-4006-4006}.

\bibitem{7} Všechny karetní hry budou nově označeny za hazardní. \href{https://zpravy.aktualne.cz/ekonomika/ceska-ekonomika/vsechny-karetni-hry-budou-nove-oznaceny-za-hazardni/r~i:article:656107/}{\tt https://\\\tt zpravy.aktualne.cz/ekonomika/ceska-ekonomika/vsechny-karetni-hry-budou-nove-oznaceny-za-hazardni/r%
\textapprox
i:article:656107/}.

\bibitem{8} Karel Poláček. \textit{Mariáš a~jiné živnosti.} Praha 1924.

\bibitem{9} Karel Poláček. \textit{Hráči.} Praha, 1931.
\url{https://web2.mlp.cz/koweb/00/04/25/29/23/hraci.pdf}

\bibitem{10} Alfréd Rényi. \textit{Teorie pravděpodobnosti.} Academia, Praha, 1972.

\bibitem{11} Karek Zvára, Josef Štěpán. \textit{Pravděpodobnost a~matematická statistika,} vydání šesté. MatfyzPress, Praha, 2019.

\bibitem{12} Jiří Likeš, Josef Machek. \textit{Počet pravděpodobnosti.} SNTL, Praha, 1981.

\bibitem{13} Valerij Nikolajevič Tutubalin. \textit{Teorie pravděpodobnosti.} SNTL, Praha, 1978.

\bibitem{14} Josef Štěpán. \textit{Pravděpodobnost a~statistika na střední škole, Základy pravděpodobnosti.}

\bibitem{15} Jiří Anděl. \textit{Matematika náhody,} vydání čtvrté. MatfyzPress, Praha, 2020.

\bibitem{16} Jiří Anděl. \textit{Statistické úlohy, historky a~paradoxy.} MatfyzPress, Praha, 2018.

\bibitem{17} Václav Dupač, Jaroslav Hájek. \textit{Pravděpodobnost ve vědě a~technice.} NČSAV, Praha, 1962.

\end{thebibliography}


\section*{Poděkování}

Autor by rád poděkoval všem, kteří mu ať již jakýmkoliv způsobem sdělili své připomínky a~námitky, ať již souhlasné či nikoliv. Některé se snažil zapracovat, některé po úvaze nepoužil. Jedno jméno ale musím zmínit, a~to jméno profesora Lva Klebanova. A~to přesto, že naše mnohahodinové diskuse zpravidla začínaly výrokem \textit{\textcyrillic{Уважаемый Яромир! Нету понимания случайности}}!, a~většinou končily výrokem: \textcyrillic{Яромир, я с вами не} \textcyrillic{согласен}! Ale ne nutně vždy\footnote{\textcyrillic{Я забыл вчера сказать Вам, что стоило бы упомянуть о \uv{положительной роли} случайности. Например, в теории игр для нахождения оптимального поведения следует использовать смешанные (т.е. случайные) стратегии. Здесь введение случайности позволяет добиваться хорошего результата. Другой (несколько спорный) пример -- последовательный анализ как он был предожен Вальдом. Там момент прекращения испытаний (число выстрелов) было случайным и зависело от предыдущих (случайных) результатов. Спорность в том, что снижение среднего числа испытаний связано с~увеличением дисперсии. Видимо, есть и другие положительные примеры рандомизации (введения случайности). }\par \textcyrillic{С наилучшими пожеланиями, Лев.}}. Byly to diskuse nejenom velmi zajímavé a~podnětné, ale také velmi důležité. Jiná věc je, že autor většinou po dlouhém boji odpadl a~pouze litoval, že je bohužel neumí hezky česky, a~hlavně pořádně, zpracovat ani pro sebe, natož pro čtenáře bulletinu. 


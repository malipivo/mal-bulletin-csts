% !TEX TS-program = LuaLaTeX
% !TEX encoding = UTF-8 Unicode 
% !TEX root = ../../mal-core.tex

\gdef\mujnazevCS{Náhoda a~doufání}
\gdef\mujnazevEN{}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Jiří Dvořák}

\bonushyper

\nazev{\mujnazevCS}

%\nazev{\mujnazevEN}

\author{\mujauthor}

%\Adresa{}

\Email{dvorak@karlin.mff.cuni.cz }

\smallskip

\noindent
Pojem náhody pro mne znamená nedostatek informací. Aparát stochastiky se uplatní v~případě, kdy chceme popsat jevy, o~kterých víme málo nebo dokonce nic. Kdybychom věděli (uměli změřit, dostatečně rychle vypočítat, …) více, dovedli bychom v~řadě situací daný jev popsat přesně. Alespoň teoreticky. \ Konkrétně si představuji hody kostkou, pohyb kuličky na ruletě, míchání míčků při losování Sportky a~podobně. Pojem náhody tedy vnímám jako subjektivní – pro jednoho pozorovatele může být daný jev náhodný, pro jiného ne. Opomíjím teď problémy kvantového světa, kde možná existuje „skutečná“, objektivní náhoda, kterou nejde obejít či omezit získáním dalších informací.

V~kontextu hazardních her pak pojem „náhoda“ překládám jako „nepředvídatelnost“ v~tom smyslu, že znalost dosavadních výsledků neumožňuje odhadnout budoucí výsledky. To je vlastně nezávislost jednotlivých výsledků na posloupnosti předchozích výsledků – podmíněné pravděpodobnosti možných výsledků v~příštím tahu jsou stejné jako nepodmíněné pravděpodobnosti.

Při znalosti rychlosti pohybu rulety a~kuličky a~velikosti a~směru všech působících sil bychom dovedli vypočítat (predikovat), na jakém políčku se kulička zastaví. Pro náhodu by už nezbylo místo. V~praxi samozřejmě tyto informace nemáme. Stochastika nám pak dává do ruky model, který můžeme použít pro naše rozhodování, například zda máme sázet dál nebo jít domů. Takovým modelem pro ruletu může být „všechny hodnoty mají stejnou šanci padnout, a~následující výsledek je nezávislý na všech předchozích výsledcích“. Tento model bude užitečný pouze v~případě, že ruleta je „poctivá“ v~tom smyslu, že její chování odpovídá našemu modelu. Pro jiné hry mohou být samozřejmě relevantní jiné modely, například takové, kde nejsou pravděpodobnosti všech výsledků stejné.

Na poctivost hazardních her v~České republice dohlíží stát prostřednictvím Ministerstva financí. Stát dovolí provozovat hazardní hru jen tomu, kdo (mimo jiné) prokáže, že

\begin{enumerate}
\itemsep=-3pt
\item o~výsledku hry „zcela nebo z~části rozhoduje náhoda nebo neznámá okolnost“,
\item hra je „poctivá“ v~tom smyslu, že dodržuje pravidla, která provozovatel inzeruje směrem k~hráčům.
\end{enumerate}

\enlargethispage{\baselineskip}%
Splnění těchto požadavků v~případě konkrétního provozovatele a~konkrétního technického zařízení posuzují firmy či laboratoře, které Ministerstvo financí za tímto účelem akreditovalo. A~v~tuto chvíli vstupuje do hry několik úrovní doufání, či chcete-li „slepé důvěry“. Hráč přicházející do herny či kasina doufá, že hra je poctivá, a~důvěřuje státu, že to umí ohlídat. Stát doufá, že akreditovaná laboratoř správně posoudila, zda je daná hra u~konkrétního provozovatele a~na konkrétním zařízení poctivá, a~důvěřuje tomu, že laboratoř dodržuje metodiku, kterou jí ministerstvo schválilo. Akreditovaná laboratoř věří tomu, že dokumentace/zařízení/softwarová implementace, které provozovatel dodal, přesně odpovídají tomu, jak je hra v~reálu provozována. Všichni sborově pak tiše a~ochotně doufají, že když při testování v~akreditované laboratoři vlastnosti hry odpovídaly modelu (a~hra byla vyhodnocena jako náhodná a~poctivá), budou vlastnosti hry odpovídat modelu i~při běžném provozu. 
Pečlivým a důkladným posouzením v~kvalitní akreditované laboratoři lze zvýšit míru jistoty na úkor tohoto doufání, ale úplné jistoty, že je všechno v~pořádku, nejde dosáhnout.
%Pečlivým a~důkladným posouzením v~kvalitní akreditované laboratoři lze prostor pro takové doufání zmenšit, ale nejde úplně odstranit.

Pouze z~posloupnosti výsledků hry nejde obecně rozhodnout, zda o~výsledku rozhoduje náhoda. Když budeme mít fyzicky v~ruce dané zařízení, například rozebereme mechanickou ruletu na prvočinitele, můžeme ověřit, že do procesu tvorby výsledku nejde z~vnějšku zasáhnout, a~pak budeme věřit, že výsledek je opravdu náhodný. Naopak, když budeme hrát online ruletu, kde o~výsledku rozhoduje generátor pseudonáhodných čísel, nedovedeme rozlišit, zda posloupnost výsledků vznikla (pseudo)náhodou, nebo zda je v~programu natvrdo zadána posloupnost výsledků, které má po každém spuštění program vydat. S~vtipem sobě vlastním na tuto skutečnost upozorňoval už před lety například Scott Adams (\url{https://dilbert.com/strip/2001-10-25}). 

\noindent
\hfil
\includegraphics[width=0.68\textwidth]{dt011025.png}%

\enlargethispage{\baselineskip}%
Proto je důležité, aby akreditovaná laboratoř vedle statistických testů na posloupnostech výsledků hry důkladně kontrolovala také softwarovou či hardwarovou implementaci, aby tak ověřila, že do procesu tvorby výsledku hry nejde zasáhnout. V~případě pseudonáhodných generátorů je také potřeba kontrolovat, zda jeho výstupy jsou skutečně použity pro tvorbu výsledku hry a~nejsou bez užitku zahazovány (ano, hodnotitel musí být zdravě nedůvěřivý, možná dokonce nezdravě nedůvěřivý). K~tomu všemu musí akreditovaná laboratoř kontrolovat řadu dalších věcí, například zda jsou finanční data řádně a~bezpečně uchovávána, zda se všechny čítače po dohrání hry správně aktualizují, a~podobně.

Samotné statistické testy prováděné laboratoří na posloupnostech výsledků hry obvykle testují četnosti jednotlivých výstupů (typicky chí-kvadrát testem dobré shody) a~nezávislost mezi jednotlivými výsledky (například pomocí runs testů nebo gap testů). Nulovou hypotézou je zde tvrzení, že „jednotlivé výsledky hry jsou nezávislé a~stejně rozdělené, a~pravděpodobnosti jednotlivých výsledků jsou takové a~takové“. Různé typy použitých testů jsou pak citlivé na různé druhy porušení této nulové hypotézy.

Toto testování můžeme přirovnat k~dětské hře s~vkládačkou, kde různě tvarované předměty pasují do různě tvarovaných otvorů. Nulová hypotéza (model) určuje tvar otvoru, například kruh, a~my zkoumáme, zda do něj daný generátor náhodných čísel (hra) pasuje. Přitom se na něj díváme z~různých stran (používáme různé statistické testy) a~zkoumáme, zda se v~daném směru do otvoru vejde (test nezamítá) nebo nevejde (test zamítá). Pokud žádný z~testů nezamítá, předmět pasuje ve všech směrech a~otvorem určitě projde. Pokud některý test zamítá, předmět je v~daném směru větší, než je průměr otvoru. I~tak může předmět otvorem projít, pokud směrů, ve kterých je moc velký, není příliš mnoho. Například protáhlý elipsoid může uvažovaným kruhovým otvorem projít, když ho správně natočíme. To je podstata korekcí na mnohonásobné porovnávání – provádíme mnoho testů současně, a~pokud některé z~nich zamítnou nulovou hypotézu (předmět je v~daném směru moc velký), nemusí to ještě být důvod celkově hypotézu zamítnout (předmět stejně otvorem projde). Je proto důležité, aby akreditovaná laboratoř nějakou formu korekce na mnohonásobné porovnávání uplatňovala, jinak bude (neoprávněně) zamítat i~kvalitní generátory, čistě proto, že i~dokonalý generátor může náhodou vyrobit posloupnost výsledků, která hypotéze moc dobře neodpovídá.

Problém ověřování, zda je daná hra „náhodná“ a~„poctivá“, má mnoho vrstev a~dává prostor akreditovaným laboratořím projevit své know-how a~kreativitu. Soukromě si dovolím povzdechnout, že ne vždy je to ku prospěchu věci. V~metodikách, které laboratoře předkládají Ministerstvu financí za účelem udělení akreditace, se pak někdy objevují nesmyslné či vyloženě chybné postupy – „testy“, které nekontrolují pravděpodobnost chyby 1. druhu, metody korekce na mnohonásobné porovnávání, jejichž předpoklady nejsou splněny, a~podobně.

%\enlargethispage{\baselineskip}%
Ve výsledku tedy všechny zúčastněné strany (hráč, provozovatel hry, akreditovaná laboratoř, stát) doufají, že všichni ostatní dělají svou práci poctivě tak, jak deklarují. Doufají, že do procesu tvorby výsledku hry nebude nikdo zasahovat. Doufají, že zákon velkých čísel bude fungovat a~provozovatel hry se dobere zisku. Doufají, že provozovatel hry bude poctivě vyplácet výhry a~platit daně. Doufají, že generátor náhodných čísel, který úspěšně prošel posuzováním, bude vykazovat stejné vlastnosti i~v~běžném provozu. Nám nezbývá než doufat, že se nepletou a~opravdu to tak bude…



local math=require 'sci.math'
kama=io.open("vysl3a.txt","w")
kamb=io.open("vysl3b.txt","w")

N=201
--aa=0; b=0; w=0; d=0; e=0; a1=0; b1=0; w1=0; d1=0; e1=0
--f=0; T=0
-- a11=0;b11=0;w11=0;d11=0;e11=0;S=0;
cc={2,3,4,5,7}
--cc(1)=2;cc(2)=3;cc(3)=4;cc(4)=5;cc(5)=7;
step=0.1

for i=1,N
do
  x=(i-1)*step
  io.write(x)
  kama:write(x)
  kamb:write(x)
  
for j=1,5
do
    p=cc[j]
    q=p/2
    --print("\n"..p.." "..q)
    --om=p*(p+q)^2/(q^3*(p+q+1))

    zz=x^(p-1)/(1+x)^(p+q)
    f=zz/math.beta(p,q)
    T=(q*x-p)/(x+1)
    S=(q/p)*T
    io.write(" "..f.." "..S)
    kama:write(" "..f)
    kamb:write(" "..S)
end
    io.write("\n")
    kama:write("\n")
    kamb:write("\n")
    
--[[
if j==1 then
    aa=f, a1=T
elseif j==2
    b=f, b1=T
elseif j==3
    w=f, w1=T
elseif j==4  
    d=f, d1=T
else 
    e=f, e1=T
end
--]]
end

kama:close()
kamb:close()

--plot(s,aa,'-',s,b,'-',s,w,'-',s,d,'-',s,e,'.')
--pause
--plot(s,a1,'-',s,b1,'-',s,w1,'-',s,d1,'-',s,e1,'-')
--pause
--plot(s,a11,'-',s,b11,'-',s,w11,'-',s,d11,'-',s,e11,'-')

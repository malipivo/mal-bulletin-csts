
%$\alpha-$\textit{bit}
%nebo
%$\alpha$-\textit{bit}
%\newpage

\gdef\mujnazevCS{Zobecněná entropie (Rényiho $\alpha$-entropie a~další)}
\gdef\mujnazevEN{Entropy (Rényi's and others)}
\gdef\mujnazevPR{\mujnazevCS}
\gdef\mujnazevDR{\mujnazevEN}
\gdef\mujauthor{Zdeněk Půlpán}

\bonushyper
\def\textgreek{}
\def\dd{\mathrm{d}}
\newdimen\malwidth \malwidth=75mm

\nazev{\mujnazevCS}

\nazev{\mujnazevEN}

\author{\mujauthor}

\Adresa{Univerzita Pardubice, Dopravní fakulta Jana Pernera, Studentská~95, 532\,10 Pardubice 2; Na Brně 1952, 500\,09 Hradec Králové 9}

\Email{zdenek.pulpan@upce.cz, zdenek.pulpan@post.cz}

\Abstrakt{Entropie je jedna z~charakteristik náhodné veličiny. V~práci jsou ukázána některá zobecnění původní Shannonovy entropie jako je Rényiho nebo Tsallisova entropie. Naznačeny jsou další možnosti zobecnění.}
\smallskip
\KlicovaSlova{entropie, Shannonova entropie, Rényiho entropie, Tsallisova entropie, lineární entropie}

\Abstract{Entropy is one of the characteristics of a~random variable. Some generalized original Shannon entropies such as Rényi's or Tsalli's entropy are shown in the work. Other possibilities of generalization are indicated too.}
\smallskip
\KeyWords{entropy, Shannon entropy, Rényi entropy, Tsallis entropy, linear entropy}




\section{Úvod}

Když v~roce 1928 definoval Hartley svůj vzorec pro určení kvantity neurčitosti v~popisu přenosu zpráv, možná netušil, jak významná byla jeho myšlenka použití logaritmu na počet všech očekávaných možných situací. Shannonovou zásluhou o~20 let později pak bylo zavedení míry neurčitosti (později nazvané entropie) a~velikosti informace (informace jako rozdíl entropie před a~po přijetí zprávy) rozšířením Hartleyovy myšlenky na rozdělení pravděpodobnosti navzájem se vylučujících očekávaných variant zprávy. Studium neurčitosti při přenosu zpráv vedlo k~objevu řady pozoruhodných vztahů mezi naším chápáním neurčitosti a~jejím zachycením pomocí pravděpodobnosti a~pak entropie. Tak se vytvořila nová charakteristika, odvozená z~pravděpodobnostního rozdělení (na množině všech variant zprávy) a~pojem entropie se stal součástí teorie pravděpodobnosti. 

Známý maďarský matematik Alfréd Rényi (1921--1970) stačil za svůj krátký život Shannonovu myšlenku dále tak zobecnit, že předchozí možnosti odhadu neurčitosti z~entropie se staly za určitých podmínek pouze speciálními případy Rényiho zobecnění Shannonovy entropie. Rényiho parametrizované zobecnění má (vhodnou volbou hodnoty parametru) větší naději pro uplatnění v~různých experimentálních situacích (tedy již ne jenom při přenosu a~kódování informace) než poněkud užší pojetí Shannonovo. Základním krokem aplikace v~konkrétní experimentální situaci je volba určité náhodné veličiny (mající vztah k~experimentu) a~její, pro interpretaci nejvhodnější, míry neurčitosti. Měr neurčitosti se ve dvacátém století objevilo velké množství, my se zde zmíníme o~těch z~našeho hlediska nejzajímavějších \cite{1,2,4,5,6,7,8,12,13,14,15}.




\section{Rényiho zobecněná entropie}

Uvažujme pravděpodobnostní schéma s~diskrétní náhodnou veličinou $X$ a~její pravděpodobnostní funkci
$$
P_n=\left(\begin{matrix}x_1,\dots,x_n\\p_1,\dots,p_n\end{matrix}\right),\quad \sum _1^np_i=1,\quad 0\leq p_i\leq n,
$$
v~pravděpodobnostním prostoru $\Omega _n,A_n,P_n,$ kde $\Omega _n=\left\{x_1,x_2,\dots,x_n\right\}$, \mbox{$n\geq2$}, je základní prostor, $A_n$ algebra všech jeho podmnožin a~$P_n$ pravděpodobnost na $A_n$.
\bigskip

\textbf{Definice 1. }Zobecněnou Rényiho entropií řádu $\alpha $, kde $\alpha \geq0,\ \alpha \neq1,$ pro diskrétní náhodnou veličinu $X$ s~pravděpodobnostní funkcí $P_n\left(X=x_i\right)=p_i,\ i=1,2,\dots ,n$, nazýváme výraz 
\begin{equation}
H_{\alpha }\left(X\right)=\frac 1{1-\alpha }\log _z\bigg(\sum _1^np_i^{\alpha }\bigg), \textrm{ o~základu logaritmu } z\geq2,
\end{equation}
%, (1)
který je nejčastěji volený $z=2,$ resp. $z=10,$ resp. $z=e$. Podle toho se odlišují jednotky, jak ukážeme.

Dále budeme diskrétní náhodnou veličinu $X$ (dále jen n.\,v.\,$X$) označovat také pomocí jejího pravděpodobnostního rozdělení takto:
$$
X=\left(p_1,p_2,\dots,p_n\right).
$$

Pro každé přípustné $\alpha $ představuje $H_{\alpha }\left(X\right)$ určitý „metr“ měření entropie (pro odhad míry neurčitosti) diskrétní n.\,v.\,$X$. Entropie souvisí s~informací n.\,v.\,$X$ tak, že informace je rozdíl veličiny $H_{\alpha }\left(X\right)$ s~rozdělením před sledovanou událostí a~po ní. Základ logaritmu je v~podstatě libovolný (určuje ale také „metr“, souvisí tedy s~interpretací veličiny $H_{\alpha }\left(X\right)$), často se užívá dvojkový logaritmus ($z=2$) protože má nejjednodušší interpretaci. Jednotku entropie při $z=2$ bychom měli označovat $\alpha$-\textit{bit}.

\textit{Poznámka: }\\
Místo $H\left(X\right)=H(\left(p_1,p_2,\dots,p_n\right))$ píšeme dále jen $H\left(p_1,p_2,\dots,p_n\right)$.

\textit{Speciální případy:}

1) Je-li $P_n\left(X=x_i\right)=\frac 1 n$, $i=1,2,\dots,n$, pak
\begin{equation*}
H_{\alpha }\left(X\right)=\frac 1{1-\alpha }\log _z(n\cdot(n^{-\alpha }))=\log _zn=H_0(X)
\end{equation*}
nezávisle na volbě $\alpha $. Výsledná Rényiho entropie je zde Hartleyovou mírou. Volíme-li ve výrazu pro $H_{\alpha }\left(X\right)$ parametr $\alpha =0$, dostaneme tutéž hodnotu $\log _zn$. Proto označujeme v~tomto případě Rényiho entropii $H_0(X)$.

2) V~definici Rényiho zobecněné entropie je vyloučena hodnota $\alpha =1.$ Limitním přechodem pro $\alpha \rightarrow 1$ však dostaneme Shannonovu entropii:
\begin{align*}
\lim _{\alpha \rightarrow 1}H_{\alpha }\left(X\right)=&\lim _{\alpha \rightarrow 1}\frac{\log _2\sum _1^np_i^{\alpha }}{1-\alpha }=\lim _{\alpha \rightarrow 1}\frac 1{\ln 2}\frac{\ln \sum _1^np_i^{\alpha }}{1-\alpha }=\\
=&\lim _{\alpha \rightarrow 1}\frac{-1}{\ln 2} \cdot \frac 1{\sum _1^np_i^{\alpha }} \cdot \sum _1^np_i^{\alpha }\ln p_i=-\sum _1^np_i\log _2p_i.
\end{align*}


Definujeme zde $0\cdot\log 0$ limitou $\lim _{p\rightarrow 0^{+}} p\cdot\log p$. Hodnotu $H_{\alpha }\left(X\right)$ lze tedy dodefinovat i~pro $\alpha =1$ Shannonovou entropií. Proto v~tomto případě se značí 
\begin{equation*}
H_1\left(X\right)=-\sum _1^n p_i\log _2 p_i\quad [\mathit{bit}].
\end{equation*}
Pro základ logaritmu 10, resp. $e$ se jednotka nazývá $\mathit{dit}$, resp. $\mathit{nit}$.

3) Volíme-li $\alpha =2$, dostaneme tzv. kolizní entropii ve tvaru
$$
H_2(X)=-\log _2\sum _1^np_i^2\quad \textrm{[2-\textit{bit}]}.
$$

4) Přechodem k~limitě $\alpha \rightarrow \infty$, dostaneme Rényiho entropii postupně ve tvaru
\begin{align*}
H_{\infty}\left(X\right)&=\lim _{\alpha \rightarrow \infty}H_{\alpha }\left(X\right)=\min_i(-\log _2p_i)=\\
&=-\max_i\log _2p_i=\log _2\max_i p_i\quad \textrm{[$\infty$-\textit{bit}]}.
\end{align*}
Z~důvodů, které dále ukážeme, se tato entropie nazývá minimální.

Ještě se někdy uvažuje o~limitě $\alpha \rightarrow -\infty$ (vzhledem k~limitě předchozí) a~pak se definuje
$$
H_{-\infty}\left(X\right)=\lim _{\alpha \rightarrow -\infty} H_{\alpha }\left(X\right)=-\log _2\min_i p_i
\quad \textrm{[$-\infty$-\textit{bit}]}.
$$
Tato entropie se nazývá maximální.%
\bigskip



Nyní se podíváme na závislost hodnot entropie $H_{\alpha }(X)$ na $\alpha $. Platí následující věta.

\bigskip


\textbf{Věta 1. }Rényiho entropie $H_{\alpha }(X)$ je nerostoucí funkcí v~proměnné $\alpha $, $\alpha >0,\ \alpha \neq1$.

\textit{Důkaz}: Spočteme derivaci $H_{\alpha }(X)$ podle $\alpha $:
\begin{align*}
 \frac{\dd H_{\alpha }(X)}{\dd\alpha }&=\frac 1{(1-\alpha)^2}
 \cdot \log _2\sum _1^np_i^{\alpha }+\frac 1{1-\alpha } \sum _1^n\frac{p_i^{\alpha }}{\sum _jp_j^{\alpha }}\log _2p_i=\\
 &=\frac 1{(1-\alpha)^2} \left\{\log _2\sum _1^np_i^{\alpha }+\sum _1^np_i^{\alpha }\frac 1{\sum _jp_j^{\alpha }}\log _2p_i- \right.\\
 &\quad-\left.\sum _1^np_i^{\alpha }\frac 1{\sum _jp_j^{\alpha }}\log _2p_i^{\alpha }\right\}=
 \frac 1{(1-\alpha)^2} \left\{1\cdot \log _2\sum _1^np_i^{\alpha }+
 \right.\\
 &\quad+\left.
 \sum _1^n\frac{p_i^{\alpha }}{\sum _jp_j^{\alpha }}
 \cdot\log_2\frac{p_i}{p_i^{\alpha}}
 \right\}.
\end{align*}

Nyní použijeme vztah
\begin{equation*}
1=\frac{\sum _1^np_i^{\alpha }}{\sum _1^np_j^{\alpha }}=\sum _1^np_i^{\alpha }\frac 1{\sum _{j=1}^np_j^{\alpha }}.
\end{equation*}
A~dosadíme do posledního výrazu pro derivaci a~dostaneme:
\begin{align*}
\frac{\dd H_{\alpha }(X)}{\dd\alpha }&=
\frac 1{(1-\alpha)^2}\left\{\sum _i\frac{p_i^{\alpha }}{\sum _jp_j^{\alpha }}\log _2\sum _ip_i^{\alpha }+\sum _i\frac{p_i^{\alpha }}{\sum _jp_j^{\alpha }}\log _2\frac{p_i}{p_i^{\alpha }}\right\}=\\
 &=\frac 1{(1-\alpha)^2} \left\{\sum _i\frac{p_i^{\alpha }}{\sum _jp_j^{\alpha }}\log _2\frac{p_i}{p_i^{\alpha }}\sum _ip_i^{\alpha }\right\}=\\
 &=-\frac 1{(1-\alpha)^2}\cdot \sum _i\frac{p_i^{\alpha }}{\sum _jp_j^{\alpha }}\cdot \log _2\frac{p_i^{\alpha }}{p_i\sum _jp_j^{\alpha }}=\frac{-1}{(1-\alpha)^2} \sum _iz_i\log _2\frac{z_i}{p_i},
\end{align*}
kde jsme položili $z_i=\frac{p_i^{\alpha }}{\sum _jp_j^{\alpha }}$. Derivace $\frac{\dd H_{\alpha }(X)}{\dd\alpha }$ je záporná, protože představuje $\frac{-1}{(1-\alpha)^2}$ násobek Kullback-Leiblerovy divergence, která je vždy nezáporná \cite{4}.

\textit{Poznámka}: Po dodefinování $H_1$ i~pro $\alpha =1$, je $H_{\alpha }(X)$ nerostoucí spojitou funkcí $\alpha $.

\textit{Důsledek}: Pro jednotlivé entropie $H_0(X),\ H_1(X),\ H_2(X),\ H_{{\infty}}(X)$ platí nerovnosti

$$H_0(X)\geq H_1(X)\geq H_2(X)\geq H_{\infty}(X).$$

\textit{Poznámka}: Uvedené nerovnosti vyplývají také z~následujících nerovností:

\begin{gather*}
H_1(X)\geq H_2(X) \textrm{ protože platí } \sum _ip_i\log _2p_i \leq\log _2\sum _ip_i^2,\\
H_{\infty}(X) \leq H_2(X) \textrm{ protože platí } \log _2\sum _ip_i^2 \leq\log _2\sup_i p_i.
\end{gather*}


\bigskip

\textbf{Věta 2. }Pro entropie $H_2(X)$ a~$H_{\infty}(X)$ platí nerovnost 
$$H_2(X)\leq2\cdot H_{\infty}(X).$$

\textit{Důkaz:} Podle definice je $H_2\left(X\right)=\frac 1{1-2}\log _2\sum _ip_i^2=-\log _2\sum _i p_i^2$.
%
Protože $\log _2\sum _i p_i^2\geq\log _2\max_i p_i^2=2\log _2\max_i p_i$, je $H_2\left(X\right)\leq-2\log _2\max_i p_i=2\cdot H_{\infty}(X)$.

\bigskip

%\noindent
\textbf{Příklad 1a.} Mějme n.\,v.\,$X$ s~rozdělením $P_2\left(X=x_1\right)=p,$ $P_2\left(X=x_2\right)=1-p=q,$ $0\leq p,\ q\leq1$. Pak je 
\begin{align*}
H_0\left(X\right)&=\log _22=1\\
H_1\left(X\right)&=-p\log _2p-(1-p) \log _2(1-p)\\
H_2\left(X\right)&=-\log _2(p^2+q^2)\\
H_{\infty}\left(X\right)&=-\log _2\max(p,q).
\end{align*}

Na Obr.\,1 jsou zakresleny grafy funkcí $H_0\left(X\right)$, $H_1\left(X\right)$, $H_2\left(X\right)$ a~$H_{\infty}(X)$, kde $X=(p,1-p)$, $0\leq p\leq1.$

O~dalších vlastnostech entropie $H_{\alpha }(X)$ se lze dozvědět z~\cite{9} nebo také z~\cite{18}.



%Obr.1
\begin{figure}[!htb]
\centering
\begin{tikzpicture}
\begin{axis} [
width=\malwidth,
legend cell align=left,
%legend pos=south east,
legend style={at={(1.05,1)},anchor=north west},
%%tick num=0.2,
%minor tick num=0.2,
%xtick,ytick,
xmajorgrids, ymajorgrids,
/pgf/number format/.cd, use comma, precision=1, fixed, fixed zerofill,
]
\addplot+[mark=none,black, line width=1.25pt] coordinates{(0,0) (0,1) (1,1) (1,0)};
\addplot+[mark=none,black, dashed, smooth,domain=0:1, samples=200, line width=1.25pt] {-x*log2(x)-(1-x)*log2(1-x)};
\addplot+[mark=none,black, dotted, smooth,domain=0:1, samples=200, line width=1.25pt] {-log2(x*x+(1-x)*(1-x))};
\addplot+[mark=none,black, dashdotted, smooth,domain=0:1, samples=200, line width=1.25pt] {-log2(max(x,1-x))};
\legend{ {$H_0(x,1-x)$}, {$H_1(x,1-x)$}, {$H_2(x,1-x)$}, {$H_{\infty}(x,1-x)$} };
\end{axis}
\end{tikzpicture}
\caption{Grafy hodnot  $H_i\left(p,1-p\right)$ pro $i = 0, 1, 2,\infty$\\ v~závislosti na $p\in\left<0;1\right>$.}
\end{figure}




Sledujeme-li Obr.\,1, na první pohled se zdá, že by mohlo existovat pro n.\,v.\,$X=(p,1-p)$ reálné $\alpha _0>2$ tak, že 
\begin{equation*}
H_{\alpha _0}\left(X\right)=2\cdot\min(p,1-p)
\end{equation*}
by byla po částech lineární. Platí ale následující Věta~3.


\bigskip

\textbf{Věta 3. }Pro žádné reálné $\alpha >2$ nemůže být 
\begin{equation*}
H_{\alpha }\left(p,1-p\right)=\frac 1{1-\alpha }\log _2(p^{\alpha })+\left(1-p^{\alpha }\right)=2\cdot\min\left(p,1-p\right),\quad 0\leq p\leq1.
\end{equation*}

\textit{Důkaz:} Předpokládejme, že takové $\alpha $ existuje pro všechna $p\in \left(0,1\right).$ Pak musí pro tato $p$ platit 
\begin{equation*}
\frac 1{1-\alpha }\log _2(p^{\alpha }+\left(1-p^{\alpha }\right))=2\cdot\min \left(p,1-p\right)=2p,
\end{equation*}
protože vztah je symetrický k~$p$ a~$1-p$. 

Ekvivalentními úpravami tohoto výrazu dostaneme mezi $p$ a~$\alpha $ vztah 
$$2^{2p(1-\alpha )}=p^{\alpha }+(1-p^{\alpha }),$$
který má být platný pro jediné $\alpha $ a~všech možných volbách $p\in (0;0{,}5)$. Pro $p=0;0{,}5;1$ vztah zřejmě platí pro jakékoliv $\alpha $. Volme proto nejprve $p=0{,}25$ a~pak $p=0{,}125$ a~dosaďme do předchozí rovnice. 

Pro $p=0{,}25$ dostaneme podmínku pro $\alpha $ ve tvaru 
\begin{equation*}
2^{1{,}5\alpha +0{,}5}=1+3^{\alpha }
\end{equation*}
a~pro $p=0{,}125$ pak 
$$2^{2{,}75\alpha +0{,}25}=1+7^{\alpha }.$$

Podle předpokladu oba vztahy pro určení $\alpha $ musí mít stejné řešení pro $\alpha >2$. Ukážeme, že nemohou mít stejná řešení.

Snadno zjistíme numerickými prostředky, že první rovnice má jediný kořen pro $\alpha >2$ v~intervalu (5; 6) a~druhá v~intervalu (3; 4). Společný kořen $\alpha >2$ tedy neexistuje. Proto také pro žádné reálné $\alpha >2$ nemůže nastat rovnost z~Věty~3.




\section{Další entropie}

Připomeňme na začátek ještě jedno velmi obecné zavedení entropie tzv. Fadě-jevovými axiomy z~roku 1956. Fadějev \cite{17} určuje entropii $H\left(p_1,p_2,\dots,p_n\right)$, $\sum _i p_i=1$, $0\leq p_i\leq1$ pro všechna $i$ až na multiplikativní konstantu jednoznačně následujícími podmínkami:

\begin{itemize}
\itemsep=-1pt
\item[1.] $H\left(p,1-p\right)$ je spojitá funkce pro $p\in(0;1)$ a~kladná v~aspoň jednom bodě;
\item[2.] $H\left(p_1,p_2,\dots,p_n\right)$ je symetrická funkce svých proměnných;
\item[3.] $H(p_1,p_2,\dots,p_{n-1},q_1,q_2)=H\left(p_1,p_2,\dots,p_n\right)+p_n\cdot H\big(\frac{q_1}{p_n},\frac{q_2}{p_n}\big)$, kde je $p_n=q_1+q_2>0$. \cite{12} 
\end{itemize}

Nyní se vrátíme k~Obr.\,1 a~uvědomíme si, že žádná Rényiho entropie $H_{\alpha }\left(X\right)$ nemůže být pro dichotomický rozhodovací proces po částech lineární ve tvaru, viz také Obr.\,2.

%\begin{equation*}
%H\left(p,1-p\right)=2\cdot \min(p,1-p);\quad 0\leq p\leq1.
%\end{equation*}

%TODO
%{\centering
%Obr. 2.
%\par}
%{\centering
%{\centering [Warning: Draw object ignored][Warning: Draw object ignored]\par}
%{\centering
%[Warning: Draw object ignored] $p$
%\par}
%0 0,5 1


\begin{figure}[!htb]
\centering
%\scalebox{0.8}{%
\begin{tikzpicture}
\begin{axis} [
width=60mm,
%legend cell align=left,
%legend pos=outer north east,
%%tick num=0.2,
%minor tick num=0.2,
xtick={0,0.5,1},
%xtick,ytick,
%xmajorgrids, ymajorgrids,
xlabel={$p$},
ylabel={$H(p,1-p)$},
/pgf/number format/.cd, use comma, precision=1, fixed, fixed zerofill,
]
\addplot+[mark=none,black, line width=1.25pt] coordinates{(0,0) (0.5,1) (1,0)};
%\addplot+[mark=none,black, dashed, smooth,domain=0:1, samples=200, line width=1.25pt] {-x*log2(x)-(1-x)*log2(1-x)};
%\addplot+[mark=none,black, dotted, smooth,domain=0:1, samples=200, line width=1.25pt] {-log2(x*x+(1-x)*(1-x))};
%\addplot+[mark=none,black, dashdotted, smooth,domain=0:1, samples=200, line width=1.25pt] {-log2(max(x,1-x))};
%\legend{ {$H_0(x,1-x)$}, {$H_1(x,1-x)$}, {$H_2(x,1-x)$}, {$H_{\infty}(x,1-x)$} };
\legend{};
\end{axis}
\end{tikzpicture}%}
%\caption{Grafy hodnot  $H_i\left(p,1-p\right)$ pro $i = 0, 1, 2,\infty$\\ v~závislosti na $p\in\left<0;1\right>$.}
\caption{Graf funkce $H\left(p,1-p\right)=2\cdot\min(p,1-p),\ 0\leq p\leq1$.}
\end{figure}

Pokusme se ale předchozí vztah rozšířit pomocí Fadějevových axiomů na novou entropii pro n.\,v.\,$X$.

Začneme podmínkou 3. Podle ní musí platit pro $n=3$
\begin{align*}
H\left(p_1,p_2,p_3\right)&=H\left(p_1,p_2+p_3\right)+\left(p_2+p_3\right)\cdot H\left(\frac{p_2}{p_2+p_3},\frac{p_3}{p_2+p_3}\right)=\\
&= 2\cdot\min(p_1,p_2+p_3)+(p_2+p_3)\cdot2\cdot
\min
\left(\frac{p_2}{p_2+p_3},\frac{p_3}{p_2+p_3}\right)=\\
&=2\cdot\min(p_1,p_2+p_3)+2\cdot\min(p_2,p_3).
\end{align*}

A~podobně pak bude
\begin{align*}
H\left(p_1,p_2,p_3,p_4\right)
&=H\left(p_1,p_2,p_3+p_4\right)+\left(p_3+p_4\right)H\left(\frac{p_3}{p_3+p_4},\frac{p_4}{p_3+p_4}\right)=\\
&=2\cdot\min\left(p_1,p_2+p_3+p_4\right)+2\cdot\min\left(p_2,p_3+p_4\right)+\\
&\phantom{=}+2\cdot\min(p_3,p_4)\textrm{, atd.}
\end{align*}
%$H\left(p_1,p_2,p_3,p_4\right)=
 
Uvedený postup odpovídá rozhodování podle schématu na Obr.\,3. 

%TODO
%{\centering
%Obr. 3.
%\par}
\begin{figure}[!htb]
\centering
\begin{equation*}
\left(\begin{matrix}x_1&x_2&x_3\\p_1&p_2&p_3\end{matrix}\right)
\longrightarrow
\left(\begin{matrix}x_1&\left\{x_2,x_3\right\}\\p_1&1-p_1\end{matrix}\right)
\longrightarrow
\left(\begin{matrix}x_2&\left\{x_3\right\}\\
\textstyle\frac{p_2}{p_2+p_3}&
\textstyle\frac{p_3}{p_2+p_3}
\end{matrix}\right)
\end{equation*}
\caption{Schéma rozhodování ve dvou krocích,\\týkající se n.\,v.\,$X=(p_1,p_2,p_3)$.}
\end{figure}


Takto definovaná funkce $H\left(p_1,p_2,p_3\right)$ ale není symetrickou funkcí svých pravděpodobností:
\begin{gather*}
H\left(0{,}5;0{,}25;0{,}25\right)=2\cdot\min\left(0{,}5;0{,}5\right)+2\cdot\min\left(0{,}25;0{,}25\right)=1+0{,}5=1{,}5;\\
H\left(0{,}25;0{,}5;0{,}25\right)=2\cdot\min\left(0{,}25;0{,}75\right)+2\cdot\min\left(0{,}5;0{,}25\right)=0{,}5+0{,}5=1.
\end{gather*}
 

\textit{Poznámka:} Někdy je důležité respektovat uspořádání pravděpodobností $p_1,p_2,\dots,p_n$ například s~hlediska logiky příslušného rozhodovacího procesu. V~některých aplikacích by mohlo záležet na tom, v~jaké posloupnosti se jednotlivé varianty v~rozhodovacím procesu identifikují nebo která z~variant je pro interpretaci variantou preferovanou. \cite{10,11}

Ukážeme, že lze předchozí 
vztah pro $H(p_1,p_2,p_3)$ upravit tak, aby jeho 
hodnota nezávisela na permutaci pravděpodobností $(p_1,p_2,p_3)$:
$H(p_1,p_2,p_3)=H(p_{i_1},p_{i_2},p_{i_3})$, kde $(p_{i_1},p_{i_2},p_{i_3})$ je taková permutace pravděpodobností, že
$$p_{i_1}\geq p_{i_2}\geq p_{i_3}.$$


Zavedeme proto tuto obecnou definici entropie $H\left(p_1,p_2,\dots,p_n\right)$:
\bigskip

\textbf{Definice 2. }Pro diskrétní n.\,v.\,$X=(p_1,p_2,\dots,p_n)$, $\sum _ip_i=1$, $0\leq p_i\leq1$ pro všechna $i$, definujeme entropii $H\left(p_1,p_2,\dots,p_n\right)$ takto:
\begin{itemize}
\itemsep=-1pt
\item[a)] $H\left(p_1,p_2\right)=2\cdot \min(p_1,p_2),$
\item[b)] pro $n\geq3$ platí
\end{itemize}
\begin{align}
H\left(p_1,p_2,\dots,p_n\right)&=H\left(p_{i_1},p_{i_2},\dots, p_{i_{n-1}}+p_{i_n}\right)+\\ \notag
&\quad+\left(p_{i_{n-1}}+p_{i_n}\right)\cdot H\left(\frac{p_{i_{n-1}}}{p_{i_{n-1}}+p_{i_n}},\frac{p_{i_n}}{p_{i_{n-1}}+p_{i_n}}\right),
\end{align}
kde $p_{i_1}\geq p_{i_2}\geq \dots \geq p_{i_{n-1}}\geq p_{i_n}\geq 0$ a~$p_{i_{n-1}}+p_{i_n}>0$. 
% !! Tento vztah označíme (2).

Použijeme-li tuto definici, dostaneme pro $H\left(p_1,p_2,\dots ,p_n\right)$ vztah (2) ve tvaru
\begin{align*}
H\left(p_1,p_2,\dots ,p_n\right)&= 2\cdot\min \left(p_{i_1},p_{i_2},\dots ,p_{i_{n-1}}+p_{i_n}\right)+\\
&\quad+\left(p_{i_{n-1}}+p_{i_n}\right)\cdot 2\cdot \min \left(\frac{p_{i_{n-1}}}{p_{i_{n-1}}+p_{i_n}},\frac{p_{i_n}}{p_{i_{n-1}}+p_{i_n}}\right)=\\
&=2\cdot \min \left(p_{i_1},p_{i_2}+\dots +p_{i_{n-1}}+p_{i_n}\right)+2\cdot \min \left(p_{i_2},p_{i_3}+\right.\\
&\quad \left. +\dots + p_{i_{n-1}}+p_{i_n}\right)+\dots +2\cdot \min (p_{i_{n-1}},p_{i_n}),
\end{align*}
kde již můžeme vypustit podmínku $p_{i_{n-1}}+p_{i_n}>0$. 

Definice 2 vyhovuje všem Fadějevovým podmínkám, ale není Shannonovou entropií:
\begin{itemize}
\itemsep=-1pt
\item[1.] Funkce $H\left(p_1,p_2\right)=2\cdot\min (p_1,p_2)$, $p_1+p_2=1,$ je spojitou funkcí svých proměnných a~kladná např. pro $p_1=p_2=0{,}5$;
\item[2.] $H\left(p_1,p_2,\dots ,p_n\right)$ je symetrickou funkcí svých proměnných;
\item[3.] určení $H\left(p_1,p_2,\dots,p_n\right)$ je jednoznačné.
\end{itemize}
\smallskip

Entropie $H\left(p_1,p_2,\dots ,p_n\right)$ má následující vlastnosti:
\begin{itemize}
\itemsep=-1pt
\item[a)] $H\left(0{,}5;0{,}5\right)=2\cdot\min \left(0{,}5;0{,}5\right)=1$. Proto jsme kdysi nazvali tuto jednotku \textit{lit} \cite{3}.
\item[b)] $H\left(1,0,0,\dots ,0\right)=0\ [\mathit{lit}]$,
\item[c)] $H\left(\frac 1 n,\frac 1 n,\dots ,\frac 1 n\right)=2\cdot \frac{n-1} n=2\cdot\left(1-\frac 1 n\right)<2\ [\mathit{lit}]$,
\item[d)] $0\leq H\left(p_1,p_2,\dots, p_n\right)<2\ [\mathit{lit}]$,
\item[e)] $H\left(p_1,p_2,0,0,\dots ,0\right)=H(p_1,p_2)$, $p_1+p_2=1$, $0\leq p_1,p_2 \leq 1$.
\end{itemize}
\bigskip


%\noindent
\textbf{Příklad 1b.} Nechť n.\,v.\,$X$ ($p=0{,}25;1-p=0{,}75)$ pak máme
\begin{align*}
H\left(p,1-p\right)&=H\left(1-p,p\right)=2\cdot\min \left(p,1-p\right)=0{,}5\ [\mathit{lit}]\\
H_0(X)&=\log _22=1\ [\mathit{bit}]\\
H_1(X)&=0{,}811\ [\mathit{bit}]\\
H_2\left(X\right)&=-\log _2\left(0{,}25^2+0{,}75^2\right)=0{,}67807\ \textrm{[2-\textit{bit}]}\\
H_{\infty}(X)&=-\log _2\max\left(0{,}25;0{,}75\right)=0{,}41503\ \textrm{[$\infty$-\textit{bit}]}.
\end{align*}

Nyní se podíváme na další z~entropií, tzv. Tsallisovu entropii. Je zajímavá také tím, že ve své formuli neobsahuje logaritmus. Brazilský fyzik řeckého původu C.\,Tsallis (*1943) zavedl tuto entropii při zobecňování Boltzmann\zz Gibbsovy fyzikální entropie \cite{5}.
\bigskip

\textbf{Definice 3. }Tsallisovou entropií řádu $q$ nazýváme entropii, která se určuje pro diskrétní náhodnou veličinu $X\left(p_1,\dots ,p_n\right)$, $\sum _ip_i=1$, $0\leq p_i \leq 1$, $i=1,2,\dots,n$ ze vzorce
\begin{equation}
T_q(X)=\frac k{q-1}\bigg(1-\sum _1^np_i^q\bigg),\quad \textrm{kde }k>0,\ q>0,\ q\neq1.
\end{equation}

Volbou parametru $k$ se upravuje jednotka této entropie. Jednotka této entropie nemá název, ale pro pořádek ji nazveme $(k,q)$-\textit{tit.}

Vlastnosti $T_q\left(X\right):$

\begin{itemize}
\itemsep=-1pt
\item[1)] Entropie $T_q(X)$ je invariantní vůči kterékoliv permutaci $\left(p_1,\dots ,p_n\right)$.
\item[2)] $\lim _{q\rightarrow 1}T_q(X)=\lim _{q\rightarrow 1}\frac k{q-1}(1-\sum _1^np_i^q)=\lim _{q\rightarrow 1}k\cdot\frac{-\sum _ip_i^q\cdot\ln p_i} 1=k\cdot(-\sum _ip_i\ln p_i)$. Proto se tato entropie značí $T_1\left(X\right)=H_1(X)$.
\item[3)] $T_2\left(X\right)=k\cdot (1-\sum _ip_i^2)$ protože $\sum _ip_i^2\leq1$, je $0\leq T_2(X)\leq k$. V~případě $n=2$ je pak $T_2\left(X\right)=k\cdot2\cdot p\cdot (1-p)$, tedy $0{\leq}T_2\left(X\right)\leq k\cdot 0{,}5$. Proto je někdy vhodné volit $k=2$, aby $0\leq T_2(X)\leq 1.$
\item[4)] $\frac{\dd T_q}{\dd q}=\frac{\dd}{\dd q}\big(\frac k{q-1}(1-\sum _1^n p_i^q)\big)=k\cdot (q-1)^{-2}\cdot(-\sum _ip_i^q\cdot\ln p_i)\leq0$, tedy $T_q(X)$ je nerostoucí funkcí pro všechna přípustná $q$. Vezmeme-li v~úvahu ještě vlastnost 2), platí to i~pro $q>0.$
\item[5)] Pro n.\,v.\,$X\left(1,0,\dots ,0\right)$ je $T_q\left(X\right)=0$ pro všechna přípustná $k$ a~$q$.
\end{itemize}

%TODO
%Obr. 4
\begin{figure}[!htb]
\centering
\begin{tikzpicture}
\begin{axis} [
width=1.1\malwidth,
%legend cell align=left,
%legend pos=outer north east,
%%tick num=0.2,
%minor tick num=0.2,
xtick={0,0.5,1},
%xtick,ytick,
%xmajorgrids, ymajorgrids,
xlabel={$p$},
ylabel={$T_q$},
/pgf/number format/.cd, use comma, precision=1, fixed, fixed zerofill,
]
%\addplot+[mark=none,black, line width=1.25pt] coordinates{(0,0) (0.5,1) (1,0)};
\addplot+[mark=none,black, solid,  smooth,domain=0:1, samples=1200, line width=1.25pt] {-2*(x*log2(x)+(1-x)*log2(1-x))};
\addplot+[mark=none,black, dashed, smooth,domain=0:1, samples=200, line width=1.25pt] {4*x*(1-x)};
\addplot+[mark=none,black, dotted, smooth,domain=0:1, samples=200, line width=1.25pt] {3*x*(1-x)};
%\legend{ {$H_0(x,1-x)$}, {$H_1(x,1-x)$}, {$H_2(x,1-x)$}, {$H_{\infty}(x,1-x)$} };
\legend{ {$T_1$}, {$T_2$}, {$T_3$} };
\end{axis}
\end{tikzpicture}
%\caption{Grafy hodnot  $H_i\left(p,1-p\right)$ pro $i = 0, 1, 2,\infty$\\ v~závislosti na $p\in\left<0;1\right>$.}
%\caption{Graf funkce $H\left(p,1-p\right)=2\cdot\min(p,1-p),\ 0\leq p\leq1$.}
\caption{Graf $T_q\left(X\right)$ pro n.\,v.\,$X(p,1-p)$ a $k=2,$ $q=1,2,3$.\\
$T_1=-2\cdot\left\{p\ln p+(1-p)\ln(1-p)\right\};\ T_2=4\cdot p\cdot(1-p);\ T_3=3\cdot p\cdot(1-p)$.}
\end{figure}



Z~Fadějevových axiomů Tsallisova entropie $T_q\left(X\right)$ splňuje samozřejmě první a~druhý. Obecně pro všechna $q>0,\ q\neq1$ třetí axiom nesplňuje, protože platí
$$
p_n^q=(q_1+q_2)^q \neq q_1^q+q_2^q.
$$

Není tedy pro uvedená $q$ entropií ve Fadějevově smyslu. Entropie $T_1\left(X\right)$ samozřejmě Fadějevovy axiomy splňuje.

Uvažujme ještě jednu možnost odhadu neurčitosti diskrétní n.\,v.\,$X$ (opět bez logaritmu v~definičním vztahu). Je to tzv. lineární entropie $L\left(X\right).$
\bigskip

\textbf{Definice 4. }Lineární entropie pro n.\,v.\,$X\left(p_1,\dots ,p_n\right)$, $\sum _ip_i=1$, $0\leq p_i\leq1$, $i=1,2,\dots,n,$ je určena vztahem

\begin{equation}
L\left(X\right)=k\cdot \sum _1^np_i\left(1-p_i\right),\quad k>0.
\end{equation}

Vlastnosti $L\left(X\right)$:

\begin{itemize}
\itemsep=-1pt
\item[1)] Lin. entropie $L\left(X\right)$ je symetrickou funkcí svých proměnných $p_1,\dots ,p_n$.
\item[2)] Pro lineární entropii $L\left(X\right)$ platí: $L\left(X\right)=k\cdot (\sum _ip_i-\sum _ip_i^2)\geq 0$, rovnost nastává pro $X=(1,0,\dots ,0)$.
\item[3)] Pro $n=2$ je $L\left(X\right)=L\left(p,1-p\right)=k\cdot p\left(1-p\right)=T_2(X)$.
\item[4)] Pro n.\,v.\,$X=(\frac 1 n,\frac 1 n,\dots ,\frac 1 n)$ je $L\left(X\right)=k\cdot\left(1-\frac 1 n\right)<k$.
\item[5)] Platí $H_1\left(X\right)=H_1\left(p_1,\dots ,p_n\right)=-\sum _ip_i\ln p_i\geq L\left(X\right)=\sum _i p_i(1-p_i)$. To vyplývá z~toho, že $p_i-1\geq \ln p_i$ a~pak je $p_i\left(1-p_i\right)\leq-p_i\ln p_i$ pro všechna $i$.
\item[6)] Jednotka této entropie také zavedena není, budeme ji zde označovat $\mathit{lit}^2$.
\end{itemize}

Lineární entropie $L(X)$ odpovídá neurčitosti $n$ nezávislých dichotomických rozhodování:
$$
x_1-\mathit{non\,}x_1; \quad 
x_2-\mathit{non\,}x_2; \quad
x_n-\mathit{non\,}x_n.
$$

\textit{Poznámka:} V~práci \cite{16} je ještě diskutována $a$-entropie $h_a$ zobecněného pravděpodobnostního schématu. Přitom je třetí Fadějevův požadavek zobecněn na tvar
$$
h_a\left(X\right)=h_a\left(tp_1,\left(1-t\right)p_1,p_2,\dots ,p_n\right)=
h_a\left(p_1,\dots ,p_n\right)+p_1^a \cdot h_a(t,1-t).
$$




\section{Aplikace}

Aplikace není snadná. Obtíže již může činit vymezení prvotního (observačního) jevového pole $\Omega ,A$, kde $\Omega $ je idealizovaný pozorovaný prostor, $A$ je \mbox{$\sigma$-algebra} podmnožin z~$\Omega $. Dříve zmíněná množina $\Omega _n$ není počátkem zkoumání, je jen součástí schématu pro popis výsledku realizace n.\,v.\,$X$, která přísluší schématu 
$\mathrm{P}=[\Omega _n,A_n,P_n]$. 
Míra jistoty realizace n.\,v.\,$X$, patřící prostoru~P je entropie. Smyslem aplikace je určení vztahu mezi prvotním jevovým polem~$[\Omega,A]$ a~konečnými množinami $\Omega _n,A_n$. Tento vztah je dán náhodnou veličinou $Y$, která má hodnoty v~$\Omega _n$ a~platí pro pravděpodobnostní prostory $[\Omega ,A,P]$ a~$[\Omega _n,A_n,P_n]$, že
$$P\left(\left\{\omega \in \Omega ;Y(\omega )=x_i\right\}\right)=P_n\left(X=x_i\right)=p_i.$$

Pokud se podaří aspoň dostatečně přesně definovat n.\,v.\,$Y$, musíme se pokusit na základě měření o~odhad pravděpodobností $p_i$. Nemůžeme, až na výjimky, stanovit skutečnou hodnotu příslušné entropie, ale jen její odhad, vycházející z~relativních četností $\widehat {p}_i=\frac{N_i} N$ jednotlivých variant $x_i$, kde $N_i$ je počet jejich výskytů v~souboru $N$ pozorování. Protože vycházíme z~odhadů pravděpodobností, vede použití příslušných vzorců pouze k~odhadu odpovídající entropie. Takový odhad ale může být vychýlený, zvlášť když výraz pro entropii obsahuje logaritmus, tj. se systematickou chybou. Každý výpočet by měl obsahovat nejen odhad pro entropii, ale i~chybu tohoto odhadu.

Je-li $h(p_1,p_2,\dots ,p_n)$ entropie se spojitými parciálními derivacemi $\frac{\partial h}{\partial p_i}$, $i=1,2,\dots ,n$, je možné určit chybu $\Delta h$ měření v~bodě $(\widehat {p}_1$, $\widehat {p}_2, \dots,\widehat {p}_n)$ ze vztahu 
$$
\Delta h \approx \left|\sum _i\frac{\partial h(\widehat {p}_1, \widehat{p}_2, \dots, \widehat {p}_n)}{\partial p_i}\Delta p_i\right|.
$$

Pro odhad $\left| \widehat {p}_i-p_i\right| =\Delta p_i$ je možné použít pro velká $N$ přibližného vztahu, platného s~pravděpodobností $1-\alpha$

$$
\Delta p_i \approx u_{1-\frac{\alpha } 2}\cdot \sqrt{\frac{\widehat {p}_i} N(1-\widehat {p}_i}),
$$
kde $u_{1-\frac{\alpha } 2}$ je $\left(1-\frac 1 2\alpha \right)$-tý kvantil normovaného normálního rozdělení. Podle tohoto vztahu je při $\alpha =0{,}05$ pro $u_{1-\frac{\alpha } 2}= 1{,}96.$ Odhady chyb jednotlivých entropií se pak budou počítat podle následujících vztahů

\begin{align*}
\Delta H_{\alpha }\left(X\right)
&\approx\frac 1{\ln 2}\frac{\alpha }{\left|1-\alpha \right|}
\cdot\frac 1{\sum _ip_i^{\alpha }}
\cdot\sum _ip_i^{\alpha -1} \Delta p_i \quad \textrm{[$\alpha$-\textit{bit}]}
\\
\Delta H\left(X\right) &\approx n\left(n-1\right)\cdot\max\Delta p_i
\\
\Delta T_q\left(X\right) &\approx \frac{kq}{\left|q-1\right|}\cdot\sum _ip_i^{q-1}\cdot\Delta p_i
\\
\Delta L(X) &\approx 2\cdot\sum _i\left|1-2p_i\right|\cdot \Delta p_i,
\end{align*}
kde všude dosazujeme za $p_i$ odhad $\widehat {p}_i.$%
\bigskip

%\noindent
\textbf{Příklad 2.} 
Mějme n.\,v.\,$X=\left(p_1,p_2,p_3\right),$ kde $0\leq p_i\leq 1,$ pro $i=1,2,3$, $\sum _1^3p_i=1$. Na vzorku o~rozsahu $N=100$ byl proveden odhad pravděpodobností $p_i$ s~výsledkem $\widehat {p}_1=0{,}2$; $\widehat {p}_2=0{,}5$; $\widehat {p}_3=0{,}3$. Odhadneme některé z~předchozích entropií. Budeme volit $\alpha =0{,}05$. Pak je
pro $\Delta p_1\approx 1{,}96\cdot 0{,}1\cdot\sqrt{0{,}2\cdot0{,}8}=0{,}078$; 
$\Delta p_2\approx1{,}96\cdot0{,}1\cdot\sqrt{0{,}5\cdot0{,}5}=0{,}098$;
$\Delta p_3\approx1{,}96\cdot0{,}1\cdot\sqrt{0{,}3\cdot0{,}7}=0{,}090$.

Pro odhady jednotlivých entropií pak máme
\begin{equation*}
\Delta H_2\left(X\right)\approx2\cdot\frac 1{0{,}38}\cdot\left(0{,}2\cdot0{,}078+0{,}5\cdot0{,}098+0{,}3\cdot0{,}090\right)=0{,}482.
\end{equation*}

Pro entropii $H_2\left(X\right)$ tak je
\begin{align*}
H_2\left(X\right) &\approx-\log _20{,}38\pm 0{,}482=1{,}40\pm 0{,}482\quad \textrm{[2-\textit{bit}]}.
\\
H\left(X\right)&\approx2\cdot\min \left(0{,}5;0{,}5\right)+2\cdot\min \left(0{,}3;0{,}2\right)\pm 6\cdot0{,}1=1{,}4\pm 0{,}6\quad [\mathit{lit}].
\\
T_2\left(X\right)&\approx2\cdot2\left(1-0{,}38\right)\pm (0{,}2\cdot0{,}078+0{,}5\cdot0{,}098+0{,}3\cdot0{,}090)=\\
&=2{,}48\pm 0{,}09\quad [\left(2{,}2\right)-\mathit{tit}]\ (\textrm{volili jsme tedy } k=2).
\\
L\left(X\right)&=2\cdot(1-0{,}38)\pm 2\cdot\left\{\left(1-2\cdot0{,}2\right)\cdot0{,}078+\left(1-2\cdot0{,}5\right)\cdot0{,}098+
\right.
\\
&\phantom{=}\left.+\left(1-2\cdot0{,}3\right)\cdot0{,}090\right\}=1{,}24\pm 0{,}16\quad [\mathit{lit}^2].
\end{align*}

Jak je vidět, všechny odhady entropií jsou zatíženy velkou chybou. Je proto třeba používat poměrně rozsáhlých náhodných výběrů. Aby byla v~našem příkladu chyba desetkrát menší, což by bylo přijatelné, měli bychom rozsah šetření $N$ zvětšit na 10\,000.




\section{Závěr}

K~odhadu neurčitosti máme poměrně velké množství měr \cite{1,5,14}. To je proto, že ne každá experimentální situace je pro určitou míru vhodná. Rozhoduje intuice a~zkušenost experimentátora, která určuje vhodnost konkrétní míry.

Všechny míry neurčitosti diskrétní náhodné veličiny $X$ jsou nezáporné a~jejich podstatnou vlastností je, že pro rovnoměrné rozdělení této veličiny nabývají nejvyšších možných hodnot (nebo se jim blíží) a~v~případě $X=(1,0,0,\dots ,0)$ jsou nulové. Vždy při odhadu neurčitosti se předpokládá buď znalost rozdělení n.\,v.\,$X$ (a to spíše výjimečně) nebo jen statistický odhad rozdělení (neurčitost je zde její pravděpodobnostní charakteristika). Naznačili jsme, že je potřebí realizovat odhad neurčitosti rozsáhlejším experimentem.

Někdy místo znalosti rozdělení náhodné veličiny $X$ je možné využít i~expertního odhadu (zvláště v~humanitních vědách), pak je možné se opřít o~teorii fuzzy množin a~využít některou z~fuzzy měr neurčitosti \cite{10,11}. Jsou však i~teorie, které neurčitosti počítají z~jiných znalostí, například Dempster\zz Schaferova teorie \cite{3}.

\section*{Poděkování}
Autor děkuje ing. Pavlu Střížovi, Ph.D., za technické dopracování tohoto příspěvku včetně obrázků.


\small
\begin{thebibliography}{99}
%\itemsep=0pt

\bibitem{1} Arndt, C.: \textit{Information measures: Information and its Description in Science and Engeneering}, Springer, 2004, ISBN 978-3-540-40855-0.

\bibitem{2} Cover, T.\,M., Thomas, J.\,A.: \textit{Elements of Information Theory,} 2. Edition, Wiley-Interscience, 2006, ISBN 0-471-24195-4.

\bibitem{3} Půlpán, Z.: \textit{K~problematice vágnosti v~humanitních vědách}, Academia, Studie 2/1997, ISBN 80-200-0648-6.

\bibitem{4} Rényi, A.: On measures of information and entropy, \textit{Proceedings of the 4th Berkley Symposium on Mathematics, Statistics and Probability,} 1961, pp.\,547--561.

\bibitem{5} Tsallis, C.: On measures of information and entropy, \textit{Journal of Statistical Physics} 52, 1988, pp.\,479--487.

\bibitem{6} Kukal, J.: Entropie nejen ve fyzice, \textit{Rozhledy matematicko-fyzikální} č.\,4, roč.\,86, 2011, str.\,13--20. 

\bibitem{7} Ben-Bassat, M., Raviv, J.: Rényis entropy and the probability of error, IEEE, \textit{Transactions on Information Theory}, Vol.\,24, No.\,3, pp.\,324--331, May 1978.

\bibitem{8} Rényi, A.: \textit{Teorie pravděpodobnosti,} Academia, Praha 1972.

\bibitem{9} Rényi, A.: On the dimension and entropy of probability distributions, \textit{Acta Math. Acad. Sci. Hung}. 10, 1959, pp.\,193--215.

\bibitem{10} Půlpán, Z.: \textit{Odhad informace z~dat vágní povahy}, Academia, Praha 2012, ISBN 978-80-200-2076-5.

\bibitem{11} Půlpán, Z.: Informace a~neurčitosti v~expertních odhadech, \textit{Informační Bulletin České statistické společnosti}, roč.\,33, 1, str.\,5--19.

\bibitem{12} Černý, J.: \textit{Entropia a~informácia v~kybernetike,} Bratislava, Alfa 1981.

\bibitem{13} Kullback, S.: \textit{Information Theory and Statistics}, New York, John Willey 1959.

\bibitem{14} Vajda, I.: Teória informácie a~štatistického rozhodovania, Bratislava, Alfa 1982.

\bibitem{15} Havrda, J.: \textit{Stochastické procesy a~teorie informace,} ČVUT, Praha 1982.

\bibitem{16} Vajda, I.: Axiomy $a${}-entropie zobecněného pravděpodobnostního schématu, \textit{Kybernetika}, č.\,2, roč.\,4, 1968.

\bibitem{17} Fadějev, D.\,K.: K~ponjatiu entropii koněčnoj věrojatnostnoj schemy, \textit{Uspěchi matěmatičeskich nauk,} č.\,11, 1956.

\bibitem{18} Zvárová, J.: On asymptotic Behavior of Sample Estimator of Rényi's Information of order $\alpha $, In: \textit{Transaction of the 6th Prague Conference on Information Theory}, Academia, Prague 1973.

\end{thebibliography}


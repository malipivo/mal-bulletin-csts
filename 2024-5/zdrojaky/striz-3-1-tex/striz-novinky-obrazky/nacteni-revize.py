import os
from collections import defaultdict # pydoc3 collections
data=defaultdict(); dleRevize=defaultdict(); citac=0
nazev="/home/malipivo/texlive/2023/tlpkg/" \
	"texlive.tlpdb.main.18022ca66fa110fb133d12433144df42"

soubor=open(nazev) # Otevření souboru...
while True: # Načítej řádek po řádku, dokud to lze.
	radek=soubor.readline()
	if not radek: break # Konec while, jinak si ukládej do dat.
	if radek[:4]=="name":
		citac+=1; data[citac]=defaultdict()
		data[citac]["name"]=radek[5:-1]
	if radek[:8]=="category":
		data[citac]["category"]=radek[9:-1]
	if radek[:8]=="revision":
		data[citac]["revision"]=int(radek[9:-1])
soubor.close()
# Rychlé vytažení jen balíčků a příprava na setřídění.
for d in data: 
	if data[d]["category"]=="Package":
		dleRevize[data[d]["revision"]]=[data[d]["name"]]
# Setříděné dle nejnovějšího vypiš do terminálu.
dleRevizeSorted=sorted(dleRevize,reverse=True)

# od: 67868 wordcloud + 670 záznamů
hranice=0; okolik=1
# hranice=660; okolik=10
citac=0
for d in dleRevizeSorted:
	citac+=1
	print(d, dleRevize[d][0])
	if citac>hranice and citac<=hranice+okolik:
		#print(citac)
		pass
		#os.popen("texdoc "+dleRevize[d][0])
